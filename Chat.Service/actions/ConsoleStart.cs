using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Collections;
using System.IO;
using Game.Server;
using System.Reflection;
using Game.Server.Packets.Server;

namespace Game.Service.actions
{
    /// <summary>
    /// Handles 
    /// </summary>
    public class ConsoleStart:IAction
    {
        /// <summary>
        /// Defines a logger for this class
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Returns the name of this action
        /// </summary>
        public string Name
        {
            get { return "--start"; }
        }

        /// <summary>
        /// Returns the syntax of this action
        /// </summary>
        public string Syntax
        {
            get { return "--start [-config=./config/serverconfig.xml]"; }
        }

        /// <summary>
        /// Returns the description of this action
        /// </summary>
        public string Description
        {
            get { return "Starts the DOL server in console mode"; }
        }

        /// <summary>
        /// Starts the game server and returns the result
        /// </summary>
        /// <returns></returns>
        private static bool StartServer()
        {
            Console.WriteLine("Starting the server");
            bool start = GameServer.Instance.Start();
            return start;
        }

        /// <summary>
        /// Handles the server action
        /// </summary>
        /// <param name="parameters"></param>
        public void OnAction(Hashtable parameters)
        {
            Console.WriteLine("Starting GameServer ... please wait a moment!");
            FileInfo configFile;
            FileInfo currentAssembly = null;
            if (parameters["-config"] != null)
            {
                Console.WriteLine("Using config file: " + parameters["-config"]);
                configFile = new FileInfo((String)parameters["-config"]);
            }
            else
            {
                currentAssembly = new FileInfo(Assembly.GetEntryAssembly().Location);
                configFile = new FileInfo(currentAssembly.DirectoryName + Path.DirectorySeparatorChar + "config" + Path.DirectorySeparatorChar + "serverconfig.xml");
            }

            GameServerConfiguration config = new GameServerConfiguration();
            if (configFile.Exists)
            {
                config.LoadFromXMLFile(configFile);
            }
            else
            {
                if (!configFile.Directory.Exists)
                    configFile.Directory.Create();
                config.SaveToXMLFile(configFile);
                if (File.Exists(currentAssembly.DirectoryName + Path.DirectorySeparatorChar + "RoadConfig.exe"))
                {
                    Console.WriteLine("No config file found, launching Road.Config.exe...");
                    System.Diagnostics.Process.Start(currentAssembly.DirectoryName + Path.DirectorySeparatorChar + "RoadConfig.exe");
                    return;
                }
            }

            GameServer.CreateInstance(config);
            StartServer();

            bool run = true;
            while (run)
            {
                Console.Write("> ");
                string line = Console.ReadLine();

                switch (line.ToLower())
                {
                    case "exit": run = false; break;
                    default:
                        break;
                }
            }

            if (GameServer.Instance != null)
                GameServer.Instance.Stop();
        }
       
    }
}
