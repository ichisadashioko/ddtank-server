using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Remoting.Contract;
using Remoting.Contract.Data;
using Remoting.Bussiness;

namespace Remoting.Services
{
    public class MapService
    {
        public MapInfo[] GetAllMap()
        {
            using (Map m = new Map())
            {
                return m.GetAllMap();
            }
        }

        public MapGoodsInfo[] GetAllMapGoods()
        {
            using (Map m = new Map())
            {
                return m.GetAllMapGoods();
            }
        }
    }
}
