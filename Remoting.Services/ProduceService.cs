using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Remoting.Contract;
using Remoting.Contract.Data;
using Remoting.Bussiness;

namespace Remoting.Services
{
    public class ProduceService:IProduceService
    {
        public ItemTemplateInfo[] GetAllGoods()
        {
            using (Produce p = new Produce())
            {
                return p.GetAllGoods();
            }
        }

        public PropInfo[] GetAllProp()
        {
            using (Produce p = new Produce())
            {
                return p.GetAllProp();
            }
        }

        public BallInfo[] GetAllBall()
        {
            using (Produce p = new Produce())
            {
                return p.GetAllBall();
            }
        }
    }
}
