using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Threading;
using Remoting.Contract;
using Remoting.Contract.Data;
using Remoting.Bussiness;

namespace Remoting.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class PlayerService:IPlayerService
    {
        public PlayerInfo Login(string username, string password)
        {
            using(Player player = new Player())
            {
                return player.Login(username, password);
            }
        }

        public int UpdatePlayer(PlayerInfo info)
        {
            using (Player player = new Player())
            {
                return player.UpdatePlayer(info);
            }
        }


        public int UpdatePlayers(PlayerInfo[] list)
        {
            using (Player player = new Player())
            {
                return player.UpdatePlayers(list);
            }
        }


        public ItemInfo[] GetAllGrids(int UserID)
        {
            using (Player player = new Player())
            {
                return player.GetAllGrids(UserID);
            }
        }

        public int BuyGoods(int UserID, int GoodsID, int count, int[] place)
        {
            using (Player player = new Player())
            {
                return player.BuyGoods(UserID, GoodsID, count, place);
            }
        }

        public int DeleteGoods(int UserID,int place)
        {
            using (Player player = new Player()) 
            {
                return player.DeleteGoods(UserID, place);
            }
        }

        public int BreakGoods(int userID, int start, int end, int count)
        {
            using (Player player = new Player())
            {
                return player.BreakGoods(userID, start, end, count);
            }
        }

        public int UnchainGoods(int userID, int start, int end)
        {
            using (Player player = new Player())
            {
                return player.UnchainGoods(userID, start, end);
            }
        }

        public int SendMail(MailInfo mail)
        {
            using (Player player = new Player()) 
            {
                return player.SendMail(mail);
            }
        }

        public int DeleteMail(int UserID, int mailID)
        {
            using (Player player = new Player())
            {
                return player.DeleteMail(UserID, mailID);
            }
        }

        public int UpdateMail(int UserID, int mailID)
        {
            using (Player player = new Player())
            {
                return player.UpdateMail(UserID,mailID);
            }
        }

        public int RepairGoods(int userID, int place)
        {
            using (Player player = new Player())
            {
                return player.RepairGoods(userID, place);
            }
        }

        #region IPlayerService Members


        public int EquipItem(int userId, int itemId)
        {
            throw new NotImplementedException();
        }

        public int UnequipItem(int userId, int itemId)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
