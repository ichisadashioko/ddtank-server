using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Base.Packets;
using Bussiness;
using SqlDataProvider.Data;
using Game.Server.GameUtils;
using Game.Server.Managers;

namespace Game.Server.Packets.Client
{
    [PacketHandler((byte)ePackageType.CHANGE_PLACE_ITEM, "改变物品位置")]
    public class UserChangeItemPlaceHandler:IPacketHandler
    {
        public int HandlePacket(GameClient client, GSPacketIn packet)
        {
            eBageType bagType = (eBageType)packet.ReadByte();
            int place = packet.ReadInt();
            //int toBagTemp = packet.ReadByte();
            //if (toBagTemp == 11) toBagTemp++;
            //eBageType tobagType = (eBageType)toBagTemp;
            eBageType tobagType = (eBageType)packet.ReadByte();
            int toplace = packet.ReadInt();
            int count = packet.ReadInt();
            //pkg.writeByte(bagtype);
            //pkg.writeInt(place);
            //pkg.writeByte(tobagType);
            //pkg.writeInt(toplace);
            //pkg.writeInt(count);
            PlayerInventory bag = client.Player.GetInventory(bagType);
            PlayerInventory tobag = client.Player.GetInventory(tobagType); 
            if ((bagType == tobagType)&place!=-1)
            {
                  //.GetItemInventory(temp);
                bag.MoveItem(place, toplace, count);
            }

            if (place == -1&toplace!=-1)
            {
                bag.RemoveItemAt(toplace);
            }
            if (place != -1 && toplace == -1&& tobagType !=eBageType.Bank&&bagType!=eBageType.Store&&tobagType !=eBageType.Store)
            {
                if (bagType == 0)
                {
                    bag.AddItem(client.Player.GetItemAt(bagType,place), 31); //toSolt = bag.FindFirstEmptySlot(31);
                }
                else
                {
                   bag.AddItem(client.Player.GetItemAt(bagType,place),0);
                }
                
                
            }
            if(place!=-1&&tobagType!=bagType&&tobagType!=eBageType.Store)
            {
                ConsortiaInfo info = ConsortiaMgr.FindConsortiaInfo(client.Player.PlayerCharacter.ConsortiaID);
                if (info != null)
                {
                    //if(tobagType==eBageType.Store) client.Player.StoreBag.MoveToStore(bag, place, bagType, info.StoreLevel * 10);
                
                    bag.MoveToStore(bag, place, toplace, tobag, info.StoreLevel * 10);
                }
            }
            if (tobagType == eBageType.Store||bagType==eBageType.Store)
            {
                var item = client.Player.GetItemAt(bagType, place);
                if (item!=null&&item.Count > 1)
                {
                    item.Count--;
                    bag.UpdateItem(item);
                    var tempItem =  item.Clone();
                    tempItem.Count = 1;
                    tobag.AddItemTo(tempItem,toplace);
                    //tobag.CommitChanges();
                }
                else bag.MoveToStore(bag, place, toplace, tobag, 50);
                //tobag.AddItem(client.Player.GetItemAt(bagType, place), toplace);
                
            }
            //bag.MoveItem(start, end);

            //if (start < 11 || end < 11)
            //    return 0;

            //client.Player.CurrentInventory.MoveItem(start, end);

            

            return 0;
        }
    }
}
