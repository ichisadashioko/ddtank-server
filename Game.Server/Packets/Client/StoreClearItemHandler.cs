using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Base.Packets;
using Bussiness;
using SqlDataProvider.Data;
using System.Configuration;
using Game.Server.Managers;
using Game.Server.Statics;
using Game.Server.GameObjects;
using Game.Server.GameUtils;

namespace Game.Server.Packets.Client
{
    [PacketHandler((int)ePackageType.CLEAR_STORE_BAG, "物品强化")]
    public class StoreClearItemHandler : IPacketHandler
    {
        public int HandlePacket(GameClient client, GSPacketIn packet)
        {
            //GSPacketIn pkg = packet.Clone();
            //pkg.ClearContext();
            PlayerInventory m_storeBag = client.Player.StoreBag2;
            PlayerEquipInventory m_mainBag = client.Player.MainBag;
            for(int i=0;i<m_storeBag.Capalility;i++){
                m_mainBag.AddItem(m_storeBag.GetItemAt(i));
                
            }
            m_storeBag.ClearBag();
            return 0;
        }
    }
}
