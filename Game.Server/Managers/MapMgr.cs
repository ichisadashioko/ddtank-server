using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Reflection;
using log4net.Util;
using Game.Server.GameObjects;
using System.Threading;
using Bussiness;
using SqlDataProvider.Data;
using Phy.Maps;
using System.IO;
using System.Drawing;
using Game.Server.Packets;

namespace Game.Server.Managers
{
    public class MapMgr
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static Dictionary<int, MapPoint> _maps;

        private static Dictionary<int, Map> _mapInfos;

        private static Dictionary<int, List<int>> _serverMap;

        private static ThreadSafeRandom random;

        private static System.Threading.ReaderWriterLock m_lock;

        #region reload

        public static bool ReLoadMap()
        {
            try
            {
                Dictionary<int, MapPoint> tempMaps = new Dictionary<int, MapPoint>();
                Dictionary<int, Map> tempMapInfos = new Dictionary<int, Map>();

                if (LoadMap(tempMaps, tempMapInfos))
                {
                    m_lock.AcquireWriterLock(Timeout.Infinite);
                    try
                    {
                        _maps = tempMaps;
                        _mapInfos = tempMapInfos;
                        return true;
                    }
                    catch
                    { }
                    finally
                    {
                        m_lock.ReleaseWriterLock();
                    }

                }
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("ReLoadMap", e);
            }

            return false;
        }

        public static bool ReLoadFallItem()
        {
            try
            {
                Dictionary<int, Dictionary<int, MapGoodsInfo>> tempMapFallGoods = new Dictionary<int, Dictionary<int, MapGoodsInfo>>();

                if (InitFallGoods(tempMapFallGoods))
                {
                    m_lock.AcquireWriterLock(Timeout.Infinite);
                    try
                    {
                        _mapFallGoods = tempMapFallGoods;
                        //return true;
                    }
                    catch
                    {
                        return false;
                    }
                    finally
                    {
                        m_lock.ReleaseWriterLock();
                    }

                }
                //_mapAwardGoods

                //Dictionary<int, Dictionary<int, MapGoodsInfo>> tempMapAwardGoods = new Dictionary<int, Dictionary<int, MapGoodsInfo>>();
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("ReLoadFallItem", e);
            }

            try
            {
                Dictionary<int, Dictionary<int, MapGoodsInfo>> tempMapAwardGoods = new Dictionary<int, Dictionary<int, MapGoodsInfo>>();
                Dictionary<int, int> tempMapAwardRandom = new Dictionary<int, int>();

                if (InitAwardGoods(tempMapAwardGoods, tempMapAwardRandom))
                {
                    m_lock.AcquireWriterLock(Timeout.Infinite);
                    try
                    {
                        _mapAwardGoods = tempMapAwardGoods;
                        _mapAwardRandom = tempMapAwardRandom;
                        //return true;
                    }
                    catch
                    {
                        return false;
                    }
                    finally
                    {
                        m_lock.ReleaseWriterLock();
                    }

                }

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("ReLoadAwardItem", e);
            }

            return true;
        }

        public static bool ReLoadQuestItem()
        {
            try
            {
                Dictionary<int, Dictionary<int, MapGoodsInfo>> tempMapQuestGoods = new Dictionary<int, Dictionary<int, MapGoodsInfo>>();

                if (InitQuestGoods(tempMapQuestGoods))
                {
                    m_lock.AcquireWriterLock(Timeout.Infinite);
                    try
                    {
                        _mapQuestGoods = tempMapQuestGoods;
                        return true;
                    }
                    catch
                    { }
                    finally
                    {
                        m_lock.ReleaseWriterLock();
                    }

                }
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("ReLoadQuestItem", e);
            }

            return false;
        }

        public static bool ReLoadMapServer()
        {
            try
            {
                Dictionary<int, List<int>> tempServerMap = new Dictionary<int, List<int>>();
                if (InitServerMap(tempServerMap))
                {
                    m_lock.AcquireWriterLock(Timeout.Infinite);
                    try
                    {
                        _serverMap = tempServerMap;
                        return true;
                    }
                    catch
                    { }
                    finally
                    {
                        m_lock.ReleaseWriterLock();
                    }

                }
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("ReLoadMapWeek", e);
            }

            return false;
        }

        public static bool ReLoadFallProp()
        {
            try
            {
                Dictionary<int, Dictionary<int, int>> tempMapPropGoods = new Dictionary<int, Dictionary<int, int>>();
                Dictionary<int, MapGoodsInfo> tempAllPropGoods = new Dictionary<int, MapGoodsInfo>();
                if (InitPropGoods(tempMapPropGoods, tempAllPropGoods))
                {
                    m_lock.AcquireWriterLock(Timeout.Infinite);
                    try
                    {
                        _mapPropGoods = tempMapPropGoods;
                        _allPropGoods = tempAllPropGoods;
                        return true;
                    }
                    catch
                    { }
                    finally
                    {
                        m_lock.ReleaseWriterLock();
                    }

                }
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("ReLoadMapWeek", e);
            }

            return false;
        }

        #endregion

        /// <summary>
        /// Initializes the MapMgr. 
        /// </summary>
        /// <returns></returns>
        public static bool Init()
        {
            try
            {
                random = new ThreadSafeRandom();
                m_lock = new System.Threading.ReaderWriterLock();

                _maps = new Dictionary<int, MapPoint>();
                _mapInfos = new Dictionary<int, Map>();
                if (!LoadMap(_maps, _mapInfos))
                    return false;

                _mapFallGoods = new Dictionary<int, Dictionary<int, MapGoodsInfo>>();
                if (!InitFallGoods(_mapFallGoods))
                    return false;

                _mapQuestGoods = new Dictionary<int, Dictionary<int, MapGoodsInfo>>();
                if (!InitQuestGoods(_mapQuestGoods))
                    return false;

                _serverMap = new Dictionary<int, List<int>>();
                if (!InitServerMap(_serverMap))
                    return false;

                _mapPropGoods = new Dictionary<int, Dictionary<int, int>>();
                _allPropGoods = new Dictionary<int, MapGoodsInfo>();
                if (!InitPropGoods(_mapPropGoods, _allPropGoods))
                    return false;

                _mapAwardGoods = new Dictionary<int, Dictionary<int, MapGoodsInfo>>();
                _mapAwardRandom = new Dictionary<int, int>();
                if (!InitAwardGoods(_mapAwardGoods, _mapAwardRandom))
                    return false;

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("MapMgr", e);
                return false;
            }
            return true;
        }

        public static bool LoadMap(Dictionary<int, MapPoint> maps, Dictionary<int, Map> mapInfos)
        {
            try
            {
                MapBussiness db = new MapBussiness();
                MapInfo[] query = db.GetAllMap();

                foreach (MapInfo m in query)
                {
                    if (string.IsNullOrEmpty(m.PosX) || string.IsNullOrEmpty(m.PosX1))
                        continue;

                    if (!maps.Keys.Contains(m.ID))
                    {
                        string[] tmp = m.PosX.Split('|');
                        string[] tmp1 = m.PosX1.Split('|');
                        //List<Point> pos = new List<Point>();
                        MapPoint pos = new MapPoint();
                        foreach (string s in tmp)
                        {
                            if (string.IsNullOrEmpty(s.Trim()))
                                continue;

                            string[] xy = s.Split(',');
                            pos.PosX.Add(new Point(int.Parse(xy[0]), int.Parse(xy[1])));
                            //Point temp = new Point(int.Parse(xy[0]), int.Parse(xy[1]));
                            //pos.Add(temp);
                        }

                        foreach (string s in tmp1)
                        {
                            if (string.IsNullOrEmpty(s.Trim()))
                                continue;

                            string[] xy = s.Split(',');
                            pos.PosX1.Add(new Point(int.Parse(xy[0]), int.Parse(xy[1])));
                            //Point temp = new Point(int.Parse(xy[0]), int.Parse(xy[1]));
                            //pos.Add(temp);
                        }

                        maps.Add(m.ID, pos);
                    }

                    if (!mapInfos.ContainsKey(m.ID))
                    {
                        Tile force = null;
                        string file = string.Format("map\\{0}\\fore.map", m.ID);
                        if (File.Exists(file))
                        {
                            force = new Tile(file, true);
                        }

                        Tile dead = null;
                        file = string.Format("map\\{0}\\dead.map", m.ID);
                        if (File.Exists(file))
                        {
                            dead = new Tile(file, false);
                        }

                        if (force != null || dead != null)
                        {
                            mapInfos.Add(m.ID, new Map(m, force, dead));
                        }
                        else
                        {
                            if (log.IsErrorEnabled)
                                log.Error("Map's file is not exist!");
                            return false;
                        }
                    }
                }

                if (maps.Count == 0 || mapInfos.Count == 0)
                {
                    if (log.IsErrorEnabled)
                        log.Error("maps:" + maps.Count + ",mapInfos:" + mapInfos.Count);
                    return false;
                }
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("MapMgr", e);
                return false;
            }
            return true;
        }

        public static Map CloneMap(int index)
        {
            if (_mapInfos.ContainsKey(index))
            {
                //return _mapInfos[index].Clone(); 
                return _mapInfos[index].Clone();

            }
            return null;
        }

        public static MapInfo FindMapInfo(int index)
        {
            if (_mapInfos.ContainsKey(index))
            {
                return _mapInfos[index].Info;
            }
            return null;
        }

        public static int GetMapIndex(int index, byte type)
        {
            if (index != 0 && !_maps.Keys.Contains(index))
                index = 0;
            if (index == 0)
            {
                List<int> tempIndex = new List<int>();
                foreach (int id in _serverMap[WorldMgr.ServerID])
                {
                    MapInfo tempInfo = FindMapInfo(id);
                    if ((int)(type & tempInfo.Type) != 0)
                    {
                        tempIndex.Add(id);
                    }
                }

                if (tempIndex.Count == 0)
                {
                    int count = _serverMap[WorldMgr.ServerID].Count;
                    return _serverMap[WorldMgr.ServerID][random.Next(count)];
                }
                else
                {
                    int count = tempIndex.Count;
                    return tempIndex[random.Next(count)];
                }
            }
            return index;
        }

        //public static Point[] GetMapPos(int index)
        //{
        //    if (index != 0 && !_maps.Keys.Contains(index))
        //        index = 0;

        //    if (index == 0)
        //    {
        //        int[] map = _maps.Keys.ToArray();

        //        return _maps[map[random.Next(map.Length)]].PosX.ToArray();
        //    }
        //    return _maps[index].PosX.ToArray();
        //}

        public static MapPoint GetMapRandomPos(int index)
        {
            MapPoint pos = new MapPoint();
            MapPoint temp;
            if (index != 0 && !_maps.Keys.Contains(index))
                index = 0;

            if (index == 0)
            {
                int[] map = _maps.Keys.ToArray();
                temp = _maps[map[random.Next(map.Length)]];

                //return _maps[map[random.Next(map.Length)]];

                //return _maps[map[random.Next(map.Length)]].PosX.ToArray();
            }
            else
            {
                temp = _maps[index];
            }

            if (random.Next(2) == 1)
            {
                pos.PosX.AddRange(temp.PosX);
                pos.PosX1.AddRange(temp.PosX1);
            }
            else
            {
                pos.PosX.AddRange(temp.PosX1);
                pos.PosX1.AddRange(temp.PosX);
            }

            return pos;
        }

        public static Dictionary<int, Dictionary<int, MapGoodsInfo>> _mapFallGoods;

        public static Dictionary<int, Dictionary<int, MapGoodsInfo>> _mapQuestGoods;

        public static Dictionary<int, Dictionary<int, int>> _mapPropGoods;

        private static Dictionary<int, MapGoodsInfo> _allPropGoods;

        private static Dictionary<int, Dictionary<int, MapGoodsInfo>> _mapAwardGoods;

        private static Dictionary<int, int> _mapAwardRandom;


        #region 周期

        public static int GetWeekDay
        {
            get
            {
                int day = Convert.ToInt32(DateTime.Now.DayOfWeek);
                return day == 0 ? 7 : day;
            }
        }

        public static bool InitServerMap(Dictionary<int, List<int>> servermap)
        {
            MapBussiness db = new MapBussiness();
            ServerMapInfo[] infos = db.GetAllServerMap();

            try
            {
                foreach (ServerMapInfo info in infos)
                {
                    //if (info.WeekID < 1 || info.WeekID > 7)
                    //    continue;

                    if (!servermap.Keys.Contains(info.ServerID))
                    {
                        string[] str = info.OpenMap.Split(',');
                        List<int> list = new List<int>();
                        foreach (string s in str)
                        {
                            if (string.IsNullOrEmpty(s))
                                continue;

                            list.Add(int.Parse(s));
                        }
                        servermap.Add(info.ServerID, list);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }

            return true;
        }

        #endregion

        #region 物品

        public static void InitGoods(Dictionary<int, Dictionary<int, MapGoodsInfo>> _mapGoods, MapGoodsInfo[] goods)
        {
            foreach (MapGoodsInfo v in goods)
            {
                if (v.MapID != 0)
                    continue;

                if (!_mapGoods.ContainsKey(v.MapID))
                {
                    Dictionary<int, MapGoodsInfo> temp = new Dictionary<int, MapGoodsInfo>();
                    //temp.Add(v.GoodsID, v.Random);
                    //temp.Add(v.GoodsID, v);
                    temp.Add(v.ID, v);
                    _mapGoods.Add(v.MapID, temp);
                }
                else
                {
                    if (!_mapGoods[v.MapID].ContainsKey(v.ID))
                    {
                        //_mapGoods[v.MapID].Add(v.GoodsID, v.Random);
                        _mapGoods[v.MapID].Add(v.ID, v);
                    }
                }
            }

            foreach (MapGoodsInfo v in goods)
            {
                if (!_mapGoods.ContainsKey(v.MapID))
                {
                    Dictionary<int, MapGoodsInfo> temp = new Dictionary<int, MapGoodsInfo>();
                    if (_mapGoods.ContainsKey(0))
                    {
                        foreach (KeyValuePair<int, MapGoodsInfo> p in _mapGoods[0])
                        {
                            temp.Add(p.Key, p.Value);
                        }
                    }

                    if (!temp.ContainsKey(v.ID))
                    {
                        temp.Add(v.ID, v);
                    }
                    else
                    {
                        temp[v.ID] = v;
                    }
                    _mapGoods.Add(v.MapID, temp);

                }
                else
                {
                    if (!_mapGoods[v.MapID].ContainsKey(v.ID))
                    {
                        _mapGoods[v.MapID].Add(v.ID, v);
                    }
                    else
                    {
                        _mapGoods[v.MapID][v.ID] = v;
                    }
                }
            }
        }

        public static List<MapGoodsInfo> GetGoodsAll(Dictionary<int, Dictionary<int, MapGoodsInfo>> _mapGoods, int map, int type)
        {
            List<MapGoodsInfo> list = new List<MapGoodsInfo>();
            if (!_mapGoods.ContainsKey(map))
                map = 0;

            if (_mapGoods.ContainsKey(map))
            {
                foreach (KeyValuePair<int, MapGoodsInfo> p in _mapGoods[map])
                {
                    if (p.Value.Type != 0 && (p.Value.Type & type) == 0)
                        continue;

                    int rand = random.Next(1000000);
                    if (rand < p.Value.Random)
                        list.Add(p.Value);

                }
            }
            return list;
        }

        #endregion

        #region 掉落物品

        public static bool InitFallGoods(Dictionary<int, Dictionary<int, MapGoodsInfo>> mapFallGoods)
        {
            using (MapBussiness db = new MapBussiness())
            {
                MapGoodsInfo[] goods = db.GetAllMapGoods();
                InitGoods(mapFallGoods, goods);
                return true;
            }
        }

        public static List<MapGoodsInfo> GetRandomGoodsAll(int map, int type)
        {
            return GetGoodsAll(_mapFallGoods, map, type);
        }

        public static MapGoodsInfo[] GetRandomGoodsByNumber(int map, int count, int roomType)
        {
            List<MapGoodsInfo> list = RandmonGoods(GetRandomGoodsAll(map, roomType));
            count = count > list.Count ? list.Count : count;
            return list.GetRange(0, count).ToArray();
        }

        public static List<MapGoodsInfo> RandmonGoods(List<MapGoodsInfo> list)
        {
            MapGoodsInfo temp;
            int index;
            for (int i = 0; i < list.Count; i++)
            {
                index = random.Next(list.Count);
                temp = list[i];
                list[i] = list[index];
                list[index] = temp;
            }
            return list;
        }

        #endregion

        #region 任务物品

        public static bool InitQuestGoods(Dictionary<int, Dictionary<int, MapGoodsInfo>> mapQuestGoods)
        {
            using (MapBussiness db = new MapBussiness())
            {
                MapGoodsInfo[] goods = db.GetAllQuestGoods();
                InitGoods(mapQuestGoods, goods);
                return true;
            }
        }

        public static List<MapGoodsInfo> GetQuestGoodsAll(int map)
        {
            return GetGoodsAll(_mapQuestGoods, map, 0);
        }

        #endregion

        #region 道具物品

        public static bool InitPropGoods(Dictionary<int, Dictionary<int, int>> mapPropGoods, Dictionary<int, MapGoodsInfo> allPropGoods)
        {
            using (MapBussiness db = new MapBussiness())
            {
                MapGoodsInfo[] goods = db.GetAllMapProps();

                foreach (MapGoodsInfo info in goods)
                {
                    if (info.MapID != 0)
                        continue;

                    if (info.GoodsID < 10000 || info.GoodsID > 10999)
                        continue;

                    if (!mapPropGoods.ContainsKey(info.MapID))
                    {
                        Dictionary<int, int> list = new Dictionary<int, int>();
                        list.Add(info.GoodsID, info.Random);
                        mapPropGoods.Add(info.MapID, list);
                    }
                    else
                    {
                        if (!mapPropGoods[info.MapID].ContainsKey(info.GoodsID))
                        {
                            int value = mapPropGoods[info.MapID].Last().Value + info.Random;
                            mapPropGoods[info.MapID].Add(info.GoodsID, value);
                        }
                    }

                    if (!allPropGoods.ContainsKey(info.GoodsID))
                    {
                        allPropGoods.Add(info.GoodsID, info);
                    }
                }

                foreach (MapGoodsInfo info in goods)
                {
                    if (mapPropGoods.ContainsKey(0) && mapPropGoods[0].ContainsKey(info.GoodsID))
                        continue;

                    if (info.GoodsID < 10000 || info.GoodsID > 10999)
                        continue;

                    if (!mapPropGoods.ContainsKey(info.MapID))
                    {
                        Dictionary<int, int> list = new Dictionary<int, int>();
                        if (mapPropGoods.ContainsKey(0))
                        {
                            foreach (KeyValuePair<int, int> p in mapPropGoods[0])
                            {
                                list.Add(p.Key, p.Value);
                            }
                        }
                        else
                        {
                            list.Add(0, 0);
                        }

                        int value = list.Last().Value + info.Random;
                        list.Add(info.GoodsID, value);
                        mapPropGoods.Add(info.MapID, list);
                    }
                    else
                    {
                        if (!mapPropGoods[info.MapID].ContainsKey(info.GoodsID))
                        {
                            int value = mapPropGoods[info.MapID].Last().Value + info.Random;
                            mapPropGoods[info.MapID].Add(info.GoodsID, value);
                        }
                    }

                    if (!allPropGoods.ContainsKey(info.GoodsID))
                    {
                        allPropGoods.Add(info.GoodsID, info);
                    }
                }

            }
            return true;
        }

        public static int GetRandomFightPropIndex(int map)
        {
            if (!_mapPropGoods.ContainsKey(map))
                map = 0;

            if (_mapPropGoods.ContainsKey(map))
            {
                Dictionary<int, int> list = _mapPropGoods[map];
                int rand = random.Next(list.Last().Value);
                foreach (KeyValuePair<int, int> info in list)
                {
                    if (info.Value > rand)
                        return info.Key;
                }
            }

            return 0;
        }

        public static void GetRandomFightPropByCount(int map, int count, List<MapGoodsInfo> list)
        {
            if (!_mapPropGoods.ContainsKey(map))
                map = 0;

            if (_mapPropGoods.ContainsKey(map))
            {
                Dictionary<int, int> infos = _mapPropGoods[map];

                for (int i = 0; i < count; i++)
                {
                    int rand = random.Next(infos.Last().Value);
                    foreach (KeyValuePair<int, int> info in infos)
                    {
                        if (info.Value > rand)
                        {
                            if (_allPropGoods.ContainsKey(info.Key))
                            {
                                list.Add(_allPropGoods[info.Key]);
                            }
                            break;
                        }

                    }
                }
            }
        }

        #endregion 

        #region 翻牌奖励

        public static bool InitAwardGoods(Dictionary<int, Dictionary<int, MapGoodsInfo>> mapAwardGoods, Dictionary<int, int> mapAwardRandom)
        {
            using (MapBussiness db = new MapBussiness())
            {
                MapGoodsInfo[] goods = db.GetAllMapAward();

                foreach (MapGoodsInfo info in goods)
                {
                    if (info.MapID != 0)
                        continue;

                    if (!mapAwardGoods.ContainsKey(info.MapID))
                    {
                        Dictionary<int, MapGoodsInfo> list = new Dictionary<int, MapGoodsInfo>();
                        //info.TotolRandom = info.Random;

                        if (!mapAwardRandom.ContainsKey(info.Type))
                        {
                            mapAwardRandom.Add(info.Type, info.Random);
                        }
                        else
                        {
                            mapAwardRandom[info.Type] = mapAwardRandom[info.Type] + info.Random;
                        }

                        info.TotolRandom = mapAwardRandom[info.Type];
                        list.Add(info.ID, info);
                        mapAwardGoods.Add(info.MapID, list);
                    }
                    else
                    {
                        if (!mapAwardRandom.ContainsKey(info.Type))
                        {
                            mapAwardRandom.Add(info.Type, info.Random);
                        }
                        else
                        {
                            mapAwardRandom[info.Type] = mapAwardRandom[info.Type] + info.Random;
                        }

                        //if (!mapAwardGoods[info.MapID].ContainsKey(info.ID))
                        //{
                        //info.TotolRandom = mapAwardGoods[info.MapID].Last().Value.TotolRandom + info.Random;

                        info.TotolRandom = mapAwardRandom[info.Type];
                        mapAwardGoods[info.MapID].Add(info.ID, info);
                        //}
                    }

                }

                foreach (MapGoodsInfo v in goods)
                {
                    if (!mapAwardGoods.ContainsKey(v.MapID))
                    {
                        Dictionary<int, MapGoodsInfo> temp = new Dictionary<int, MapGoodsInfo>();
                        if (mapAwardGoods.ContainsKey(0))
                        {
                            foreach (KeyValuePair<int, MapGoodsInfo> p in mapAwardGoods[0])
                            {
                                temp.Add(p.Key, p.Value);
                            }
                        }

                        if (!temp.ContainsKey(v.ID))
                        {
                            //v.TotolRandom = mapAwardGoods[v.MapID].Last().Value.TotolRandom + v.Random;

                            if (!mapAwardRandom.ContainsKey(v.Type))
                            {
                                mapAwardRandom.Add(v.Type, v.Random);
                            }
                            else
                            {
                                mapAwardRandom[v.Type] = mapAwardRandom[v.Type] + v.Random;
                            }

                            v.TotolRandom = mapAwardRandom[v.Type];

                            temp.Add(v.ID, v);
                        }

                        mapAwardGoods.Add(v.MapID, temp);

                    }
                    else
                    {
                        if (!mapAwardGoods[v.MapID].ContainsKey(v.ID))
                        {
                            //v.TotolRandom = mapAwardGoods[v.MapID].Last().Value.TotolRandom + v.Random;

                            if (!mapAwardRandom.ContainsKey(v.Type))
                            {
                                mapAwardRandom.Add(v.Type, v.Random);
                            }
                            else
                            {
                                mapAwardRandom[v.Type] = mapAwardRandom[v.Type] + v.Random;
                            }

                            v.TotolRandom = mapAwardRandom[v.Type];

                            mapAwardGoods[v.MapID].Add(v.ID, v);
                        }
                    }
                }

            }
            return true;
        }

        public static MapGoodsInfo GetRandomAward(int map,int mapType)
        {
            if (!_mapAwardGoods.ContainsKey(map))
                map = 0;

            if(!_mapAwardRandom.ContainsKey(mapType))
                return null;

            if (_mapAwardGoods.ContainsKey(map))
            {
                Dictionary<int, MapGoodsInfo> list = _mapAwardGoods[map];
                //int rand = random.Next(list.Last().Value.TotolRandom);
                int rand = random.Next(_mapAwardRandom[mapType]);
                foreach (KeyValuePair<int, MapGoodsInfo> info in list)
                {
                    if ((info.Value.Type & mapType) == 0)
                        continue;

                    if (info.Value.TotolRandom > rand)
                        return info.Value;

                    //if (info.Value.GoodsID == 11301)
                    //    return info.Value;
                }
            }

            return null;
        }

        #endregion 

    }

    public class MapPoint
    {
        public MapPoint()
        {
            posX = new List<Point>();
            posX1 = new List<Point>();
        }
        private List<Point> posX;

        public List<Point> PosX
        {
            get
            {
                return posX;
            }
            set
            {
                posX = value;
            }
        }

        private List<Point> posX1;

        public List<Point> PosX1
        {
            get
            {
                return posX1;
            }
            set
            {
                posX1 = value;
            }
        }

    }
}
