using Game.Base.Packets;
using Game.Logic.Phy.Object;

namespace Game.Logic.Cmd
{
    [GameCommand((byte)eTankCmdType.LOAD,"游戏加载进度")]
    public class LoadCommand:ICommandHandler
    {
        public void  HandleCommand(BaseGame game, Player player, GSPacketIn packet)
        {
            if (game.GameState == eGameState.Loading)
            {
                player.LoadingProcess = packet.ReadInt();
                if (player.LoadingProcess >= 100)
                {
                    game.CheckState(0);
                }
                game.SendToAll(packet);
            }
        }
    }
}
