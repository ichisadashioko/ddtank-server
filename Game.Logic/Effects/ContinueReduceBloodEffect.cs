using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Logic.Phy.Object;
using Bussiness.Managers;
using Game.Logic.Spells;
using Bussiness;


namespace Game.Logic.Effects
{
    public class ContinueReduceBloodEffect : AbstractEffect
    {
        private int m_count;

        public ContinueReduceBloodEffect(int count)
            : base(eEffectType.ContinueReduceBloodEffect)
        {
            m_count = count;
        }

        public override bool Start(Living living)
        {
            ContinueReduceBloodEffect effect = living.EffectList.GetOfType(eEffectType.ContinueDamageEffect) as ContinueReduceBloodEffect;
            if (effect != null)
            {
                effect.m_count = m_count;
                return true;
            }
            else
            {
                return base.Start(living);
            }
        }

        public override void OnAttached(Living living)
        {
            living.BeginSelfTurn += new LivingEventHandle(player_BeginFitting);
            living.Game.SendPlayerPicture(living, 2, true);
        }

        public override void OnRemoved(Living living)
        {
            living.BeginSelfTurn -= new LivingEventHandle(player_BeginFitting);
            living.Game.SendPlayerPicture(living, 2, false);
        }

        void player_BeginFitting(Living living)
        {
            m_count--;
            if (living is Player)
            {
                (living as Player).AddBlood(-50);
            }
            if (m_count < 0)
            {
                Stop();
            }
        }

    }
}
