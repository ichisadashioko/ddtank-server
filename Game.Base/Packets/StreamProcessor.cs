using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Reflection;
using System.Collections;
using Game.Base;
using System.Timers;
using System.Net.Sockets;
using System.Threading;
using Game.Base.Events;
using Game.Base.Packets;
using Road.Base.Packets;

namespace Game.Base.Packets
{
    /// <summary>
    /// This class handles the packets, receiving and sending
    /// </summary>
    public class StreamProcessor
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Holds the current client for this processor
        /// </summary>
        protected readonly BaseClient m_client;

        private FSM send_fsm;

        private FSM receive_fsm;

        private SocketAsyncEventArgs send_event;

        /// <summary>
        /// Constructs a new PacketProcessor
        /// </summary>
        /// <param name="client">The processor client</param>
        public StreamProcessor(BaseClient client)
        {
            m_client = client;

            m_tcpSendBuffer = client.SendBuffer;
            m_tcpQueue = new Queue(256);

            send_event = new SocketAsyncEventArgs();
            send_event.UserToken = this;
            send_event.Completed += AsyncTcpSendCallback;
            send_event.SetBuffer(m_tcpSendBuffer, 0, 0);

            send_fsm = new FSM(0x7abcdef7, 1501,"send_fsm");
            receive_fsm = new FSM(0x7abcdef7, 1501,"receive_fsm");
        }

        public void SetFsm(int adder, int muliter)
        {
            send_fsm.Setup(adder, muliter);
            receive_fsm.Setup(adder, muliter);
        }


        #region Send TCP Package

        /// <summary>
        /// Holds the TCP send buffer
        /// </summary>
        protected byte[] m_tcpSendBuffer;

        /// <summary>
        /// The client TCP packet send queue
        /// </summary>
        protected Queue m_tcpQueue;

        /// <summary>
        /// Indicates whether data is currently being sent to the client
        /// </summary>
        protected bool m_sendingTcp;

        /// <summary>
        /// 
        /// </summary>
        protected int m_firstPkgOffset = 0;


        protected int m_sendBufferLength = 0;

        /// <summary>
        /// Sends a packet via TCP
        /// </summary>
        /// <param name="packet">The packet to be sent</param>
        public void SendTCP(GSPacketIn packet)
        {

            //Fix the packet size
            packet.WriteHeader();

            //reset the offset for read
            packet.Offset = 0;

            //Check if client is connected
            if (m_client.Socket.Connected)
            {
                try
                {
                    Statistics.BytesOut += packet.Length;
                    Statistics.PacketsOut++;

                    if (log.IsDebugEnabled)
                    {
                        log.Debug(Marshal.ToHexDump(string.Format("Send Pkg to {0} :", m_client.TcpEndpoint), packet.Buffer, 0, packet.Length));
                    }

                    lock (m_tcpQueue.SyncRoot)
                    {
                        m_tcpQueue.Enqueue(packet);
                        if (m_sendingTcp)
                        {
                            return;
                        }
                        else
                        {
                            m_sendingTcp = true;
                        }
                    }

                    if (m_client.AsyncPostSend)
                    {
                        ThreadPool.QueueUserWorkItem(new WaitCallback(AsyncSendTcpImp), this);
                    }
                    else
                    {
                        AsyncTcpSendCallback(this, send_event);
                    }
                }
                catch (Exception e)
                {
                    log.Error("SendTCP", e);
                    log.WarnFormat("It seems <{0}> went linkdead. Closing connection. (SendTCP, {1}: {2})", m_client, e.GetType(), e.Message);
                    m_client.Disconnect();
                }
            }
        }

        private static void AsyncSendTcpImp(object state)
        {
            StreamProcessor proc = state as StreamProcessor;
            BaseClient client = proc.m_client;
            try
            {
                AsyncTcpSendCallback(proc, proc.send_event);
            }
            catch (Exception ex)
            {
                log.Error("AsyncSendTcpImp", ex);
                client.Disconnect();
            }
        }


        /// <summary>
        /// Callback method for async sends
        /// </summary>
        /// <param name="ar"></param>
        private static void AsyncTcpSendCallback(object sender, SocketAsyncEventArgs e)
        {
            StreamProcessor proc = (StreamProcessor)e.UserToken;
            BaseClient client = proc.m_client;
            try
            {
                Queue q = proc.m_tcpQueue;
                if (q == null || !client.Socket.Connected)
                    return;
                int sent = e.BytesTransferred;
                byte[] data = proc.m_tcpSendBuffer;
                int count = 0;
                if (sent != e.Count)
                {
                    //log.Error("Count:" + e.Count + ",sent:" + sent + ",offset:" + e.Offset + ",m_sendBufferLength:" + proc.m_sendBufferLength + ",client:" + client.TcpEndpoint);
                    if (proc.m_sendBufferLength > sent)
                    {
                        count = proc.m_sendBufferLength - sent;
                        Array.Copy(data, sent, data, 0, count);
                    }
                }
                e.SetBuffer(0, 0);

                int firstOffset = proc.m_firstPkgOffset;
                lock (q.SyncRoot)
                {
                    if (q.Count > 0)
                    {
                        do
                        {
                            PacketIn pak = (PacketIn)q.Peek();

                            int len = 0;
                            if (client.Encryted)
                            {
                                int key = proc.send_fsm.getState();
                                len = pak.CopyTo(data, count, firstOffset,key);
                            }
                            else
                            {
                                len = pak.CopyTo(data, count, firstOffset);
                            }

                            firstOffset += len;
                            count += len;

                            if (pak.Length <= firstOffset)
                            {
                                q.Dequeue();
                                firstOffset = 0;
                                if (client.Encryted)
                                {
                                    proc.send_fsm.UpdateState();
                                }
                            }
                            if (data.Length == count)
                            {
                                break;
                            }
                        } while (q.Count > 0);

                    }
                    proc.m_firstPkgOffset = firstOffset;
                    if (count <= 0)
                    {
                        proc.m_sendingTcp = false;
                        return;
                    }
                }

                proc.m_sendBufferLength = count;
                e.SetBuffer(0, count);
                if (client.SendAsync(e) == false)
                {
                    AsyncTcpSendCallback(sender, e);
                }
            }
            catch (Exception ex)
            {
                log.Error("AsyncTcpSendCallback", ex);
                log.WarnFormat("It seems <{0}> went linkdead. Closing connection. (SendTCP, {1}: {2})", client, ex.GetType(), ex.Message);
                client.Disconnect();
            }
        }


        #endregion

        #region Receive Package
        /// <summary>
        /// Called when the client receives bytes
        /// </summary>
        /// <param name="numBytes">The number of bytes received</param>
        public void ReceiveBytes(int numBytes)
        {
            lock (this)
            {
                byte[] buffer = m_client.PacketBuf;

                //End Offset of buffer
                int bufferSize = m_client.PacketBufSize + numBytes;
                //log.Debug(Marshal.ToHexDump("Recieve:", buffer, 0, bufferSize));
                //Size < minimum
                if (bufferSize < GSPacketIn.HDR_SIZE)
                {
                    m_client.PacketBufSize = bufferSize; // undo buffer read
                    return;
                }
                //Reset the offset
                m_client.PacketBufSize = 0;

                int curOffset = 0;

                do
                {
                    //read buffer length
                    int packetLength = 0;
                    int header = 0;
                    if (m_client.Encryted)
                    {
                        int key = receive_fsm.getState();
                        int i = receive_fsm.count;
                        key = (key & (0xff << 16)) >> 16;
                        while(curOffset +4 < bufferSize)
                        {
                            header = ((byte)(buffer[curOffset] ^ key) << 8) + (byte)(buffer[curOffset + 1] ^ key);
                            if (header == GSPacketIn.HEADER)
                            {
                                packetLength = ((byte)(buffer[curOffset + 2] ^ key) << 8) + (byte)(buffer[curOffset + 3] ^ key);
                                break;
                            }
                            else
                            {
                                curOffset++;
                            }
                        }
                    }
                    else
                    {
                        while(curOffset + 4 < bufferSize)
                        {
                            header = (buffer[curOffset] << 8) + buffer[curOffset + 1];
                            if (header == GSPacketIn.HEADER)
                            {
                                packetLength = (buffer[curOffset + 2] << 8) + buffer[curOffset + 3];
                                break;
                            }
                            else
                            {
                                curOffset++;
                            }
                        }
                    }

                    if ((packetLength != 0 && packetLength < GSPacketIn.HDR_SIZE) || packetLength > 2048)
                    {
                        log.Error("packetLength:" + packetLength + ",GSPacketIn.HDR_SIZE:" + GSPacketIn.HDR_SIZE + ",offset:" + curOffset + ",bufferSize:" + bufferSize + ",numBytes:" + numBytes);
                        log.ErrorFormat("Err pkg from {0}:", m_client.TcpEndpoint);
                        log.Error(Marshal.ToHexDump("===> error buffer", buffer));
                        m_client.PacketBufSize = 0;
                        if (m_client.Strict)
                        {
                            m_client.Disconnect();
                        }
                        return;
                    }

                    int dataLeft = bufferSize - curOffset;
                    if (dataLeft < packetLength || packetLength == 0)
                    {
                        Array.Copy(buffer, curOffset, buffer, 0, dataLeft);
                        m_client.PacketBufSize = dataLeft;
                        break;
                    }

                    GSPacketIn pkg = new GSPacketIn(new byte[2048],2048);
                    if (m_client.Encryted)
                    {
                        pkg.CopyFrom(buffer, curOffset, 0, packetLength, receive_fsm.getState());
                        receive_fsm.UpdateState();
                    }
                    else
                    {
                        pkg.CopyFrom(buffer, curOffset, 0, packetLength);
                    }
                    pkg.ReadHeader();

                    log.Debug((Marshal.ToHexDump("Recieve Packet:", pkg.Buffer, 0, packetLength)));
                    try
                    {
                        m_client.OnRecvPacket(pkg);
                    }
                    catch (Exception e)
                    {
                        if (log.IsErrorEnabled)
                            log.Error("HandlePacket(pak)", e);
                    }

                    curOffset += packetLength;

                } while (bufferSize - 1 > curOffset);

                if (bufferSize - 1 == curOffset)
                {
                    buffer[0] = buffer[curOffset];
                    m_client.PacketBufSize = 1;
                }
            }
        }

        #endregion

        public void Dispose()
        {
            send_event.Dispose();
            m_tcpQueue.Clear();
        }
    }
}
