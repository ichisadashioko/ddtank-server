using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Game.Base
{
    /// <summary>
    /// Class reads data from incoming incoming packets
    /// </summary>
    public class PacketIn 
    {
        protected byte[] m_buffer;
        protected int m_length;
        protected int m_offset;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="buf">Buffer containing packet data to read from</param>
        /// <param name="start">Starting index into buf</param>
        /// <param name="size">Number of bytes to read from buf</param>
        public PacketIn(byte[] buf, int len)
        {
            m_buffer = buf;
            m_length = len;
            m_offset = 0;
        }

        public byte[] Buffer
        {
            get { return m_buffer; }
        }

        public int Length
        {
            get { return m_length; }
        }

        public int Offset
        {
            get { return m_offset; }
            set { m_offset = value; }
        }

        public int DataLeft
        {
            get { return m_length - m_offset; }
        }

        /// <summary>
        /// Skips 'num' bytes ahead in the stream
        /// </summary>
        /// <param name="num">Number of bytes to skip ahead</param>
        public void Skip(int num)
        {
            m_offset += num;
        }

        public virtual bool ReadBoolean()
        {
            return m_buffer[m_offset++] != 0;
        }

        public virtual byte ReadByte()
        {
             return m_buffer[m_offset++];
        }

        /// <summary>
        /// Reads in 2 bytes and converts it from network to host byte order
        /// </summary>
        /// <returns>A 2 byte (short) value</returns>
        public virtual short ReadShort()
        {
            byte v1 = (byte)ReadByte();
            byte v2 = (byte)ReadByte();
            return Marshal.ConvertToInt16(v1, v2);
        }

        /// <summary>
        /// Reads in 2 bytes
        /// </summary>
        /// <returns>A 2 byte (short) value in network byte order</returns>
        public virtual short ReadShortLowEndian()
        {
            byte v1 = (byte)ReadByte();
            byte v2 = (byte)ReadByte();
            return Marshal.ConvertToInt16(v2, v1);
        }

        /// <summary>
        /// Reads in 4 bytes and converts it from network to host byte order
        /// </summary>
        /// <returns>A 4 byte value</returns>
        public virtual int ReadInt()
        {
            byte v1 = (byte)ReadByte();
            byte v2 = (byte)ReadByte();
            byte v3 = (byte)ReadByte();
            byte v4 = (byte)ReadByte();
            return Marshal.ConvertToInt32(v1, v2, v3, v4);
        }
        
        /// <summary>
        /// Reads in 4 bytes and converts it from network to host byte order
        /// </summary>
        /// <returns>A 4 byte value</returns>
        public virtual float ReadFloat()
        {
            byte[] v = new byte[4];
            for (int i = 0; i < v.Length;i++ )
            {
                v[i] = (byte)ReadByte();
            }

            return BitConverter.ToSingle(v,0);
        }

        /// <summary>
        /// Reads in 8 bytes and converts it from network to host byte order
        /// </summary>
        /// <returns>A 8 byte value</returns>
        public virtual double ReadDouble()
        {
            byte[] v = new byte[8];
            for (int i = 0; i < v.Length; i++)
            {
                v[i] = (byte)ReadByte();
            }

            return BitConverter.ToDouble(v,0);
        }

        /// <summary>
        /// Reads a null-terminated string from the stream
        /// </summary>
        /// <param name="maxlen">Maximum number of bytes to read in</param>
        /// <returns>A string of maxlen or less</returns>
        public virtual string ReadString()
        {
            short len = ReadShort();
            string temp = Encoding.UTF8.GetString(m_buffer, m_offset,len);
            m_offset += len;
            return temp.Replace("\0","");
            //return temp;
        }

        public virtual byte[] ReadBytes(int maxLen)
        {
            byte[] data = new byte[maxLen];
            Array.Copy(m_buffer, m_offset, data, 0, maxLen);
            m_offset += maxLen;
            return data;
        }

        public virtual byte[] ReadBytes()
        {
            return ReadBytes(m_length - m_offset);
        }

        /// <summary>
        /// Read a datetime
        /// </summary>
        /// <returns></returns>
        public DateTime ReadDateTime()
        {
            return new DateTime(ReadShort(), ReadByte(), ReadByte(), ReadByte(), ReadByte(), ReadByte());
        }

        /// <summary>
        /// Copy data to target buffer,this doesn't move offset.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public virtual int CopyTo(byte[] dst, int dstOffset, int offset)
        {
            int len = m_length - offset < dst.Length - dstOffset ? m_length - offset : dst.Length - dstOffset;
            if (len > 0)
            {
                System.Buffer.BlockCopy(m_buffer, offset, dst, dstOffset, len);
            }
            return len;
        }

        public virtual int CopyTo(byte[] dst, int dstOffset, int offset, int key)
        {
            int len = m_length - offset < dst.Length - dstOffset ? m_length - offset : dst.Length - dstOffset;

            if (len > 0)
            {
                key = (key & (0xff << 16)) >> 16;
                for (int i = 0; i < len; i++)
                {
                    dst[dstOffset + i] = (byte)(m_buffer[offset + i] ^ key);
                }
            }
            return len;
        }

        public virtual int CopyFrom(byte[] src, int srcOffset, int offset, int count)
        {
            if (count < m_buffer.Length && count - srcOffset < src.Length)
            {
                System.Buffer.BlockCopy(src, srcOffset, m_buffer, offset, count);
                return count;
            }
            return -1;
        }


        public virtual int CopyFrom(byte[] src, int srcOffset, int offset,int count, int key)
        {
            if (count < m_buffer.Length && count - srcOffset < src.Length)
            {
                key = (key & (0xff << 16)) >> 16;
                for (int i = 0; i < count; i++)
                {
                    m_buffer[offset + i] = (byte)(src[srcOffset + i] ^ key);
                }
                return count;
            }
            return -1;
        }

        public virtual void WriteBoolean(bool val)
        {
            m_buffer[m_offset++] = val ? (byte)1 : (byte)0;
            m_length = m_offset > m_length ? m_offset : m_length;
        }

        public virtual void WriteByte(byte val)
        {
            m_buffer[m_offset++] = val;
            m_length = m_offset > m_length ? m_offset : m_length;
        }

        public virtual void Write(byte[] src)
        {
            Write(src, 0, src.Length);
        }

        public virtual void Write(byte[] src, int offset, int len)
        {
            Array.Copy(src, offset, m_buffer, m_offset, len);
            m_offset += len;
            m_length = m_offset > m_length ? m_offset : m_length;
        }

        /// <summary>
        /// Writes a 2 byte (short) value to the stream in network byte order
        /// </summary>
        /// <param name="val">Value to write</param>
        public virtual void WriteShort(short val)
        {
            WriteByte((byte)(val >> 8));
            WriteByte((byte)(val & 0xff));
        }

        /// <summary>
        /// Writes a 2 byte (short) value to the stream in host byte order
        /// </summary>
        /// <param name="val">Value to write</param>
        public virtual void WriteShortLowEndian(short val)
        {
            WriteByte((byte)(val & 0xff));
            WriteByte((byte)(val >> 8));
        }

        /// <summary>
        /// Writes a 4 byte value to the stream in host byte order
        /// </summary>
        /// <param name="val">Value to write</param>
        public virtual void WriteInt(int val)
        {
            WriteByte((byte)(val >> 24));
            WriteByte((byte)((val >> 16) & 0xff));
            WriteByte((byte)((val & 0xffff) >> 8));
            WriteByte((byte)((val & 0xffff) & 0xff));
        }

        /// <summary>
        /// Writes a 4 byte value to the stream in host byte order
        /// </summary>
        /// <param name="val">Value to write</param>
        public virtual void WriteFloat(float val)
        {
            byte[] src = BitConverter.GetBytes(val);
            Write(src);
        }

        /// <summary>
        /// Writes a 4 byte value to the stream in host byte order
        /// </summary>
        /// <param name="val">Value to write</param>
        public virtual void WriteDouble(double val)
        {
            byte[] src = BitConverter.GetBytes(val);
            Write(src);
        }

        /// <summary>
        /// Writes the supplied value to the stream for a specified number of bytes
        /// </summary>
        /// <param name="val">Value to write</param>
        /// <param name="num">Number of bytes to write</param>
        public virtual void Fill(byte val, int num)
        {
            for (int i = 0; i < num; ++i)
            {
                WriteByte(val);
            }
        }

        /// <summary>
        /// Writes a C-style string to the stream
        /// </summary>
        /// <param name="str">String to write</param>
        public virtual void WriteString(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                byte[] bytes = Encoding.UTF8.GetBytes(str);
                WriteShort((short)(bytes.Length + 1));
                Write(bytes, 0, bytes.Length);
                WriteByte(0x0);
            }
            else
            {
                WriteShort((short)1);
                WriteByte(0x0);
            }
        }


        /// <summary>
        /// Writes up to maxlen bytes to the stream from the supplied string
        /// </summary>
        /// <param name="str">String to write</param>
        /// <param name="maxlen">Maximum number of bytes to be written</param>
        public virtual void WriteString(string str, int maxlen)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            int len = bytes.Length < maxlen ? bytes.Length : maxlen;
            WriteShort((short)len);
            Write(bytes, 0, len);
        }

        /// <summary>
        ///  writes a dattime to buffer
        /// </summary>
        /// <param name="date"></param>
        public void WriteDateTime(DateTime date)
        {
            WriteShort((short)date.Year);
            WriteByte((byte)date.Month);
            WriteByte((byte)date.Day);
            WriteByte((byte)date.Hour);
            WriteByte((byte)date.Minute);
            WriteByte((byte)date.Second);
        }
    }
}
