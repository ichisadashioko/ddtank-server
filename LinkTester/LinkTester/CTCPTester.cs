using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Configuration;
using System.Net;

namespace LinkTester
{
    class CTCPTester:CBaseTester
    {
        private readonly int TCP_TIME_OUT = 1000000;
        
        public CTCPTester(string ip, string port)
        {
            _ip = ip; 
            _port = port;
        }

        public override void Execute(int testID)
        {
            TcpClient MyClient = new TcpClient();

            DateTime dt_begin = DateTime.Now;

            try
            {
                IPAddress ip = IPAddress.Parse(_ip);

                MyClient.SendTimeout = TCP_TIME_OUT;
                MyClient.Connect(new IPEndPoint(ip, Convert.ToInt32(_port)));

                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - dt_begin;

                string content = dt_begin.ToString() + "," + testID + "," + LINK_SUCCEED + "," + ts.Milliseconds.ToString();

                WriteLogFile(content);

                return;
            }
            catch (SocketException ex)
            {

                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - dt_begin;

                string content = dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                Console.WriteLine("{0}", content);

                WriteLogFile(content);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.ToString());
                WriteErrFile(e.ToString());
            }
            
            //base.Execute();
        }

        private string _port;
        private string _ip;
    }
}
