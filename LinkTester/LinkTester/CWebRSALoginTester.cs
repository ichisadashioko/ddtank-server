using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Net;
using System.Configuration;
using System.Web;
using Game.Base;

namespace LinkTester
{
    class CWebRSALoginTester : CBaseTester
    {
        private string _adr;
        private string _user;
        private string _pwd;
        private static string _innerpwd = "";

        public CWebRSALoginTester(string adr, string user, string pwd)
        {
            _adr = adr;
            _user = user;
            _pwd = pwd; 
        }

        public override void Execute(int testID)
        {
            DateTime dt_begin = DateTime.Now;
            try
            {
                char[] w = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

                StringBuilder strRandom = new StringBuilder(6);
                Random rd = new Random();
                for (int i = 0; i < 6; i++)
                {
                    strRandom.Append(w[rd.Next(26)]);
                }
                _innerpwd = strRandom.ToString();

                InnerPassWord = _innerpwd;
                //SaveNewPassword();

                string strdata = _user + "," + _pwd + "," + _innerpwd;

                MemoryStream msData = new MemoryStream();

                BinaryWriter writer = new BinaryWriter(msData);
                
                writer.Write((short)DateTime.UtcNow.Year);
                writer.Write((byte)DateTime.UtcNow.Month);
                writer.Write((byte)DateTime.UtcNow.Date.Day);
                writer.Write((byte)DateTime.UtcNow.Hour);
                writer.Write((byte)DateTime.UtcNow.Minute);
                writer.Write((byte)DateTime.UtcNow.Second);
                writer.Write(Encoding.UTF8.GetBytes(strdata));

                string privateKey = ConfigurationSettings.AppSettings["privateKey"];

                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(privateKey);
                byte[] data = rsa.Encrypt(msData.ToArray(), false);
            
                writer.Close();

                string sendcontent = _adr + "?p=" + HttpUtility.UrlEncode(Convert.ToBase64String(data)) + "&v=0";

                WebRequest wrq = WebRequest.Create(sendcontent);

                WebResponse wrs = wrq.GetResponse();

                Stream strm = wrs.GetResponseStream();

                StreamReader sr = new StreamReader(strm);

                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - dt_begin;

                string line;
                string content;

                if ((line = sr.ReadLine()) != null)
                {
                    if (line.IndexOf("登陆成功") > 0)
                    {
                        content = dt_begin.ToString() + "," + testID + "," + LINK_SUCCEED + "," + ts.Milliseconds.ToString();
                        Result = true;
                    }
                    else
                    {
                        content = dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                        Console.WriteLine("{0}", content);
                        Result = false;
                    }
                }
                else
                {
                    content = dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                    Console.WriteLine("{0}", content);
                    Result = false;
                }

                WriteLogFile(content);

                sr.Close();
                strm.Close();
                wrs.Close();                

            }
            catch (WebException ex)
            {
                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - dt_begin;

                string content = dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                Console.WriteLine("{0}", content);
                Result = false;

                WriteLogFile(content);
                WriteErrFile(ex.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.ToString());
                WriteErrFile(e.ToString());
            }
        }

        public void SaveNewPassword()
        {
            string logPath = ConfigurationSettings.AppSettings["passwordpath"];
            using (FileStream fs = File.Open(logPath, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.WriteLine("{0}", _innerpwd);
                }
            }
        }

        public static string InnerPassWord
        {
            get
            {
                return _innerpwd; 
            }

            set
            {
                _innerpwd = value;
            }
        }

    }
}
