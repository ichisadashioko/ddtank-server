using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Configuration;
using Game.Base.Packets;
using Game.Base;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Road.Base.Packets;

namespace LinkTester
{
    class CTCPRSALoginTester:CBaseTester
    {
        private string _user;
        private string _innerpwd;
        private string _ip;
        private string _port;
        private DateTime _dt_begin;
        private int _testID;
        private bool _writeTimeOut;

        private CLinkTesterConnector bconnector;

        private const short CODE_LOGIN = 0x01;
        private const short CODE_CREATROOM = 0x5e;
        
        public CTCPRSALoginTester(string ip, string port,string user, string innerpwd)
        {
            _user = user;
            _innerpwd = innerpwd;
            _ip = ip;
            _port = port;
            _writeTimeOut = true;

            byte[] readbuf = new byte[1024];
            byte[] sendbuf = new byte[1024];

            bconnector = new CLinkTesterConnector(_ip, Convert.ToInt32(_port), false, readbuf, sendbuf);
            bconnector.PacketReceived += new LinkTester.CLinkTesterConnector.PacketProcessHandle(bconnector_PacketReceived);
        }

        void bconnector_PacketReceived(GSPacketIn packet)
        {
            string content;
            switch(packet.Code)
            {
                case CODE_LOGIN:
                    if (packet.ReadBoolean())
                    {
                        DateTime dt_end = DateTime.Now;
                        TimeSpan ts = dt_end - _dt_begin;

                        content = _dt_begin.ToString() + "," + _testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                        WriteLogFile(content);
                        Result = false;


                    }
                    else
                    {
                        CreateRoom();

                    }

                    
                    break;

                case 2:
                    Console.WriteLine(packet.ReadString());
                    break;

                case CODE_CREATROOM:
                    if (packet.ReadBoolean())
                    {
                        DateTime dt_end = DateTime.Now;
                        TimeSpan ts = dt_end - _dt_begin;

                        content = _dt_begin.ToString() + "," + _testID + "," + LINK_SUCCEED + "," + ts.Milliseconds.ToString();
                        WriteLogFile(content);
                        _writeTimeOut = false;
                        Result = true;

                        bconnector.Disconnect();

                    }
                    else
                    {
                        DateTime dt_end = DateTime.Now;
                        TimeSpan ts = dt_end - _dt_begin;

                        content = _dt_begin.ToString() + "," + _testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                        WriteLogFile(content);
                        _writeTimeOut = false;
                        Result = false;

                        bconnector.Disconnect();

                    }

                    break;

                default:

                    break;
            }
            
        }

        private void CreateRoom()
        {
            try 
            {
                GSPacketIn pkg = new GSPacketIn((byte)CODE_CREATROOM);
                pkg.WriteByte(1);
                pkg.WriteByte(1);
                pkg.WriteByte(2);
                pkg.WriteString("恐怖集中营" + "," + "998");

                bconnector.SendTCP(pkg);
            }
            catch (SocketException ex)
            {

                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - _dt_begin;

                string content = _dt_begin.ToString() + "," + _testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                Console.WriteLine("{0}", content);
                Result = false;

                WriteLogFile(content);
                WriteErrFile(ex.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.ToString());
                WriteErrFile(e.ToString());
            }            
        }

        public override void Execute(int testID)
        {
            _dt_begin = DateTime.Now;
            _testID = testID;

            try
            {
                string strdata = _user + "," + _innerpwd;

                short fsm_key = 101;
                byte[] bkey = BitConverter.GetBytes(fsm_key);
                Array.Reverse(bkey);

                MemoryStream msData = new MemoryStream();

                BinaryWriter writer = new BinaryWriter(msData);

                writer.Write((short)DateTime.UtcNow.Year);
                writer.Write((byte)DateTime.UtcNow.Month);
                writer.Write((byte)DateTime.UtcNow.Date.Day);
                writer.Write((byte)DateTime.UtcNow.Hour);
                writer.Write((byte)DateTime.UtcNow.Minute);
                writer.Write((byte)DateTime.UtcNow.Second);
                writer.Write(bkey[0]);
                writer.Write(bkey[1]);
                writer.Write(Encoding.UTF8.GetBytes(strdata));

                string privateKey = ConfigurationSettings.AppSettings["privateKey"];

                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(privateKey);
                byte[] data = rsa.Encrypt(msData.ToArray(), false);
                
                writer.Close();

                GSPacketIn pkg = new GSPacketIn((byte)CODE_LOGIN);

                string strEdition = ConfigurationSettings.AppSettings["Edition"];

                pkg.WriteInt(Convert.ToInt32(strEdition));
                pkg.Write(data);

                bconnector.Connect();

                if (bconnector.IsConnected)
                {
                    bconnector.SendTCP(pkg);
                    bconnector._processor.SetFsm(fsm_key, Convert.ToInt32(strEdition));
                }
                else
                {
                    DateTime dt_end = DateTime.Now;
                    TimeSpan ts = dt_end - _dt_begin;

                    string content = _dt_begin.ToString() + "," + _testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                    Console.WriteLine("{0}", content);
                    Result = false;

                    WriteLogFile(content);
                    _writeTimeOut = false;
                }

                

                return;
            }
            catch (SocketException ex)
            {

                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - _dt_begin;

                string content = _dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                Console.WriteLine("{0}", content);
                Result = false;

                WriteLogFile(content);
                WriteErrFile(ex.ToString());

            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.ToString());
                WriteErrFile(e.ToString());
            }

            //base.Execute(testID);
        }

        public override void SendErrorbySMS(string resean)
        {
            string msg = string.Format("Server disconnect: comate[{0}],step[{1}],ip[{2}] ", ConfigurationSettings.AppSettings["CarrierName"], resean, _ip);
            SendSMSToAdmin(msg);
        }

        public override void WriteTimeOutLog()
        {
            if (_writeTimeOut)
            {
                string content = _dt_begin.ToString() + "," + _testID + "," + LINK_FAILED + "," + "-1";
                Console.WriteLine("{0}", content);

                WriteLogFile(content);
            }
        }

    }
}
