using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Configuration;
using System.Xml;
using System.Collections;
using System.Threading;

namespace LinkTester
{
    class Program
    {
        private static readonly double INTERVAL = 180000;
        //private static int ErrorCount = 0;
        private static Dictionary<int, int> ErrorCountList = new Dictionary<int, int>();

        [MTAThread]
        static void Main(string[] args)
        {
            DoCheck();

            System.Timers.Timer tr = new System.Timers.Timer(INTERVAL);
            tr.Elapsed += new ElapsedEventHandler(tr_Elapsed);
            tr.AutoReset = true;
            tr.Enabled = true;

            bool run = true;
            while (run)
            {
                string line = Console.ReadLine();
                switch (line)
                {
                    case "stop":
                        tr.Close();
                        run = false;
                        break;
                }
            }
            return;
        }

        static void tr_Elapsed(object sender, ElapsedEventArgs e)
        {
            DoCheck();
        }

        static void DoCheck()
        {
            DateTime dt = DateTime.Now;
            Console.WriteLine("CHECK {0}", dt.ToString());

            XmlDocument xDoc = new XmlDocument();

            string filePath = AppDomain.CurrentDomain.BaseDirectory.ToString() + "testcase.xml";
            xDoc.Load(filePath);

            XmlElement root = xDoc.DocumentElement;

            IEnumerator ienum = root.GetEnumerator();
            XmlElement testcase;
            bool Result = true;
            string Resean = null;
            bool flag = true;
            while (ienum.MoveNext())
            {
                testcase = (XmlElement)ienum.Current;

                string strid = testcase.GetAttribute("id");
                string type = testcase.GetAttribute("type");
                string dec = testcase.GetAttribute("dec");
                int id = Convert.ToInt32(strid);

                if ("tcpRSAlogin" == type)
                {
                    if (!ErrorCountList.ContainsKey(id))
                    {
                        ErrorCountList.Add(id, 0);
                    }
                }
                
                CTesterFactory TesterFactory = new CTesterFactory();
                TesterFactory.ClassType = type;

                CBaseTester tester = TesterFactory.CreateInstance(testcase);

                tester.Execute(id);
                if (flag && !tester.Result)
                {
                    switch(type)
                    {
                        case "weblogin":
                            Resean = "1";
                            break;
                        case "webRSAlogin":
                            Resean = "2";
                            break;
                        case "tcpRSAlogin":
                            Resean = "3";
                            break;
                        default:
                            Resean = "0";
                            break;
                    }

                    flag = false;
                }


                switch (type)
                {
                    case "weblogin":
                    case "webRSAlogin":
                        Result = Result && tester.Result;
                        break;
                    case "tcpRSAlogin":
                        Thread.Sleep(20000);
                        Result = Result && tester.Result;
                        if (Result == false)
                        {
                            tester.WriteTimeOutLog();
                            ErrorCountList[id]++;
                            //ErrorCount++;
                            if (ErrorCountList[id] == 3)
                            {
                                tester.SendErrorbySMS(Resean);
                                ErrorCountList[id] = 0;
                            }
                            Result = true;
                        }
                        else
                        {
                            ErrorCountList[id] = 0;
                        }

                        break;
                }

            }
        }

    }
}
