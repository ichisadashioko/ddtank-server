using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.Configuration;

namespace LinkTester
{
    class CWebLoginTester:CBaseTester
    {

        private string _adr;
        private string _user;
        private string _pwd;

        public CWebLoginTester(string adr, string user, string pwd)
        {
            _adr = adr;
            _user = user;
            _pwd = pwd; 
        }

        public override int Execute(int testID)
        {
            DateTime dt_begin = DateTime.Now;

            try
            {
                int rst = 1;
                int ntime = ConvertDateTimeInt(DateTime.Now);
                string strtime = ntime.ToString();

                string key = ConfigurationSettings.AppSettings["Key"];
                
                string strmd5 = md5(_user + _pwd + strtime + key);

                string strdata = _adr + "?content=" + HttpUtility.UrlEncode(_user + "|" + _pwd + "|" + strtime + "|" + strmd5);

                WebRequest wrq = WebRequest.Create(strdata);

                WebResponse wrs = wrq.GetResponse();

                Stream strm = wrs.GetResponseStream();

                StreamReader sr = new StreamReader(strm);

                string line;
                string content;

                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - dt_begin;

                if ((line = sr.ReadLine()) != null)
                {
                    if ("0" != line)
                    {
                        content = dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                        Console.WriteLine(_user + "  " + "******************************第一步web请求失败");
                        Result = false;
                        rst = 1;
                    }
                    else 
                    {
                        content = dt_begin.ToString() + "," + testID + "," + LINK_SUCCEED + "," + ts.Milliseconds.ToString();
                        //Console.WriteLine("{0}", content);
                        //Console.WriteLine(_user + "  " + "第一步web请求成功（1）");
                        Result = true;
                        rst = 0;
                    }
                }
                else
                {
                    content = dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                    //Console.WriteLine("{0}", content);
                    Console.WriteLine(_user + "  " + "***************第一步web请求失败");
                    Result = false;
                    rst = 1;
                }

                
                WriteLogFile(content);

                sr.Close();
                strm.Close();
                wrs.Close();

                return rst;
            }
            catch (WebException ex)
            {
                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - dt_begin;

                string content = dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                //Console.WriteLine("{0}", content);
                Console.WriteLine(_user + "  " + "*******第一步web请求失败");
                Result = false;

                WriteLogFile(content);
                WriteErrFile(ex.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.ToString());
                WriteErrFile(e.ToString());
            }

            return 1;
        }

        public static int ConvertDateTimeInt(System.DateTime time)
        {
            double intResult = 0;
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            intResult = (time - startTime).TotalSeconds;
            return (int)intResult;
        }

        public static string md5(string str)
        {
            return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(str, "md5").ToLower();
        }
    }
}
