using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Configuration;

namespace LinkTester
{
    class CTesterFactory
    {
        public CTesterFactory()
        {
        }

        public CBaseTester CreateInstance(XmlElement testCase)
        {
            XmlNodeList infoList = testCase.ChildNodes;

            string ip = "";
            string port = "";
            string adr = "";
            string user = "";
            string pwd = "";
            

            switch (_classType)
            {
                case "weblogin":

                    foreach (XmlNode n in infoList)
                    {
                        string name = n.LocalName;
                        switch (name)
                        {
                            case "adr":
                                adr = n.InnerText;
                                break;
                            case "user":
                                user = n.InnerText;
                                break;
                            case "password":
                                pwd = n.InnerText;
                                break;
                            default:
                                //null
                                break;

                        }
                    }

                    return new CWebLoginTester(adr, user, pwd );
                    break;

                case "webRSAlogin":

                    foreach (XmlNode n in infoList)
                    {
                        string name = n.LocalName;
                        switch (name)
                        {
                            case "adr":
                                adr = n.InnerText;
                                break;
                            case "user":
                                user = n.InnerText;
                                break;
                            case "password":
                                pwd = n.InnerText;
                                break;
                            default:
                                //null
                                break;

                        }
                    }

                    return new CWebRSALoginTester(adr, user, pwd);
                    break;

                case "tcpRSAlogin":

                    foreach (XmlNode n in infoList)
                    {
                        string name = n.LocalName;
                        switch (name)
                        {
                            case "ip":
                                ip = n.InnerText;
                                break;
                            case "port":
                                port = n.InnerText;
                                break;
                            case "user":
                                user = n.InnerText;
                                break;
                            case "password":
                                pwd = n.InnerText;
                                break;
                            default:
                                //null
                                break;

                        }
                    }

                    //return new CTCPRSALoginTester(ip, port, user, CWebRSALoginTester.InnerPassWord);
                    return new CBaseTester();
                    break;

                default:
                    return new CBaseTester();
                    break;
            }

        }

        public string ClassType
        {
            set
            {
                _classType = value;
            }
        }

        private string _classType;
    }
}
