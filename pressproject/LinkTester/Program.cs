using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Configuration;
using System.Xml;
using System.Collections;
using System.Threading;
using log4net.Config;
using System.IO;
using Game.Base;
using System.Reflection;

namespace LinkTester
{
    class Program
    {
        private static readonly int THREADNUMBER = 1000;

        [MTAThread]
        static void Main(string[] args)
        {
            //设置日志的配置
            FileInfo logConfig = new FileInfo(ConfigurationSettings.AppSettings["LogConfigFile"]);
            //if (!logConfig.Exists)
            //{
            //    ResourceUtil.ExtractResource(logConfig.Name, logConfig.FullName, Assembly.GetAssembly(typeof(GameServer)));
            //}

            //设置日志的配置
            XmlConfigurator.ConfigureAndWatch(logConfig);

            List<CPlayer> playerList = new List<CPlayer>();


            string webadr1 = ConfigurationSettings.AppSettings["webadr1"];
            string webadr2 = ConfigurationSettings.AppSettings["webadr2"];
            string ip = ConfigurationSettings.AppSettings["ip"];
            string port = ConfigurationSettings.AppSettings["port"];

            for (int i = 0; i < THREADNUMBER; i++ )
            {
                int tempNum = 100000 + i;
                string testName = "test" + tempNum.ToString();

                CPlayer tempPlayer = new CPlayer(testName, webadr1, webadr2, ip, port);
                playerList.Add(tempPlayer);

                //tempPlayer.simulateBegin();

                //ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadTest), tempPlayer);

                if (!ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadTest), tempPlayer))
                {
                    ThreadTest(tempPlayer);
                }
            }
            
            //count++;
            //if (count > THREADNUMBER)
            //{
            //    count = 1;
            //}

            bool run = true;
            while (run)
            {
                string line = Console.ReadLine();
                switch (line)
                {
                    case "q":
                        run = false;
                        break;
                }
            }
            return;

        }

        public static void ThreadTest(Object state)
        {
            try
            {
                CPlayer tempPlayer = (CPlayer)state;
                tempPlayer.simulateBegin();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }

}
