using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Configuration;
using Game.Base.Packets;
using Game.Base;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using log4net;
using System.Reflection;
using Road.Base.Packets;

namespace LinkTester
{
    class CTCPRSALoginTester:CBaseTester
    {
        private static readonly int INTERVAL = 5000;
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private string _user;
        private string _innerpwd;
        private string _ip;
        private string _port;
        private DateTime _dt_begin;
        private int _testID;
        private bool _writeTimeOut;
        private CPlayer _Player;
        //public FSM _send_fsm;
        //public FSM _receive_fsm;

        private CLinkTesterConnector bconnector;

        private const short LOGIN = 0x01;
        private const short CODE_CREATROOM = 0x5e;

         /// <summary>
        /// 进入游戏
        /// </summary>
        private const short GAME_ROOM_LOGIN = 0x51; //GAME_LOGIN

        /// <summary>
        /// player状态改变
        /// </summary>
        private const short PLAYER_STATE = 0x57;

        /// <summary>
        /// 游戏开始
        /// </summary>
        private const short GAME_START = 0x56;

        /// <summary>
        /// 用户进入
        /// </summary>
        private const short GAME_PLAYER_ENTER = 0x52;

        private const short GAME_PLAYER_EXIT = 0x53;

        private const short GAME_ROOM_HOST = 0x61;

        private const short GAME_LOAD = 0x67;

        private const short GAME_CMD = 0x5b;

        private const short GAME_OVER = 0x5a;

        private const short GAME_TAKE_OUT = 0x6a;




        //////////////////////////////////////////////////////////////

        private const short TURN = 0x06;
        private const short PLAYFINISH = 0x70;
        private const short SUICIDE = 0x11;
        private const short FIRE_TAG = 0x60;
        private const short FIRE = 0x02;
        private const short LOAD = 0x10;


        public CTCPRSALoginTester(string ip, string port,string user, string innerpwd , CPlayer player)
        {
            _user = user;
            _innerpwd = innerpwd;
            _ip = ip;
            _port = port;
            _writeTimeOut = true;

            byte[] readbuf = new byte[1024];
            byte[] sendbuf = new byte[1024];

            bconnector = new CLinkTesterConnector(_ip, Convert.ToInt32(_port), false, readbuf, sendbuf);
            bconnector.PacketReceived += new LinkTester.CLinkTesterConnector.PacketProcessHandle(bconnector_PacketReceived);

            _Player = player;
        }

        void bconnector_PacketReceived(GSPacketIn packet)
        {
            string content;
            switch(packet.Code)
            {
                case LOGIN:
                    _writeTimeOut = false;
                    if (packet.ReadByte() == 1)
                    {
                        DateTime dt_end = DateTime.Now;
                        TimeSpan ts = dt_end - _dt_begin;

                        log.Error(_Player.account + "  " + "登陆失败");
                    }

                    if (packet.ReadByte() == 0)
                    {
                        DateTime dt_end = DateTime.Now;
                        TimeSpan ts = dt_end - _dt_begin;
                        
                        content = _dt_begin.ToString() + "," + _testID + "," + LINK_SUCCEED + "," + ts.Milliseconds.ToString();

                        log.Info(_Player.account + "  " + "登陆成功");

                        
                        _Player.id = packet.ClientID;
                        SetTimeout(INTERVAL,randomEnterRoom);
                     }
                    break;
                case GAME_ROOM_LOGIN:
                    if (packet.ReadBoolean())
                    {
                        _Player.roomType = packet.ReadByte();
                        packet.ReadInt(); //game.ID
                        packet.ReadByte(); //game.ScanTime
                        packet.ReadByte(); //game.Count
                        packet.ReadByte(); //(game.OpenState.Length - game.CloseTotal())
                        packet.ReadBoolean(); //game.Pwd != "" ? true : false
                        packet.ReadInt(); //game.MapIndex
                        packet.ReadBoolean(); //game.GameState == eGameState.FREE ? false : true
                        _Player.gameMode = packet.ReadByte(); //game.GameMode
                        packet.ReadString(); //game.Name

                        SetTimeout(INTERVAL, sendPlayerReady);

                        log.Info(_Player.account + "  " + "随机进入房间成功");
                    }
                    else
                    {
                        SetTimeout(INTERVAL, CreateRoom);
                        _Player.isHost = true;

                        log.Info(_Player.account + "  " + "创建房间");
                    }
                    break;
            
                case GAME_PLAYER_ENTER:
                    _Player.playerCount++;
                    if (!_Player.roomUserList.ContainsKey(packet.ClientID))
                    {
                        _Player.roomUserList.Add(packet.ClientID, false);
                    }

                    log.Info(_Player.account + "  " + "进入游戏房间");

                    canStart();

                    break;

                case GAME_PLAYER_EXIT:
                    if (_Player.roomUserList.ContainsKey(packet.ClientID))
                    {
                        _Player.playerCount--;
                        if (_Player.roomUserList[packet.ClientID])
                            _Player.readyCount--;
                        _Player.roomUserList.Remove(packet.ClientID);
                    }

                    log.Info(_Player.account + "  " + "退出房间");

                    canStart();

					break;
                case GAME_ROOM_HOST:
                    if (packet.ClientID == _Player.id)
                    {
                        if (_Player.ready)
                        {
                            _Player.readyCount--;
                            _Player.ready = false;
                        }
                        _Player.isHost = true;
                        log.Info(_Player.account + "  " + "更改房主");
                        
                        canStart();
                    }
                    break;
				case PLAYER_STATE:
                    int state = packet.ReadByte();
                    if (state == 1)
                    {
                        _Player.roomUserList[packet.ClientID] = true;

                        _Player.readyCount++;
                        canStart();
                    }
                    else if (state == 0)
                    {
                        _Player.roomUserList[packet.ClientID] = false;
                        _Player.readyCount--;
                        canStart();
                    }
					
					break;
                case GAME_LOAD:
                    SetTimeout(INTERVAL, sendLoadComplete);
                    break;
                case GAME_START:
					_Player.shootCount = 3;
                    int id = packet.ReadInt();
					if(id == _Player.id)
					{
                        SetTimeout(INTERVAL, shoot);
					}
					_Player.currentTurn = 1;
					break;

				case GAME_CMD:
                    int code = packet.ReadByte();
					switch(code)
					{
						case TURN:
                            _Player.currentTurn++;
                            if (packet.ClientID == _Player.id)
							{
                                SetTimeout(INTERVAL, shoot);
                            }
							break;
						case PLAYFINISH:
                            sendGameEndTurn(_Player.currentTurn);
							break;
					}
					break;
				case GAME_OVER:
                    SetTimeout(INTERVAL, playerTakeOut);

                    //Thread.Sleep(10000);
                    //log.Error(_Player.account + "  " + "断连");
                    //bconnector.Disconnect();
					break;

                default:

                    break;
            }
            
        }

        private void playerTakeOut()
        {
            sendGameTakeOut(100);
            log.Info(_Player.account + "  " + "翻牌");

            if (_Player.isHost)
            {
                canStart();
            }
            else
            {
                sendPlayerReady();
            }
        }

        private void sendGameTakeOut(int place)
        {
			GSPacketIn pkg = new GSPacketIn((byte)GAME_TAKE_OUT);
            pkg.WriteByte((byte)place);
            bconnector.SendTCP(pkg);
        }

        private void sendGameEndTurn(int turnid)
        {
			GSPacketIn pkg = new GSPacketIn((byte)GAME_CMD);
            pkg.WriteByte((byte)PLAYFINISH);
			pkg.WriteInt(turnid);
            bconnector.SendTCP(pkg);
        }

        private void shoot()
        {
            if (_Player.shootCount > 0)
            {
                sendShootTag(true, 0);
                sendGameCMDShoot(100, 100, 100, 60);
                _Player.shootCount--;
                log.Info(_Player.account + "  " + "发射炮弹");
            }
            else
            {
                playerSuicide();
            }
        }

        private void playerSuicide()
        {
            sendSuicide(20);
        }

        private void sendSuicide(int id)
        {
			GSPacketIn pkg = new GSPacketIn((byte)GAME_CMD);
            pkg.WriteByte((byte)SUICIDE);
            pkg.WriteInt(id);
            bconnector.SendTCP(pkg);
        }
        /**
         * 发射子弹　 
         *(炸弹数量，炸弹id,发射点x,发射点y,x速度，y速度)
         */
        private void sendGameCMDShoot(int x, int y, int force, int angle)
        {
			GSPacketIn pkg = new GSPacketIn((byte)GAME_CMD);
            pkg.WriteByte((byte)FIRE);
            pkg.WriteInt(x);
			pkg.WriteInt(y);
			pkg.WriteInt(force);
			pkg.WriteInt(angle);
            bconnector.SendTCP(pkg);
        }

        private void sendShootTag(bool b,int time)
        {
			GSPacketIn pkg = new GSPacketIn((byte)GAME_CMD);
            pkg.WriteByte((byte)FIRE_TAG);
            pkg.WriteBoolean(b);
            if(b)
            {
                pkg.WriteByte((byte)time);
            }

            bconnector.SendTCP(pkg);
        }

        private void sendLoadComplete()
        {
            sendLoadingProgress(1);
            sendPlayerState(2);
            log.Info(_Player.account + "  " + "加载完成");
        }

        private void sendLoadingProgress(int progress)
        {
			GSPacketIn pkg = new GSPacketIn((byte)GAME_CMD);
            pkg.WriteByte((byte)LOAD);
            pkg.WriteDouble((double)progress);
            bconnector.SendTCP(pkg);
        }

        private void canStart()
        {
            if(_Player.roomType == 1)
            {
                if (_Player.playerCount % 2 == 0 && _Player.readyCount >= _Player.playerCount - 1 && _Player.isHost)
                {
                    startGame();
                    _Player.roomUserList.Clear();
                    _Player.readyCount = 0;
                }
            }
            else
            {
                if (/*_Player.readyCount >= _Player.playerCount - 1 &&*/ _Player.isHost)
                {
                    startGame();
                    _Player.roomUserList.Clear();
                    _Player.readyCount = 0;
                }
            }
            

        }

        private void startGame()
        {
            log.Info(_Player.account + "  " + "开始游戏");
            sendGameStart();
            _Player.ready = false;
        }

        private void sendGameStart()
        {
			GSPacketIn pkg = new GSPacketIn((byte)GAME_START);
            bconnector.SendTCP(pkg);
        }

        private void sendPlayerReady()
        {
            _Player.ready = true;
            sendPlayerState(1);
        }

		public void sendPlayerState(int ready)
		{
			GSPacketIn pkg = new GSPacketIn((byte)PLAYER_STATE);
           
			pkg.WriteByte((byte)ready);
            bconnector.SendTCP(pkg);
		}

        private void CreateRoom()
        {
            GSPacketIn pkg = new GSPacketIn((byte)CODE_CREATROOM);
            pkg.WriteByte((byte)_Player.roomType);
            pkg.WriteByte((byte)_Player.gameMode);
            pkg.WriteByte(2);
            pkg.WriteString("火门三关" + "," + "");

            bconnector.SendTCP(pkg);
        }

        private void randomEnterRoom()
		{
            sendGameLogin(1, -1, "");
		}

        /**
        * 进入房间 0指定房进入  FREE = 1,
        BLANCE = 2,
        LUMP = 3,
        FLAG = 4,
        NOBLANCE = 5,
        NOFLAG = 6,
        * 
        */	
        private void sendGameLogin(int isRnd, int roomId, string pass)
        {
            GSPacketIn pkg = new GSPacketIn((byte)GAME_ROOM_LOGIN);
			pkg.WriteByte((byte)isRnd);
			if(isRnd == 0)
			{
				pkg.WriteInt(roomId);
				pkg.WriteString(pass);
			}
            bconnector.SendTCP(pkg);
        }

        public override int Execute(int testID)
        {
            _dt_begin = DateTime.Now;
            _testID = testID;

            try
            {
                string strdata = _user + "," + _innerpwd;

                short fsm_key = 101;
                byte[] bkey = BitConverter.GetBytes(fsm_key);
                Array.Reverse(bkey);

                MemoryStream msData = new MemoryStream();

                BinaryWriter writer = new BinaryWriter(msData);

                writer.Write((short)DateTime.UtcNow.Year);
                writer.Write((byte)DateTime.UtcNow.Month);
                writer.Write((byte)DateTime.UtcNow.Date.Day);
                writer.Write((byte)DateTime.UtcNow.Hour);
                writer.Write((byte)DateTime.UtcNow.Minute);
                writer.Write((byte)DateTime.UtcNow.Second);
                writer.Write(bkey[0]);
                writer.Write(bkey[1]);
                writer.Write(Encoding.UTF8.GetBytes(strdata));

                string privateKey = ConfigurationSettings.AppSettings["privateKey"];

                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(privateKey);
                byte[] data = rsa.Encrypt(msData.ToArray(), false);
                
                writer.Close();

                GSPacketIn pkg = new GSPacketIn((byte)LOGIN);

                string strEdition = ConfigurationSettings.AppSettings["Edition"];

                pkg.WriteInt(Convert.ToInt32(strEdition));
                pkg.Write(data);

                bconnector.Connect();

                if (bconnector.IsConnected)
                {
                    bconnector.SendTCP(pkg);

                    bconnector.m_processor.SetFsm(fsm_key, Convert.ToInt32(strEdition));
                    //System.Timers.Timer tr = new System.Timers.Timer(45000);
                    //tr.Elapsed += new System.Timers.ElapsedEventHandler(tr_Elapsed);
                    //tr.AutoReset = true;
                    //tr.Enabled = true;

                }
                else
                {
                    DateTime dt_end = DateTime.Now;
                    TimeSpan ts = dt_end - _dt_begin;

                    string content = _dt_begin.ToString() + "," + _testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                    Console.WriteLine(_Player.account + "  " + "连接已经被断开");
                    Result = false;

                    WriteLogFile(content);
                    _writeTimeOut = false;
                }

                

                return 0;
            }
            catch (SocketException ex)
            {

                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - _dt_begin;

                string content = _dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                Console.WriteLine(_Player.account + "  " + "连接已经被断开，异常抛出");
                Result = false;

                WriteLogFile(content);
                WriteErrFile(ex.ToString());

            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.ToString());
                WriteErrFile(e.ToString());
            }

            return 1;
        }

        //void tr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    if(bconnector.IsConnected)
        //    {
        //        log.Error(Player.account + "  " + "断连");
        //        bconnector.Disconnect();
        //    }

        //}

        public override void SendErrorbySMS()
        {
            string msg = "Server disconnect:" + _ip;
            SendSMSToAdmin(msg);
        }

        public override void WriteTimeOutLog()
        {
            if (_writeTimeOut)
            {
                string content = _dt_begin.ToString() + "," + _testID + "," + LINK_FAILED + "," + "1000";
                Console.WriteLine("{0}", content);

                WriteLogFile(content);
            }
        }

         public void SetTimeout(double interval, Action action)
         {
            System.Timers.Timer timer = new System.Timers.Timer(interval);
            timer.Elapsed += delegate(object sender, System.Timers.ElapsedEventArgs e)
            {
                timer.Enabled = false;
                action();
            };
            timer.Enabled = true;
        }

        //public void setFsm(int adder,int muliter)
        //{
        //    _send_fsm.Setup(adder, muliter);
        //    _receive_fsm.Setup(adder, muliter);
        //}


    }
}
