using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkTester
{
    //人物及房间信息
    class CPlayer
    {
        private static readonly int ROOMTYPE = 0;
        private static readonly int GAMEMODE = 1;
        private static readonly int INTERVAL = 1000*600;

        public int id;
        
        public string account;
        
		public bool isHost;
		
		public int playerCount;
		
		public int readyCount;
		
        public Dictionary<int, bool> _roomUserList;

        public object _lock;
		
		public bool ready;
		
		public int currentTurn;

        public int shootCount;
		//public int wind;

        public int roomType;

        public int gameMode;

        public CTimer timer;

        //public CWebLoginTester loginStep1;

        //public CWebRSALoginTester loginStep2;

        //public CTCPRSALoginTester loginStep3;

        private string _webadr1;

        private string _webadr2;

        private string _ip;

        private string _port;

        public CPlayer(string userAccount, string webadr1, string webadr2, string ip, string port)
        {
            id = 0;
            account = userAccount;
            isHost = false;
            playerCount = 0;
            readyCount = 0;
            ready = false;
            currentTurn = 0;
            roomType = ROOMTYPE;
            gameMode = GAMEMODE;
            _roomUserList = new Dictionary<int, bool>();
            _webadr1 = webadr1;
            _webadr2 = webadr2;
            _ip = ip;
            _port = port;
            _lock = new object();

            timer = new CTimer(this);
            timer.BeginTimer(INTERVAL);
        }

        public Dictionary<int, bool> roomUserList
        {
            set
            {
                _roomUserList = value;
            }
            get
            {
                return _roomUserList;
            }
        
        }

        public void simulateBegin()
        {
            DateTime dt = DateTime.Now;

            //Console.WriteLine(testName);
            CWebLoginTester ObjWebLoginTester = new CWebLoginTester(_webadr1, account, account);
            //Console.WriteLine("CWebLogin({0}, {1}, {2})", webadr1, testName, testName);
            int rst1 = ObjWebLoginTester.Execute(1);

            if (rst1 != 0)
            {
                return;
            }
            string innerPassWord = "";
            CWebRSALoginTester ObjWebRSALoginTester = new CWebRSALoginTester(_webadr2, account, account);
            //Console.WriteLine("CWebRSALogin({0}, {1}, {2})", webadr2, testName, testName);
            int rst2 = ObjWebRSALoginTester.Execute(2, ref innerPassWord);

            if(rst2 != 0)
            {
                return;
            }

            CTCPRSALoginTester ObjTCPRSALoginTester = new CTCPRSALoginTester(_ip, _port, account, innerPassWord, this);
            //Console.WriteLine("CTCPRSALogin({0}, {1}, {2},{3})", ip, port, testName, innerPassWord);
            ObjTCPRSALoginTester.Execute(3);
            //Thread.Sleep(50000);

        }
    }
}
