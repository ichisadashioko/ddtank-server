using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;
using System.Reflection;

namespace LinkTester
{
    class CTimer
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private Timer _timer;
        private CPlayer _player;

        public CTimer(CPlayer player)
        {
            _player = player;
        }

        public void BeginTimer(int interval)
        {
            if (_timer == null)
            {
                _timer = new Timer(new TimerCallback(OnTick), null, interval, interval);
            }
            else
            {
                _timer.Change(interval, interval);
            }
        }

        public void StopTimer()
        {
            if (_timer != null)
            {
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
                //_timer.Dispose();
                //_timer = null;
            }
        }

        protected void OnTick(object obj)
        {
            _player.simulateBegin();
        }
    }
}
