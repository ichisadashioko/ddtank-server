using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Base;
using Game.Base.Packets;

namespace LinkTester
{
    class CLinkTesterConnector : BaseConnector
    {
        public delegate void PacketProcessHandle(GSPacketIn packet);
        public event PacketProcessHandle PacketReceived;

        public CLinkTesterConnector(string ip, int port, bool autoReconnect, byte[] readBuffer, byte[] sendBuffer)
            : base(ip, port, autoReconnect, readBuffer, sendBuffer)
        {
            Encryted = true;
        }

        public override void OnRecvPacket(Game.Base.Packets.GSPacketIn pkg)
        {
            if (PacketReceived != null)
            {   
                PacketReceived(pkg);
            }
        }


    }
}
