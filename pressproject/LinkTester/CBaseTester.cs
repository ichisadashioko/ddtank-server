using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web;

namespace LinkTester
{
    class CBaseTester
    {
        public static readonly int LINK_SUCCEED = 0;
        public static readonly int LINK_FAILED = 1;
        
        public CBaseTester()
        {
            _logPath = ConfigurationSettings.AppSettings["logpath"];
            _ErrPath = ConfigurationSettings.AppSettings["Errpath"];
            _testResult = false;
            _myNum = ConfigurationSettings.AppSettings["SmsNumSender"];
            _myPass = ConfigurationSettings.AppSettings["SmsPassWord"];
            string strNum = ConfigurationSettings.AppSettings["SmsNumReciever"];
            string[] tmp = strNum.Split('|');
            _toNumList = tmp.ToList<string>();
        }

        public virtual int Execute(int testID)
        {
            return 0;
        }

        public void WriteLogFile(string content)
        {
            //using (FileStream fs = File.Open(_logPath, FileMode.Append))
            //{
            //    using (StreamWriter writer = new StreamWriter(fs))
            //    {
            //        writer.WriteLine("{0}", content);
            //    }
            //}
            return;
        }

        public void WriteErrFile(string content)
        {
            //using (FileStream fs = File.Open(_ErrPath, FileMode.Append))
            //{
            //    using (StreamWriter writer = new StreamWriter(fs))
            //    {
            //        writer.WriteLine("{0}", content);
            //    }
            //}
            return;
        }

        public bool Result
        {
            get { return _testResult; }
            set { _testResult = value; }
        }

        public void SendSMSToAdmin(string Msg)
        {
            //string msg = HttpUtility.UrlEncode(Msg);

            //foreach(string revNum in _toNumList)
            //{
            //    string url = string.Format("http://www.websafeguard.cn/action/sendfetion?sfetion={0}&password={1}&tfetion={2}&message={3}", _myNum, _myPass, revNum, msg);
            //    //Console.WriteLine("Request:");
            //    //Console.WriteLine(url);
            //    HttpWebResponse response = (HttpWebResponse)(WebRequest.Create(url).GetResponse());
            //    response.Close();
            //    //Console.WriteLine("Send success!");
            //}

        }

        public virtual void SendErrorbySMS()
        {

        }

        public virtual void WriteTimeOutLog()
        { 
            
        }

        private string _logPath;
        private string _ErrPath;
        private bool _testResult;
        private string _myNum;
        private string _myPass;
        private List<string> _toNumList;


        //internal void Execute(string id)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
