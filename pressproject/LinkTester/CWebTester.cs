using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using System.IO;

namespace LinkTester
{
    class CWebTester:CBaseTester
    {
        public CWebTester(string webadress)
        {
            _webadr = webadress;
        }

        public override void Execute(int testID)
        {

            DateTime dt_begin = DateTime.Now;

            try
            {
                WebRequest wrq = WebRequest.Create(_webadr);

                WebResponse wrs = wrq.GetResponse();

                Stream strm = wrs.GetResponseStream();

                StreamReader sr = new StreamReader(strm);

                string line;
                string content;

                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - dt_begin;

                if ((line = sr.ReadLine()) != null)
                {
                    content = dt_begin.ToString() + "," + testID + "," + LINK_SUCCEED + "," + ts.Milliseconds.ToString();
                }
                else
                {
                    content = dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                    Console.WriteLine("{0}", content);
                }

                WriteLogFile(content);
                strm.Close();
            }
            catch (WebException ex)
            {
                DateTime dt_end = DateTime.Now;
                TimeSpan ts = dt_end - dt_begin;
                
                string content = dt_begin.ToString() + "," + testID + "," + LINK_FAILED + "," + ts.Milliseconds.ToString();
                Console.WriteLine("{0}", content);

                WriteLogFile(content);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.ToString());
                WriteErrFile(e.ToString());
            }
            //base.Execute();
        }

        private string _webadr;
    }
}
