using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Net.Sockets;
using System.Collections;
using Game.Base.Packets;

namespace Game.Base
{
    public delegate void ClientEventHandle(BaseClient client);

    public class BaseClient
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected object _syncStop = new object();

        /// <summary>
        /// Socket that holds the client connection
        /// </summary>
        protected Socket _sock;

        /// <summary>
        /// Packet buffer, holds incoming packet data
        /// </summary>
        protected byte[] m_readBuffer;

        /// <summary>
        /// Current offset into the buffer
        /// </summary>
        protected int m_readBufEnd;

        /// <summary>
        /// Gets or sets the socket the client is using
        /// </summary>
        public Socket Socket
        {
            get
            {
                return _sock;
            }
            set
            {
                _sock = value;
            }
        }

        /// <summary>
        /// Gets the packet buffer for the client
        /// </summary>
        public byte[] PacketBuf
        {
            get
            {
                return m_readBuffer;
            }
        }

        /// <summary>
        /// The remote client is connected
        /// </summary>
        private bool _isConnected;

        public bool IsConnected
        {
            get { return _isConnected; }
        }

        /// <summary>
        /// Gets or sets the offset into the receive buffer
        /// </summary>
        public int PacketBufSize
        {
            get { return m_readBufEnd; }
            set { m_readBufEnd = value; }
        }

        /// <summary>
        /// Gets the client's TCP endpoint string, if connected
        /// </summary>
        public string TcpEndpoint
        {
            get
            {
                Socket s = _sock;
                if (s != null && s.Connected && s.RemoteEndPoint != null)
                    return s.RemoteEndPoint.ToString();
                return "not connected";
            }
        }

        /// <summary>
        /// Send buffer
        /// </summary>
        protected byte[] m_sendBuffer;

        public byte[] SendBuffer
        {
            get
            {
                return m_sendBuffer;
            }
        }

        /// <summary>
        /// Stream buffer processor.
        /// </summary>
        protected StreamProcessor _processor;

        public event ClientEventHandle Connected;

        public event ClientEventHandle Disconnected;

        /// <summary>
        /// Called when data has been received from the connection
        /// </summary>
        /// <param name="num_bytes">Number of bytes received in m_pbuf</param>
        public virtual void OnRecv(int num_bytes) 
        {
            _processor.ReceiveBytes(num_bytes);
        }

        /// <summary>
        /// Called wehn pkg has been dispath from the received data
        /// </summary>
        /// <param name="pkg"></param>
        public virtual void OnRecvPacket(GSPacketIn pkg) { }

        /// <summary>
        /// Called after the client connection has been accepted
        /// </summary>
        public virtual void OnConnect() 
        {
            _isConnected = true;

            if (_processor == null)
            {
                _processor = new StreamProcessor(this);
            }
            if (Connected != null)
            {
                Connected(this);
            }
        }

        /// <summary>
        /// Called right after the client has been disconnected
        /// </summary>
        public virtual void OnDisconnect() 
        {
            if (Disconnected != null)
            {
                Disconnected(this);
            }
        }



        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="srvr">Pointer to the server the client is connected to</param>
        public BaseClient(byte[] readBuffer,byte[] sendBuffer)
        {
            m_readBuffer = readBuffer;
            m_sendBuffer = sendBuffer;
            m_readBufEnd = 0;
        }

        /// <summary>
        /// Bengin a asynchnorous receive data call.It will add callback function to e.Completed event.
        /// </summary>
        /// <param name="e"></param>
        public void ReceiveAsync(SocketAsyncEventArgs e)
        {
            //Add callback function.
            e.Completed += RecvEventCallback;

            ReceiveAsyncImp(e);
        }

        /// <summary>
        /// Implements a asynchnorous receive data call.
        /// </summary>
        /// <param name="e"></param>
        private void ReceiveAsyncImp(SocketAsyncEventArgs e)
        {
            if (_sock != null && _sock.Connected)
            {
                int bufSize = m_readBuffer.Length;
                if (m_readBufEnd >= bufSize) //Do we have space to receive?
                {
                    if (log.IsErrorEnabled)
                    {
                        log.Error(TcpEndpoint + " disconnected because of buffer overflow!");
                        log.Error("m_pBufEnd=" + m_readBufEnd + "; buf size=" + bufSize);
                        log.Error(m_readBuffer);
                    }
                    Disconnect();
                }
                else      
                {
                    e.SetBuffer(m_readBuffer, m_readBufEnd, bufSize - m_readBufEnd);
                    if (!_sock.ReceiveAsync(e))
                    {
                        RecvEventCallback(_sock, e);
                    }
                }
            }
            else
            {
                Disconnect();
            }
        }

        /// <summary>
        /// SocketAsyncEvent.Completed event callback function.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecvEventCallback(object sender, SocketAsyncEventArgs e)
        {
            try
            {
                int num_bytes = e.BytesTransferred;

                if (num_bytes > 0)
                {
                    this.OnRecv(num_bytes);
                    this.ReceiveAsyncImp(e);
                }
                else
                {
                    log.DebugFormat("Disconnecting client ({0}), received bytes={1}", TcpEndpoint, num_bytes);
                    Disconnect();
                }
            }
            catch (Exception ex)
            {   
                log.ErrorFormat("{0} RecvCallback:{1}", TcpEndpoint,ex);
                Disconnect();
            }
        }

        public void SendTCP(GSPacketIn pkg)
        {
            _processor.SendTCP(pkg);
        }

        public void SendAsync(SocketAsyncEventArgs e)
        {
            int start = Environment.TickCount;
            log.Debug(string.Format("Send To ({0}) {1} bytes", TcpEndpoint, e.Count));
            
            bool result = false;
            if (_sock.Connected)
            {
                result = _sock.SendAsync(e);
            }

            int took = Environment.TickCount - start;
            if (took > 100)
                log.WarnFormat("AsyncTcpSendCallback.BeginSend took {0}ms! (TCP to client: {1})", took,TcpEndpoint);

            if (!result)
            {
                Disconnect();
            }
        }


        /// <summary>
        /// Closes the client connection
        /// </summary>
        protected void CloseConnections()
        {
            if (_sock != null)
            {
                try { _sock.Shutdown(SocketShutdown.Both); }
                catch { }
                try { _sock.Close(); }
                catch { }
            }
        }

        /// <summary>
        /// Closes the client connection
        /// </summary>
        public void Disconnect()
        {
            try
            {
                if (_isConnected)
                {
                    _isConnected = false;
                    CloseConnections();
                    OnDisconnect();
                }
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("Exception", e);
            }
        }
    }
}
