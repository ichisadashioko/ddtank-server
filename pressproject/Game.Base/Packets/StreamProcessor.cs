using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Reflection;
using System.Collections;
using Game.Base;
using System.Timers;
using System.Net.Sockets;
using System.Threading;
using Game.Base.Events;
using Game.Base.Packets;

namespace Game.Base.Packets
{
    /// <summary>
    /// This class handles the packets, receiving and sending
    /// </summary>
    public class StreamProcessor
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Holds the current client for this processor
        /// </summary>
        protected readonly BaseClient m_client;

        protected SocketAsyncEventArgs e;

        /// <summary>
        /// Constructs a new PacketProcessor
        /// </summary>
        /// <param name="client">The processor client</param>
        public StreamProcessor(BaseClient client)
        {
            m_client = client;

            e = new SocketAsyncEventArgs();
            e.Completed += AsyncTcpSendCallback;
            m_tcpSendBuffer = client.SendBuffer;
        }


        #region Send TCP Package

        /// <summary>
        /// Holds the TCP send buffer
        /// </summary>
        protected byte[] m_tcpSendBuffer;

        /// <summary>
        /// The client TCP packet send queue
        /// </summary>
        protected readonly Queue m_tcpQueue = new Queue(256);

        /// <summary>
        /// Indicates whether data is currently being sent to the client
        /// </summary>
        protected bool m_sendingTcp;

        /// <summary>
        /// 
        /// </summary>
        protected int m_firstPkgOffset = 0;

        /// <summary>
        /// Sends a packet via TCP
        /// </summary>
        /// <param name="packet">The packet to be sent</param>
        public void SendTCP(GSPacketIn packet)
        {

            //Fix the packet size
            packet.WriteHeader();

            //reset the offset for read
            packet.Offset = 0;

            //Check if client is connected
            if (m_client.Socket.Connected)
            {
                try
                {
                    Statistics.BytesOut += packet.Length;
                    Statistics.PacketsOut++;

                    if (log.IsDebugEnabled)
                    {
                        log.Debug(Marshal.ToHexDump(string.Format("Send Pkg to {0} :", m_client.TcpEndpoint), packet.Buffer, 0, packet.Length));
                        //log.Debug(string.Format("Send Pkg to {0}     {1} bytes", m_client.TcpEndpoint, packet.Length));
                    }

                    lock (m_tcpQueue.SyncRoot)
                    {
                        if (m_sendingTcp)
                        {
                            //log.Debug(string.Format("{0} : En Queue Pkg", m_client.TcpEndpoint));
                            m_tcpQueue.Enqueue(packet);
                            return;
                        }
                        else
                        {
                            m_sendingTcp = true;
                        }
                    }

                    //packet会在多个线程中使用,这里不能用Read,只能用CopyTo,CopyTo不会改变pakcet的读的位置
                    int count = packet.CopyTo(m_tcpSendBuffer, 0, 0);
                    if (packet.DataLeft - count > 0)
                    {
                        lock (m_tcpQueue.SyncRoot)
                        {
                            m_tcpQueue.Enqueue(packet);
                            m_firstPkgOffset = count;
                        }
                    }

                    e.SetBuffer(m_tcpSendBuffer,0,count);
                    e.UserToken = this;
                    m_client.SendAsync(e);
                }
                catch (Exception e)
                {
                    log.WarnFormat("It seems <{0}> went linkdead. Closing connection. (SendTCP, {1}: {2})", m_client, e.GetType(), e.Message);
                    m_client.Disconnect();
                }
            }
        }


        /// <summary>
        /// Callback method for async sends
        /// </summary>
        /// <param name="ar"></param>
        protected static void AsyncTcpSendCallback(object sender, SocketAsyncEventArgs e)
        {
            StreamProcessor proc = (StreamProcessor)e.UserToken;
            BaseClient client = proc.m_client;
            try
            {
                Queue q = proc.m_tcpQueue;

                int sent = e.BytesTransferred;

                int count = 0;
                byte[] data = proc.m_tcpSendBuffer;
                int firstOffset = proc.m_firstPkgOffset;

                lock (q.SyncRoot)
                {
                    if (q.Count > 0)
                    {
                        do
                        {
                            PacketIn pak = (PacketIn)q.Peek();

                            int len = pak.CopyTo(data, count, firstOffset);

                            firstOffset += len;
                            count += len;

                            if (pak.DataLeft - firstOffset == 0)
                            {
                                q.Dequeue();
                                firstOffset = 0;
                            }
                            if (data.Length == count)
                            {
                                break;
                            }
                        } while (q.Count > 0);

                    }
                    if (count <= 0)
                    {
                        proc.m_sendingTcp = false;
                        return;
                    }
                    proc.m_firstPkgOffset = firstOffset;
                }

                e.SetBuffer(proc.m_tcpSendBuffer, 0, count);
                e.UserToken = proc;
                client.SendAsync(e);
            }
            catch (Exception ex)
            {
                log.WarnFormat("It seems <{0}> went linkdead. Closing connection. (SendTCP, {1}: {2})", client, ex.GetType(), ex.Message);
                client.Disconnect();
            }
        }


        #endregion

        #region Receive Package
        /// <summary>
        /// Called when the client receives bytes
        /// </summary>
        /// <param name="numBytes">The number of bytes received</param>
        public void ReceiveBytes(int numBytes)
        {
            lock (this)
            {
                byte[] buffer = m_client.PacketBuf;

                //End Offset of buffer
                int bufferSize = m_client.PacketBufSize + numBytes;
                log.Debug(Marshal.ToHexDump("Recieve:", buffer, 0, bufferSize));
                //Size < minimum
                if (bufferSize < GSPacketIn.HDR_SIZE)
                {
                    m_client.PacketBufSize = bufferSize; // undo buffer read
                    return;
                }
                //Reset the offset
                m_client.PacketBufSize = 0;

                int curOffset = 0;

                do
                {
                    //read buffer length
                    int packetLength = (buffer[curOffset] << 8) + buffer[curOffset + 1];
                    int dataLeft = bufferSize - curOffset;

                    if (packetLength < GSPacketIn.HDR_SIZE || packetLength > bufferSize)
                    {
                        log.ErrorFormat("Err pkg from {0}:", m_client.TcpEndpoint);
                        log.Error(Marshal.ToHexDump("===> error buffer", buffer));
                        m_client.PacketBufSize = 0;
                        m_client.Disconnect();
                        break;
                    }

                    if (dataLeft < packetLength)
                    {
                        Array.Copy(buffer, curOffset, buffer, 0, dataLeft);
                        m_client.PacketBufSize = dataLeft;
                        break;
                    }

                    byte[] curPacket = new byte[2048];
                    Array.Copy(buffer, curOffset, curPacket, 0, packetLength);

                    GSPacketIn pak = new GSPacketIn(curPacket, packetLength);

                    try
                    {
                        m_client.OnRecvPacket(pak);
                    }
                    catch (Exception e)
                    {
                        if (log.IsErrorEnabled)
                            log.Error("HandlePacket(pak)", e);
                    }

                    curOffset += packetLength;

                } while (bufferSize - 1 > curOffset);

                if (bufferSize - 1 == curOffset)
                {
                    buffer[0] = buffer[curOffset];
                    m_client.PacketBufSize = 1;
                }
            }
        }

        #endregion

    }
}
