using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Base;
using log4net;
using System.Reflection;

namespace Game.Base.Packets
{
    /// <summary>
    /// Game server specific packet
    /// </summary>
    public class GSPacketIn : PacketIn
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Header size including checksum at the end of the packet
        /// </summary>
        public const ushort HDR_SIZE = 8;
        
        /// <summary>
        /// Porotocal ID
        /// </summary>
        protected short m_code;
        /// <summary>
        /// Client ID
        /// </summary>
        protected int m_cliendId;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="size">Size of the internal buffer</param>
        public GSPacketIn(byte[] buf,int size)
            : base(buf,size)
        {
            ReadHeader();
        }

        public GSPacketIn(short code):this(code,0,2048)
        {
        }

        public GSPacketIn(short code, int clientId):this(code,clientId,2048)
        {
 
        }

        public GSPacketIn(short code, int clientId, int size)
            : base(new byte[size],8)
        {
            m_code = code;
            m_cliendId = clientId;
            _offset = 8;
        }

        /// <summary>
        /// Protocal
        /// </summary>
        public short Code
        {
            get { return m_code; }
            set { m_code = value; }
        }

        /// <summary>
        /// Gets the Client id
        /// </summary>
        public int ClientID
        {
            get { return m_cliendId; }
            set { m_cliendId = value; }
        }

        /// <summary>
        /// 读
        /// </summary>
        public void ReadHeader()
        {
            _length = ReadShort();
            m_code = ReadShort();
            m_cliendId = ReadInt();
        }

        public void WriteHeader()
        {
            int old = _offset;
            _offset = 0;
            base.WriteShort((short)_length); //reserved for size
            base.WriteShort(m_code);
            base.WriteInt(m_cliendId);
            _offset = old;
        }

        public void Compress()
        {
            //_buffer = Marshal.Compress(_buffer, HDR_SIZE, Length - HDR_SIZE);
            //_length = _buffer.Length + HDR_SIZE;
            //WriteHeader();


            byte[] temp = Marshal.Compress(_buffer, HDR_SIZE, Length - HDR_SIZE);
            _offset = HDR_SIZE;
            Write(temp);
            _length = temp.Length + HDR_SIZE;
        }

        public void UnCompress()
        {
 
        }

        public void ClearContext()
        {
            _offset = 8;
            _length = 8;
        }

        public GSPacketIn Clone()
        {
            //return new GSPacketIn(_buffer, _length);
            GSPacketIn pkg = new GSPacketIn(_buffer, _length);
            pkg.Offset = _length;
            return pkg;
        }

    }
}
