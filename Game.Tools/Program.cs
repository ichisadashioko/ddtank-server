using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bussiness.Managers;
using Game.Logic;
using SqlDataProvider.Data;
using System.Diagnostics;

namespace Game.Tools
{
    class Program
    {
        static void Main(string[] args)
        {
            GC.Collect();
            DropMgr.Init();
            System.Diagnostics.Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start(); //  开始监视代码运行时间            
            List<ItemInfo> tempItem=null;
            for (int i = 0; i < 100000; i++)
            {
                DropInventory.PvEQuestsDrop(1, ref tempItem);                
                DropInventory.NPCDrop(3, ref tempItem);                
                DropInventory.CopyDrop(1071,1, ref tempItem);                
            }
            stopwatch.Stop(); //  停止监视
            TimeSpan timespan = stopwatch.Elapsed; //  获取当前实例测量得出的总时间          
            double seconds = timespan.TotalSeconds;  //  总秒数
            double milliseconds = timespan.TotalMilliseconds;  //  总毫秒数 
            Console.WriteLine("完毕" + seconds.ToString()+"秒");
            Console.Read();
             
        }
 

    }
}
