using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Server.GameObjects;
using System.Collections;
using Game.Base.Packets;

namespace Game.Server.Packets.Server
{
    public interface IPacketLib
    {
        byte GetPacketCode(ePackageType packetCode);
        void SendTCP(GSTCPPacketOut packet);
        void SendLoginSuccess(GamePlayer player);
        void SendLoginFaild(string msg);
        void SendKitoff();
        void SendAddFriend(Guid friend, bool result);
        GSTCPPacketOut SendUpdateNickname(string name);
        GSTCPPacketOut SendDateTime();
        GSTCPPacketOut SendFriendLogin(GamePlayer player);
        GSTCPPacketOut SendFriendLogout(GamePlayer player);
        GSTCPPacketOut SendFriendState(GamePlayer player,bool state);
        GSTCPPacketOut SendFriendAdd(GamePlayer player);
        GSTCPPacketOut SendFriendRemove(int FriendID);

        void SendRSAKey(byte[] m, byte[] e);

    }
}
