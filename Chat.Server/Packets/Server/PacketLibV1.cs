using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Bussiness;
using SqlDataProvider.Data;
using Game.Server.GameObjects;
using Game.Base;
using Game.Base.Packets;

namespace Game.Server.Packets.Server
{
    [PacketLib(1)]
    public class PacketLibV1 : AbstractPacketLib
    {
        public PacketLibV1(GameClient client) : base(client) { }

        public override void SendLoginFaild(string msg)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.LOGIN);
            pkg.WriteByte(0);
            pkg.WriteString(msg);
            //pkg.Compress();
            SendTCP(pkg);
        }

        public override void SendLoginSuccess(GamePlayer player)
        {
            PlayerInfo user = player.PlayerCharacter;

            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.LOGIN);
            pkg.WriteByte(1);
            XElement node = new XElement("player", new XAttribute("ID", user.ID),
                                                  new XAttribute("NickName", user.NickName),
                                                  //new XAttribute("Sex", user.Sex),
                                                  //new XAttribute("Email", user.Email),
                                                  //new XAttribute("Win", user.Win),
                                                  //new XAttribute("Lost", user.Lost),
                                                  //new XAttribute("Escape", user.Escape),
                                                  //new XAttribute("Grade", user.Grade),
                                                  //new XAttribute("Score", user.Score),
                                                  new XAttribute("Money", user.Money));  
            pkg.WriteString(node.ToString(false));
            //pkg.Compress();
            SendTCP(pkg);
        }

        public override void SendKitoff()
        {
            SendTCP(new GSTCPPacketOut((byte)ePackageType.KIT_USER));
        }

        public override GSTCPPacketOut SendPlayerState(int id, byte state)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.CHANGE_STATE, id);
            pkg.WriteByte(state);

            SendTCP(pkg);
            return pkg;
        }

        public override GSTCPPacketOut SendFriendState(GamePlayer player,bool state)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.FRIEND_STATE, player.PlayerCharacter.ID);
            pkg.WriteBoolean(!state);

            PlayerBussiness db = new PlayerBussiness();
            int[] friends = db.GetFriendsIDAll(player.PlayerCharacter.ID);


            //TankDBDataContext db = GameServer.CreateQueryDb();
            //var friends = from f in db.Sys_Users_Friends
            //              join u in db.Sys_Users_Details on f.UserID equals u.UserID
            //              where f.FriendID ==  player.PlayerCharacter.ID && u.State != 0
            //              select u.UserID;

            foreach (int f in friends)
            {
                GamePlayer friend = Worlds.WorldMgr.GetClientFromID(f);
                if (friend != null)
                {
                    friend.Out.SendTCP(pkg);
                }
            }
            return pkg;
        }


    }
}
