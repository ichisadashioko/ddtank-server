using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Reflection;
using System.Xml.Linq;
using Game.Server.GameObjects;
using Game.Server.Managers;
using Game.Base.Packets;

namespace Game.Server.Packets.Server
{
    public abstract class AbstractPacketLib:IPacketLib
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The GameClient of this PacketLib
        /// </summary>
        protected readonly GameClient m_gameClient;

        /// <summary>
        /// Constructs a new PacketLib
        /// </summary>
        /// <param name="client">the gameclient this lib is associated with</param>
        public AbstractPacketLib(GameClient client)
        {
            m_gameClient = client;
        }

        /// <summary>
        /// Retrieves the packet code depending on client version
        /// </summary>
        /// <param name="packetCode"></param>
        /// <returns></returns>
        public virtual byte GetPacketCode(ePackageType packetCode)
        {
            return (byte)packetCode;
        }

        /// <summary>
        /// Sends a packet via TCP
        /// </summary>
        /// <param name="packet">The packet to be sent</param>
        public void SendTCP(GSTCPPacketOut packet)
        {
            m_gameClient.SendTCP(packet);
        }

        /// <summary>
        /// Finds and creates packetlib for specified raw version.
        /// </summary>
        /// <param name="rawVersion">The version number sent by the client.</param>
        /// <param name="client">The client for which to create packet lib.</param>
        /// <param name="version">The client version of packetlib.</param>
        /// <returns>null if not found or new packetlib instance.</returns>
        public static IPacketLib CreatePacketLibForVersion(int rawVersion, GameClient client)
        {
            foreach (Type t in ScriptMgr.GetDerivedClasses(typeof(IPacketLib)))
            {
                foreach (PacketLibAttribute attr in t.GetCustomAttributes(typeof(PacketLibAttribute), false))
                {
                    if (attr.RawVersion == rawVersion)
                    {
                        try
                        {
                            IPacketLib lib = (IPacketLib)Activator.CreateInstance(t, new object[] { client });
                            return lib;
                        }
                        catch (Exception e)
                        {
                            if (log.IsErrorEnabled)
                                log.Error("error creating packetlib (" + t.FullName + ") for raw version " + rawVersion, e);
                        }
                    }
                }
            }
            return null;
        }

        public virtual void SendLoginSuccess(Game.Server.GameObjects.GamePlayer player)
        {
            throw new NotImplementedException();
        }

        public virtual void SendLoginFaild(string msg)
        {
            throw new NotImplementedException();
        }

        public virtual void SendKitoff()
        {
            throw new NotImplementedException();
        }

        public virtual GSTCPPacketOut SendPlayerState(int id, byte state)
        {
            throw new NotImplementedException();
        }

        public virtual void SendAddFriend(Guid friend, bool result)
        {
            throw new NotImplementedException();
        }

        public GSTCPPacketOut SendDateTime()
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.SYS_DATE);
            pkg.WriteDateTime(DateTime.Now);
            SendTCP(pkg);
            return pkg;
        }

        public GSTCPPacketOut SendUpdateNickname(string name)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.CHANGE_NICKNAME, m_gameClient.Player.PlayerCharacter.ID);
            pkg.WriteString(name);
            SendTCP(pkg);
            return pkg;
        }

        public GSTCPPacketOut SendFriendLogin(GamePlayer player)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.FRIEND_LOGIN,player.PlayerCharacter.ID);
            SendTCP(pkg);
            return pkg;            
        }

        public GSTCPPacketOut SendFriendLogout(GamePlayer player)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.FRIEND_LOGOUT, player.PlayerCharacter.ID);
            SendTCP(pkg);
            return pkg;
        }

        public virtual GSTCPPacketOut SendFriendState(GamePlayer player,bool state)
        {
            throw new NotImplementedException();
        }

        public GSTCPPacketOut SendFriendAdd(GamePlayer player)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.FRIEND_ADD, player.PlayerCharacter.ID);
            SendTCP(pkg);
            return pkg;
        }

        public GSTCPPacketOut SendFriendRemove(int FriendID)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.FRIEND_REMOVE, FriendID);
            SendTCP(pkg);
            return pkg;
        }

        public void SendRSAKey(byte[] m, byte[] e)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut((byte)ePackageType.RSAKEY);
            pkg.Write(m);
            pkg.Write(e);
            SendTCP(pkg);
        }
    }
}
