using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bussiness;
using SqlDataProvider.Data;
using Road.Flash;
using Game.Server.Packets.Server;
using Game.Server.Worlds;
using Game.Server.GameObjects;
using Game.Base.Packets;

namespace Game.Server.Packets.Client
{
    [PacketHandler((int)ePackageType.LOGIN, "User Login handler")]
    public class UserLoginHandler : IPacketHandler
    {
        public int HandlePacket(GameClient client, GSPacketIn packet)
        {

            GSTCPPacketOut pkg = new GSTCPPacketOut(packet);
            pkg.Clear();

            if (client.Player == null && client.RsaCryptor != null)
            {
                int room = packet.ReadInt();
                string cryStr = packet.ReadString();
                //解密
                string[] temp = CryptoHelper.RsaDecrypt(client.RsaCryptor, cryStr).Split(',');
                if (temp.Length == 2)
                {
                    client.RsaCryptor = null;

                    string user = temp[0];
                    string pass = temp[1];

                    PlayerBussiness db = new PlayerBussiness();
                    PlayerInfo cha = db.LoginGame(user, pass);

                    if (cha != null)
                    {
                        GamePlayer player = WorldMgr.GetClientFromID(cha.ID);
                        if (player != null)
                        {
                            player.Quit();
                        }

                        client.Player = new GamePlayer(client, cha);
                        WorldMgr.AddClient(client.Player);
                        pkg.WriteBoolean(true);

                    }
                    else
                    {
                        pkg.WriteBoolean(false);
                    }

                    client.Out.SendTCP(pkg);
                }
                else
                {
                    //验证格式错误，端口连接
                    client.Disconnect();
                }
            }

            return 1;
        }
    }
}
