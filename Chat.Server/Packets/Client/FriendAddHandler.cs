using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bussiness;
using SqlDataProvider.Data;
using Game.Server.Packets.Server;
using Game.Server.Worlds;
using System.Xml.Linq;
using Newtonsoft.Json;
using Game.Base.Packets;

namespace Game.Server.Packets.Client
{
    [PacketHandler((byte)ePackageType.FRIEND_ADD,"添加好友")]
    public class FriendAddHandler:IPacketHandler
    {
        public int HandlePacket(GameClient client, GSPacketIn packet)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut(packet);
            pkg.Clear();

            string nickName = packet.ReadString();
            PlayerBussiness db = new PlayerBussiness();
            PlayerInfo user = db.GetUserSingleByNickName(nickName);
            if (user != null)
            {
                FriendInfo friend = new FriendInfo();
                friend.FriendID = user.ID;
                friend.IsExist = true;
                friend.Remark = "";
                friend.UserID = client.Player.PlayerCharacter.ID;

                if (db.AddFriends(friend))
                {
                    JavaScriptObject json = new JavaScriptObject();
                    json.Add("ID", user.ID);
                    json.Add("NickName", user.NickName);
                    json.Add("Style", user.Style);
                    json.Add("Sex", user.Sex);
                    json.Add("Colors", user.Colors);
                    json.Add("State", user.State);
                    pkg.WriteString(JavaScriptConvert.SerializeObject(json));

                    client.Out.SendTCP(pkg);
                }
            }

            return 0;
        }
    }
}
