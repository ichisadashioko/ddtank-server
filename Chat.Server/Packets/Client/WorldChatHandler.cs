using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Server.Packets.Server;
using Bussiness;
using SqlDataProvider.Data;
using Game.Server.GameObjects;
using Game.Base.Packets;

namespace Game.Server.Packets.Client
{
    [PacketHandler((byte)ePackageType.WORLD_CHAT,"大喇叭")]
    public class WorldChatHandler:IPacketHandler
    {
        public int HandlePacket(GameClient client, GSPacketIn packet)
        {
            int id = packet.ReadInt();
            bool result = false;

            GSTCPPacketOut pkg = new GSTCPPacketOut(packet);
            try
            {
                //TankDBDataContext db = GameServer.CreateQueryDb();

                //Sys_Users_Good goods = db.Sys_Users_Goods.SingleOrDefault(g => g.ID == id);

                PlayerBussiness db  = new PlayerBussiness();
                ItemInfo goods = db.GetUserItemSingle(id);
                if (goods != null)
                {
                    //db.Sys_Users_Goods.DeleteOnSubmit(goods);
                    //db.SubmitChanges();
                    result = true;
                    GamePlayer[] list = Worlds.WorldMgr.GetAllClients();
                    foreach (GamePlayer p in list)
                    {
                        p.Out.SendTCP(pkg);
                    }
                }
            }
            catch
            { }
            finally
            {
                if (!result)
                {
                    pkg.Clear();
                    client.Out.SendTCP(pkg);
                }
            }

            return 0;
        }
    }
}
