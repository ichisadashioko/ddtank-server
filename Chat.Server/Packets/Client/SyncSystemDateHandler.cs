using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Server.Packets.Server;
using Game.Base.Packets;

namespace Game.Server.Packets.Client
{
    [PacketHandler((int)ePackageType.SYS_DATE,"同步系统数据")]
    public class SyncSystemDateHandler:IPacketHandler
    {
        public int HandlePacket(GameClient client, GSPacketIn packet)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut(packet);
            pkg.Clear();
            pkg.WriteDateTime(DateTime.Now);
            client.Out.SendTCP(pkg);
            return 0;
        }
    }
}
