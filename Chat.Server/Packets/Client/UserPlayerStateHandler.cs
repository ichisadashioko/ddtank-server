using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Server.Packets.Server;
using Game.Base.Packets;

namespace Game.Server.Packets.Client
{
    [PacketHandler((int)ePackageType.PLAYER_STATE, "用户状态改变")]
    public class GameUserReadyHandle : IPacketHandler
    {
        public int HandlePacket(GameClient client, GSPacketIn packet)
        {
            return 0;
        }
    }
}
