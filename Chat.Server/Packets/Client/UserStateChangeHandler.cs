using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Base.Packets;

namespace Game.Server.Packets.Client
{
    [PacketHandler((byte)ePackageType.FRIEND_STATE,"改变状态")]
    class UserStateChangeHandler:IPacketHandler
    {
        public int HandlePacket(GameClient client, GSPacketIn packet)
        {
            bool state = packet.ReadBoolean();
            client.Player.Out.SendFriendState(client.Player, state);
            return 0;
        }
    }
}
