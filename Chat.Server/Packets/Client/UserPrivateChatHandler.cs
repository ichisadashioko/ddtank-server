using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Server.Packets.Server;
using Game.Server.GameObjects;
using Newtonsoft.Json;
using Game.Base.Packets;

namespace Game.Server.Packets.Client
{
    [PacketHandler((int)ePackageType.CHAT_PERSONAL,"用户与用户之间的聊天")]
    public class UserPrivateChatHandler:IPacketHandler
    {
        public int HandlePacket(GameClient client, GSPacketIn packet)
        {
            GSTCPPacketOut pkg = new GSTCPPacketOut(packet);
            pkg.SetFromClient(client.Player.PlayerCharacter.ID);
            pkg.Clear();

            string msg = packet.ReadString();
            JavaScriptObject json = JavaScriptConvert.DeserializeObject(msg) as JavaScriptObject;
            if (json.ContainsKey("fromNick"))
            {
                string nickName = (string)json["fromNick"];
                GamePlayer player = Worlds.WorldMgr.GetClientByPlayerNickName(nickName);
                if (player != null)
                {
                    pkg.WriteInt(player.PlayerCharacter.ID);
                    pkg.WriteString(msg);
                    player.Out.SendTCP(pkg);

                }
                else
                {
                    pkg.WriteInt(0);
                }
                client.Out.SendTCP(pkg);
            }

            return 1;
        }
    }
}
