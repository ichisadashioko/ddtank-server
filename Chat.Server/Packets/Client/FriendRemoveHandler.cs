using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bussiness;
using SqlDataProvider.Data;
using Game.Server.Packets.Server;
using Game.Base.Packets;

namespace Game.Server.Packets.Client
{
    [PacketHandler((byte)ePackageType.FRIEND_REMOVE,"删除好友")]
    public class FriendRemoveHandler:IPacketHandler
    {
        public int HandlePacket(GameClient client, GSPacketIn packet)
        {
            int id = packet.ReadInt();
            PlayerBussiness db = new PlayerBussiness();
            if (db.DeleteFriends(client.Player.PlayerCharacter.ID, id))
            {
                client.Out.SendFriendRemove(id);
            }

            return 0;
        }
    }
}
