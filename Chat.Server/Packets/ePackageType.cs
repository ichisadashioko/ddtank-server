namespace Game.Server.Packets
{
    public enum ePackageType
    {

        /**************************************系统*********************************************
         * */
        /// <summary>
        /// 服务器Ready
        /// </summary>
        SERVER_READY = 0x00,
        /* 
        * 登陆 
        */
        LOGIN = 0x01,

        /**
         * 用户被踢下线 
         */
        KIT_USER = 0x02,

        /**
         * 系统消息 
         */
        SYS_MESS = 0x03,

        /**
         * ping 
         */
        PING = 0x04,

        /// <summary>
        /// 同步系统时间
        /// </summary>
        SYS_DATE = 0x05,

        /// <summary>
        /// RSA密钥
        /// </summary>
        RSAKEY = 0x07,


        /*************************************场景*********************************************
         * 登陆场景 
         */
        SCENE_LOGIN = 0x10,

        /**
         * 场景准备
         */
        SCENE_READY = 0x11,

        /**
         * 场景中加入用户 
         */
        SCENE_ADD_USER = 0x12,

        /**
         * 场景聊天
         */
        SCENE_CHAT = 0x13,
        /**
         * 场景表情
         */
        SCENE_SMILE = 0x14,

        /**
         * 场景中离开用户 
         */
        SCENE_REMOVE_USER = 0x15,

        /**
         * 场景中游戏开始 
         */
        SCENE_GAME_START = 0x16,

        /**
         * 场景中游戏结束 
         */
        SCENE_GAME_STOP = 0x17,
        /**
		 * 更改场景聊天频道
		 */		
	    SCENE_CHANNEL_CHANGE = 0x18,

        /// <summary>
        /// 保存形象
        /// </summary>
        SAVE_STYLE = 0x29,

        /// <summary>
        /// 删除物品
        /// </summary>
        DELETE_ITEM = 0x2a,

        /// <summary>
        /// 添加物品
        /// </summary>
        ADD_ITEM = 0x2b,

        /// <summary>
        /// 购买物品
        /// </summary>
        BUY_ITEM = 0x2c,

        /// <summary>
        /// 更新金钱
        /// </summary>
        UPDATE_MONEY = 0x2d,
        /// <summary>
        /// 更新积分
        /// </summary>
        UPDATE_SCORE = 0x2e,
        /// <summary>
        /// 解除物品
        /// </summary>
        UNCHAIN_ITEM = 0x2f,

        /// <summary>
        /// 大喇叭，全服传音
        /// </summary>
        WORLD_CHAT = 0x31,

        /************************************游戏****************************************
         * 组聊天
         */
        CHAT_GROUP = 0x50,

        /// <summary>
        /// 进入游戏
        /// </summary>
        GAME_LOGIN = 0x51,

        /**
         * 玩家进入
         */
        GAME_PLAYER_ENTER = 0x52,

        /**
         * 玩家退出
         */
        GAME_PLAYER_EXIT = 0x53,

        GAME_VISITOR_ENTER = 0x54,

        GAME_VISITOR_EXIT = 0x55,

        GAME_START = 0x56,

   ///     GAME_READY = 0x57,
        PLAYER_STATE = 0x57,

        GAME_BEGIN_COUNT = 0x58,

        GAME_STOP_COUNT = 0x59,

        GAME_OVER = 0x5a,

        GAME_CMD = 0x5b,

        /// <summary>
        /// 游戏协议
        /// </summary>
        GAME_SYNC = 0x5c,

        GAME_VISITOR_DATA = 0x5d,

        GAME_PLAYER_CREATE = 0x5e,

        GAME_ROOM_CREATE = 0x5f,

        GAME_ROOM_CLEAR = 0x60,

        GAME_ROOM_HOST = 0x61,

        GAME_ROOM_KICK = 0x62,

        GAME_ROOM_CLOSE = 0x63,

        GAME_ROOM_OPEN = 0x64,

        GAME_ROOM_INFO = 0x65,

        GAME_TEAM = 0x66,

        GAME_LOAD = 0x67,

        GAME_CHANGE_MAP = 0x68,

        GAME_TEAM_TYPE = 0x69,

        /**************************************人物********************************************
         * 人物改变状态 
         */
        CHANGE_STATE = 0x20,

        /**
         * 更改昵称
         */
        CHANGE_NICKNAME = 0x21,

        /**
         * 人物动作 
         */
        AC_ACTION = 0x23,

        /**
         * 同步动作 
         */
        SYNCH_ACTION = 0x24,

        /**
         * 个人聊天
         */
        CHAT_PERSONAL = 0x25,

        /// <summary>
        ///  保存用户资料
        /// </summary>
        SAVE_INFO = 0x2f,
        /// <summary>
        /// 保存用户的密码
        /// </summary>
        SAVE_PASSWORD = 0x90,

        /// <summary>
        /// 补充资料
        /// </summary>
        RENEW_INFO = 0x91,

        /**************************************好友********************************************/
        /// <summary>
        /// 添加好友
        /// </summary>
        FRIEND_ADD = 0xa0,
        /// <summary>
        /// 删除好友
        /// </summary>
        FRIEND_REMOVE = 0xa1,
        /// <summary>
        /// 更新好友备注
        /// </summary>
        FRIEND_UPDATE = 0xa2,
        /// <summary>
        /// 好友登陆
        /// </summary>
        FRIEND_LOGIN = 0xa3,
        /// <summary>
        /// 好友离开
        /// </summary>
        FRIEND_LOGOUT = 0xa4,
        /// <summary>
        /// 好友状态
        /// </summary>
        FRIEND_STATE = 0xa5,

    }
}
