using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Base;
using Game.Base.Config;
using System.Net;
using System.Reflection;
using System.IO;

namespace Game.Server
{
    public class GameServerConfiguration:BaseServerConfiguration
    {
        #region Server

        /// <summary>
        /// holds the server root directory
        /// </summary>
        protected string _rootDirectory;

        /// <summary>
        /// Holds the log configuration file path
        /// </summary>
        protected string _logConfigFile;

        /// <summary>
        /// Name of the scripts compilation target
        /// </summary>
        protected string _scriptCompilationTarget;

        /// <summary>
        /// The assemblies to include when compiling the scripts
        /// </summary>
        protected string _scriptAssemblies;

        /// <summary>
        /// True if the server shall automatically create accounts
        /// </summary>
        protected bool _autoAccountCreation;

        /// <summary>
        /// The game server name
        /// </summary>
        protected string _serverName;

        /// <summary>
        /// The short server name, shown in /loc command
        /// </summary>
        protected string _serverNameShort;

        /// <summary>
        /// The count of server cpu
        /// </summary>
        protected int _cpuCount;

        /// <summary>
        /// The max client count.
        /// </summary>
        protected int _maxClientCount;

        /// <summary>
        /// The endpoint to send UDP packets from.
        /// </summary>
        protected IPEndPoint m_udpOutEndpoint;

        #endregion
        #region Logging
        /// <summary>
        /// The logger name where to log the gm+ commandos
        /// </summary>
        protected string _gmActionsLoggerName;

        /// <summary>
        /// The logger name where to log cheat attempts
        /// </summary>
        protected string _cheatLoggerName;

        /// <summary>
        /// The file name of the invalid names file
        /// </summary>
        protected string _invalidNamesFile = "";

        #endregion
        #region Database

        /// <summary>
        /// The path to the XML database folder
        /// </summary>
        protected string _dbConnectionString;

        /// <summary>
        /// True if the server shall autosave the db
        /// </summary>
        protected bool _autoSave;

        /// <summary>
        /// The auto save interval in minutes
        /// </summary>
        protected int _saveInterval;

        #endregion
        #region Load/Save

        /// <summary>
        /// Loads the config values from a specific config element
        /// </summary>
        /// <param name="root">the root config element</param>
        protected override void LoadFromConfig(ConfigElement root)
        {
            base.LoadFromConfig(root);


            _logConfigFile = root["Server"]["LogConfigFile"].GetString(_logConfigFile);

            _scriptCompilationTarget = root["Server"]["ScriptCompilationTarget"].GetString(_scriptCompilationTarget);
            _scriptAssemblies = root["Server"]["ScriptAssemblies"].GetString(_scriptAssemblies);
            _autoAccountCreation = root["Server"]["AutoAccountCreation"].GetBoolean(_autoAccountCreation);

            _serverName = root["Server"]["ServerName"].GetString(_serverName);
            _serverNameShort = root["Server"]["ServerNameShort"].GetString(_serverNameShort);

            _cheatLoggerName = root["Server"]["CheatLoggerName"].GetString(_cheatLoggerName);
            _gmActionsLoggerName = root["Server"]["GMActionLoggerName"].GetString(_gmActionsLoggerName);
            _invalidNamesFile = root["Server"]["InvalidNamesFile"].GetString(_invalidNamesFile);

            _dbConnectionString = root["Server"]["DBConnectionString"].GetString(_dbConnectionString);
            _autoSave = root["Server"]["DBAutosave"].GetBoolean(_autoSave);
            _saveInterval = root["Server"]["DBAutosaveInterval"].GetInt(_saveInterval);
            _maxClientCount = root["Server"]["MaxClientCount"].GetInt(_maxClientCount);
            _cpuCount = root["Server"]["CpuCount"].GetInt(_cpuCount);
            if (_cpuCount < 1)
                _cpuCount = 1;

            IPAddress address = null;
            int port = -1;
            string addressStr = root["Server"]["UDPOutIP"].GetString(string.Empty);
            string portStr = root["Server"]["UDPOutPort"].GetString(string.Empty);
            if (IPAddress.TryParse(addressStr, out address)
                && int.TryParse(portStr, out port)
                && IPEndPoint.MaxPort >= port
                && IPEndPoint.MinPort <= port)
            {
                m_udpOutEndpoint = new IPEndPoint(address, port);
            }
            else
            {
                m_udpOutEndpoint = new IPEndPoint(IPAddress.Any, 9001);
            }

        }

        /// <summary>
        /// Saves the values into a specific config element
        /// </summary>
        /// <param name="root">the root config element</param>
        protected override void SaveToConfig(ConfigElement root)
        {
            base.SaveToConfig(root);
            root["Server"]["ServerName"].Set(_serverName);
            root["Server"]["ServerNameShort"].Set(_serverNameShort);
            // Removed to not confuse users
            //			root["Server"]["RootDirectory"].Set(_rootDirectory);
            root["Server"]["LogConfigFile"].Set(_logConfigFile);

            root["Server"]["ScriptCompilationTarget"].Set(_scriptCompilationTarget);
            root["Server"]["ScriptAssemblies"].Set(_scriptAssemblies);
            root["Server"]["AutoAccountCreation"].Set(_autoAccountCreation);

            root["Server"]["CheatLoggerName"].Set(_cheatLoggerName);
            root["Server"]["GMActionLoggerName"].Set(_gmActionsLoggerName);
            root["Server"]["InvalidNamesFile"].Set(_invalidNamesFile);
            root["Server"]["DBConnectionString"].Set(_dbConnectionString);
            root["Server"]["DBAutosave"].Set(_autoSave);
            root["Server"]["DBAutosaveInterval"].Set(_saveInterval);

            // Store UDP out endpoint
            if (m_udpOutEndpoint != null)
            {
                root["Server"]["UDPOutIP"].Set(m_udpOutEndpoint.Address.ToString());
                root["Server"]["UDPOutPort"].Set(m_udpOutEndpoint.Port.ToString());
            }
        }
        #endregion
        #region Constructors
        /// <summary>
        /// Constructs a server configuration with default values
        /// </summary>
        public GameServerConfiguration()
            : base()
        {
            _serverName = "7Road Server";
            _serverNameShort = "7Road";
            if (Assembly.GetEntryAssembly() != null)
                _rootDirectory = new FileInfo(Assembly.GetEntryAssembly().Location).DirectoryName;
            else
                _rootDirectory = new FileInfo(Assembly.GetAssembly(typeof(GameServer)).Location).DirectoryName;

            _logConfigFile = "." + Path.DirectorySeparatorChar + "config" + Path.DirectorySeparatorChar + "logconfig.xml";

            _scriptCompilationTarget = "." + Path.DirectorySeparatorChar + "lib" + Path.DirectorySeparatorChar + "GameServerScripts.dll";
            _scriptAssemblies = "Game.Base.dll,Road.Database.dll,Game.Server.dll";
            _autoAccountCreation = true;

            _cheatLoggerName = "cheats";
            _gmActionsLoggerName = "gmactions";
            _invalidNamesFile = "." + Path.DirectorySeparatorChar + "config" + Path.DirectorySeparatorChar + "invalidnames.txt";
            _dbConnectionString = "Data Source=192.168.0.4;Initial Catalog=Db_Tank;Persist Security Info=True;User ID=sa;Password=123456;Asynchronous Processing=true;";
            _autoSave = true;
            _saveInterval = 10;
            _maxClientCount = 3000;

            // Get count of CPUs
            _cpuCount = Environment.ProcessorCount;
            if (_cpuCount < 1)
            {
                try
                {
                    _cpuCount = int.Parse(Environment.GetEnvironmentVariable("NUMBER_OF_PROCESSORS"));
                }
                catch { _cpuCount = -1; }
            }
            if (_cpuCount < 1)
                _cpuCount = 1;
        }

        #endregion

        /// <summary>
        /// Gets or sets the root directory of the server
        /// </summary>
        public string RootDirectory
        {
            get { return _rootDirectory; }
            set { _rootDirectory = value; }
        }

        /// <summary>
        /// Gets or sets the log configuration file of this server
        /// </summary>
        public string LogConfigFile
        {
            get
            {
                if (Path.IsPathRooted(_logConfigFile))
                    return _logConfigFile;
                else
                    return Path.Combine(_rootDirectory, _logConfigFile);
            }
            set { _logConfigFile = value; }
        }

        /// <summary>
        /// Gets or sets the script compilation target
        /// </summary>
        public string ScriptCompilationTarget
        {
            get { return _scriptCompilationTarget; }
            set { _scriptCompilationTarget = value; }
        }

        /// <summary>
        /// Gets or sets the script assemblies to be included in the script compilation
        /// </summary>
        public string ScriptAssemblies
        {
            get { return _scriptAssemblies; }
            set { _scriptAssemblies = value; }
        }

        /// <summary>
        /// Gets or sets the auto account creation flag
        /// </summary>
        public bool AutoAccountCreation
        {
            get { return _autoAccountCreation; }
            set { _autoAccountCreation = value; }
        }

        /// <summary>
        /// Gets or sets the server name
        /// </summary>
        public string ServerName
        {
            get { return _serverName; }
            set { _serverName = value; }
        }

        /// <summary>
        /// Gets or sets the short server name
        /// </summary>
        public string ServerNameShort
        {
            get { return _serverNameShort; }
            set { _serverNameShort = value; }
        }

        /// <summary>
        /// Gets or sets the GM action logger name
        /// </summary>
        public string GMActionsLoggerName
        {
            get { return _gmActionsLoggerName; }
            set { _gmActionsLoggerName = value; }
        }

        /// <summary>
        /// Gets or sets the cheat logger name
        /// </summary>
        public string CheatLoggerName
        {
            get { return _cheatLoggerName; }
            set { _cheatLoggerName = value; }
        }

        /// <summary>
        /// Gets or sets the invalid name filename
        /// </summary>
        public string InvalidNamesFile
        {
            get
            {
                if (Path.IsPathRooted(_invalidNamesFile))
                    return _invalidNamesFile;
                else
                    return Path.Combine(_rootDirectory, _invalidNamesFile);
            }
            set { _invalidNamesFile = value; }
        }

        /// <summary>
        /// Gets or sets the xml database path
        /// </summary>
        public string DBConnectionString
        {
            get { return _dbConnectionString; }
            set { _dbConnectionString = value; }
        }

        /// <summary>
        /// Gets or sets the autosave flag
        /// </summary>
        public bool AutoSave
        {
            get { return _autoSave; }
            set { _autoSave = value; }
        }

        /// <summary>
        /// Gets or sets the autosave interval
        /// </summary>
        public int SaveInterval
        {
            get { return _saveInterval; }
            set { _saveInterval = value; }
        }

        /// <summary>
        /// Gets or sets the server cpu count
        /// </summary>
        public int CpuCount
        {
            get { return _cpuCount; }
            set { _cpuCount = value; }
        }

        /// <summary>
        /// Gets or sets the max cout of clients allowed
        /// </summary>
        public int MaxClientCount
        {
            get { return _maxClientCount; }
            set { _maxClientCount = value; }
        }

        /// <summary>
        /// Gets or sets UDP address and port to send UDP packets from.
        /// If <code>null</code> then <see cref="Socket"/> decides where to bind.
        /// </summary>
        public IPEndPoint UDPOutEndpoint
        {
            get { return m_udpOutEndpoint; }
            set { m_udpOutEndpoint = value; }
        }

    }
}
