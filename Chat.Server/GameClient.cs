using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Base;
using System.Net;
using Game.Server.GameObjects;
using log4net;
using Game.Base.Packets;
using Game.Server.Managers;
using System.Reflection;
using System.Threading;
using SqlDataProvider.Data;
using System.Security.Cryptography;
using Game.Server.Packets.Server;

namespace Game.Server
{
    public class GameClient : BaseClient
    {

        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly byte[] POLICY = Encoding.UTF8.GetBytes("<?xml version=\"1.0\"?><!DOCTYPE cross-domain-policy SYSTEM \"http://www.adobe.com/xml/dtds/cross-domain-policy.dtd\"><cross-domain-policy><allow-access-from domain=\"*\" to-ports=\"*\" /></cross-domain-policy>\0");

        /// <summary>
        /// 用户实例
        /// </summary>
        protected GamePlayer m_player;

        public GamePlayer Player
        {
            get { return m_player; }
            set
            {
                GamePlayer oldPlayer = Interlocked.Exchange(ref m_player, value);
                if (oldPlayer != null)
                {
                    oldPlayer.Quit();
                }
            }
        }

        /// <summary>
        /// 客户端最后发包时间
        /// </summary>
        protected long m_pingTime = DateTime.Now.Ticks;

        public long PingTime
        {
            get { return m_pingTime; }
        }

        /// <summary>
        /// 发送接口
        /// </summary>
        protected IPacketLib m_packetLib;

        public IPacketLib Out
        {
            get { return m_packetLib; }
            set { m_packetLib = value; }
        }

        /// <summary>
        /// TCP包处理器
        /// </summary>
        protected PacketProcessor m_packetProcessor;

        public PacketProcessor PacketProcessor
        {
            get { return m_packetProcessor; }
        }


        protected GameServer _srvr;

        public GameServer Server
        {
            get { return _srvr; }
        }

        /// <summary>
        /// RSA加密器
        /// </summary>
        private RSACryptoServiceProvider _rsa;

        public RSACryptoServiceProvider RsaCryptor
        {
            get
            {
                return _rsa;
            }
            set
            {
                _rsa = null;
            }
        }

        /// <summary>
        /// 收到的错误包数量
        /// </summary>
        private int _error = 0;

        /// <summary>
        /// 接收到客户端数据
        /// </summary>
        /// <param name="num_bytes"></param>
        public override void OnRecv(int num_bytes)
        {
            if (m_player != null || _rsa != null)
            {
                base.OnRecv(num_bytes);

                m_pingTime = DateTime.Now.Ticks;

            }
            else if (_rsa == null)
            {
                if (m_readBuffer[0] == '<')
                {
                    _sock.Send(POLICY);
                }
                else if (m_readBuffer[0] == '7')
                {
                    m_packetLib = AbstractPacketLib.CreatePacketLibForVersion(1, this);
                    m_packetProcessor = new PacketProcessor(this);
                    _rsa = new RSACryptoServiceProvider();
                    RSAParameters para = _rsa.ExportParameters(false);
                    m_packetLib.SendRSAKey(para.Modulus, para.Exponent);
                }
                else
                {
                    _error++;
                }
                if (_error >= 3)
                {
                    //如果错误包数量大于5,则断开连接
                    Disconnect();
                }
            }
        }

        /// <summary>
        /// 收到协议包
        /// </summary>
        /// <param name="pkg"></param>
        public override void OnRecvPacket(GSPacketIn pkg)
        {
            m_packetProcessor.HandlePacket(pkg);
        }

        /// <summary>
        /// 客户端连接上
        /// </summary>
        public override void OnConnect()
        {
            _error = 0;
            base.OnConnect();
        }

        /// <summary>
        /// 客户端断开连接
        /// </summary>
        public override void OnDisconnect()
        {
            try
            {
                if (m_player != null)
                {
                    m_player.Quit();
                    m_player = null;
                }
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("OnDisconnect", e);
            }
        }

        /// <summary>
        /// 保存用户数据到数据库
        /// </summary>
        public void SavePlayer()
        {
            try
            {
                if (m_player != null)
                {
                    m_player.SaveIntoDatabase();
                }
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("SavePlayer", e);
            }
        }

        /// <summary>
        /// Constructor for a game client
        /// </summary>
        /// <param name="srvr">The server that's communicating with this client</param>
        public GameClient(GameServer svr)
            : base(svr.AcquirePacketBuffer(), svr.AcquirePacketBuffer())
        {
            _srvr = svr;
            m_player = null;
        }

        /// <summary>
        /// Returns short informations about the client
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return new StringBuilder(128)
                .Append(" pakLib:").Append(Out == null ? "(null)" : Out.GetType().FullName)
                .Append(" IP:").Append(TcpEndpoint)
                .Append(" char:").Append(Player == null ? "null" : Player.PlayerCharacter.NickName)
                .ToString();
        }
    }
}
