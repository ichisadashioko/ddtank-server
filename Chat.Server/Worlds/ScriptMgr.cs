using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Reflection;
using System.Collections;
using System.IO;
using Game.Server.GameObjects;
using Game.Base;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using Microsoft.VisualBasic;
using Game.Base.Config;

namespace Game.Server.Managers
{
    public class ScriptMgr
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly ArrayList _scripts = new ArrayList(4);

        /// <summary>
        /// Gets the scripts assemblies
        /// </summary>
        public static Assembly[] Scripts
        {
            get { return (Assembly[])_scripts.ToArray(typeof(Assembly)); }
        }

       
        #region Compile Scripts

        public static void InsertAssembly(Assembly ass)
        {
            _scripts.Add(ass);
        }

        /// <summary>
        /// Compiles the scripts into an assembly
        /// </summary>
        /// <param name="compileVB">True if the source files will be in VB.NET</param>
        /// <param name="path">Path to the source files</param>
        /// <param name="dllName">Name of the assembly to be generated</param>
        /// <param name="asm_names">References to other assemblies</param>
        /// <returns>True if succeeded</returns>
        public static bool CompileScripts(bool compileVB, string path, string dllName, string[] asm_names)
        {
            if (!path.EndsWith(@"\") && !path.EndsWith(@"/"))
                path = path + "/";

            //Reset the assemblies
            _scripts.Clear();


            //Check if there are any scripts, if no scripts exist, that is fine as well
            ArrayList files = ParseDirectory(new DirectoryInfo(path), compileVB ? "*.vb" : "*.cs", true);
            if (files.Count == 0)
            {
                return true;
            }

            //Recompile is required as standard
            bool recompileRequired = true;

            //This file should hold the script infos
            FileInfo configFile = new FileInfo(dllName + ".xml");

            //If the script assembly is missing, recompile is required
            if (!File.Exists(dllName))
            {
                if (log.IsDebugEnabled)
                    log.Debug("Script assembly missing, recompile required!");
            }
            else
            {
                //Script assembly found, check if we have a file modify info
                if (configFile.Exists)
                {
                    //Ok, we have a config file containing the script file sizes and dates
                    //let's check if any script was modified since last compiling them
                    if (log.IsDebugEnabled)
                        log.Debug("Found script info file");

                    try
                    {
                        XMLConfigFile config = XMLConfigFile.ParseXMLFile(configFile);

                        //Assume no scripts changed
                        recompileRequired = false;

                        ArrayList precompiledScripts = new ArrayList(config.Children.Keys);

                        //Now test the files
                        foreach (FileInfo finfo in files)
                        {
                            if (config[finfo.FullName]["size"].GetInt(0) != finfo.Length
                                || config[finfo.FullName]["lastmodified"].GetLong(0) != finfo.LastWriteTime.ToFileTime())
                            {
                                //Recompile required
                                recompileRequired = true;
                                break;
                            }
                            precompiledScripts.Remove(finfo.FullName);
                        }

                        recompileRequired |= precompiledScripts.Count > 0; // some compiled script was removed

                        if (recompileRequired && log.IsDebugEnabled)
                        {
                            log.Debug("At least one file was modified, recompile required!");
                        }
                    }
                    catch (Exception e)
                    {
                        if (log.IsErrorEnabled)
                            log.Error("Error during script info file to scripts compare", e);
                    }
                }
                else
                {
                    if (log.IsDebugEnabled)
                        log.Debug("Script info file missing, recompile required!");
                }
            }

            //If we need no compiling, we load the existing assembly!
            if (!recompileRequired)
            {
                try
                {
                    Assembly asm = Assembly.LoadFrom(dllName);
                    if (!_scripts.Contains(asm))
                        _scripts.Add(asm);

                    if (log.IsDebugEnabled)
                        log.Debug("Precompiled script assembly loaded");
                }
                catch (Exception e)
                {
                    if (log.IsErrorEnabled)
                        log.Error("Error loading precompiled script assembly, recompile required!", e);
                }

                //Return success!
                return true;
            }

            //We need a recompile, if the dll exists, delete it firsthand
            if (File.Exists(dllName))
                File.Delete(dllName);

            CompilerResults res = null;
            try
            {
                CodeDomProvider compiler;

                if (compileVB)
                {
                    compiler = new VBCodeProvider();
                }
                else
                {
                    compiler = new CSharpCodeProvider();
                }
                CompilerParameters param = new CompilerParameters(asm_names, dllName, true);
                param.GenerateExecutable = false;
                param.GenerateInMemory = false;
                param.WarningLevel = 2;
                param.CompilerOptions = @"/lib:." + Path.DirectorySeparatorChar + "lib";

                string[] filepaths = new string[files.Count];
                for (int i = 0; i < files.Count; i++)
                    filepaths[i] = ((FileInfo)files[i]).FullName;

                res = compiler.CompileAssemblyFromFile(param, filepaths);

                //After compiling, collect
                GC.Collect();

                if (res.Errors.HasErrors)
                {
                    foreach (CompilerError err in res.Errors)
                    {
                        if (err.IsWarning) continue;

                        StringBuilder builder = new StringBuilder();
                        builder.Append("   ");
                        builder.Append(err.FileName);
                        builder.Append(" Line:");
                        builder.Append(err.Line);
                        builder.Append(" Col:");
                        builder.Append(err.Column);
                        if (log.IsErrorEnabled)
                        {
                            log.Error("Script compilation failed because: ");
                            log.Error(err.ErrorText);
                            log.Error(builder.ToString());
                        }
                    }

                    return false;
                }

                if (!_scripts.Contains(res.CompiledAssembly))
                    _scripts.Add(res.CompiledAssembly);

            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("CompileScripts", e);
                _scripts.Clear();
            }
            //now notify our callbacks
            bool ret = false;
            if (res != null)
            {
                ret = !res.Errors.HasErrors;
            }
            if (ret == false)
                return ret;

            XMLConfigFile newconfig = new XMLConfigFile();
            foreach (FileInfo finfo in files)
            {
                newconfig[finfo.FullName]["size"].Set(finfo.Length);
                newconfig[finfo.FullName]["lastmodified"].Set(finfo.LastWriteTime.ToFileTime());
            }
            if (log.IsDebugEnabled)
                log.Debug("Writing script info file");

            newconfig.Save(configFile);

            return true;
        }

        /// <summary>
        /// Parses a directory for all source files
        /// </summary>
        /// <param name="path">The root directory to start the search in</param>
        /// <param name="filter">A filter representing the types of files to search for</param>
        /// <param name="deep">True if subdirectories should be included</param>
        /// <returns>An ArrayList containing FileInfo's for all files in the path</returns>
        private static ArrayList ParseDirectory(DirectoryInfo path, string filter, bool deep)
        {
            ArrayList files = new ArrayList();

            if (!path.Exists)
                return files;

            files.AddRange(path.GetFiles(filter));

            if (deep)
            {
                foreach (DirectoryInfo subdir in path.GetDirectories())
                    files.AddRange(ParseDirectory(subdir, filter, deep));
            }

            return files;
        }

        #endregion

        #region Type Utils
        /// <summary>
        /// Search for a type by name; first in GameServer assembly then in scripts assemblies
        /// </summary>
        /// <param name="name">The type name</param>
        /// <returns>Found type or null</returns>
        public static Type GetType(string name)
        {
            Type t = typeof(GameServer).Assembly.GetType(name);
            if (t == null)
            {
                foreach (Assembly asm in Scripts)
                {
                    t = asm.GetType(name);
                    if (t == null) continue;
                    return t;
                }
            }
            else
            {
                return t;
            }
            return null;
        }

        /// <summary>
        /// Finds all classes that derive from given type.
        /// First check scripts then GameServer assembly.
        /// </summary>
        /// <param name="baseType">The base class type.</param>
        /// <returns>Array of types or empty array</returns>
        public static Type[] GetDerivedClasses(Type baseType)
        {
            if (baseType == null)
                return new Type[0];

            ArrayList types = new ArrayList();
            ArrayList asms = new ArrayList(Scripts);
            asms.Add(typeof(GameServer).Assembly);

            foreach (Assembly asm in asms)
            foreach (Type t in asm.GetTypes())
            {
                if (t.IsClass && baseType.IsAssignableFrom(t))
                    types.Add(t);
            }

            return (Type[])types.ToArray(typeof(Type));
        }

        /// <summary>
        /// Finds all classes the implements the given interface.
        /// </summary>
        /// <param name="baseInterface"></param>
        /// <returns></returns>
        public static Type[] GetImplementedClasses(string baseInterface)
        {
            ArrayList types = new ArrayList();
            ArrayList asms = new ArrayList(Scripts);
            asms.Add(typeof(GameServer).Assembly);
            foreach (Assembly asm in asms)
            {
                foreach (Type t in asm.GetTypes())
                {
                    if (t.IsClass && t.GetInterface(baseInterface) != null)
                    {
                        types.Add(t);
                    }
                }
            }
            return (Type[])types.ToArray(typeof(Type));
        }
        #endregion
    }
}
