using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Collections;
using System.Timers;
using System.Threading;
using Game.Server.GameObjects;
using System.Reflection;
using System.Collections.Specialized;

namespace Game.Server.Worlds
{
    /// <summary>
    /// The WorldMgr is used to retrieve information and objects from
    /// the world. It contains lots of functions that can be used. It
    /// is a static class.
    /// </summary>
    public sealed class WorldMgr
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Sync the clients collection.
        /// </summary>
        private static ReaderWriterLock m_clientLocker;
        /// <summary>
        /// This array holds all gameclients connected to the game
        /// </summary>
        private static Dictionary<GamePlayer, GamePlayer> m_clients;

        private static Dictionary<string, GamePlayer> m_clientsNick;

        /// <summary>
        /// Initializes the WorldMgr. This function must be called
        /// before the WorldMgr can be used!
        /// </summary>
        public static bool Init()
        {
            try
            {
                m_clientLocker = new ReaderWriterLock();
                m_clients = new Dictionary<GamePlayer, GamePlayer>();
                m_clientsNick = new Dictionary<string, GamePlayer>();
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("Init", e);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Adds a GamePlayer 
        /// </summary>
        /// <param name="player"></param>
        public static void AddClient(GamePlayer player)
        {
            m_clientLocker.AcquireWriterLock(Timeout.Infinite);
            try
            {
                m_clients.Add(player, player);
                m_clientsNick.Add(player.PlayerCharacter.NickName, player);
                player.Out.SendFriendState(player, true);
            }
            finally
            {
                m_clientLocker.ReleaseWriterLock();
            }
        }

        /// <summary>
        /// Removes a GamePlayer!
        /// </summary>
        /// <param name="entry">The GameClient to be removed</param>
        public static void RemoveClient(GamePlayer player)
        {
            m_clientLocker.AcquireWriterLock(Timeout.Infinite);
            try
            {
                m_clients.Remove(player);
                m_clientsNick.Remove(player.PlayerCharacter.NickName);
                player.Out.SendFriendState(player, false);
            }
            finally
            {
                m_clientLocker.ReleaseWriterLock();
            }
        }

        /// <summary>
        /// Cleans up and stops all the RegionMgr tasks inside
        /// the regions.
        /// </summary>
        public static void Exit()
        {
          
        }

        /// <summary>
        /// Fetch a GameClient based on it's ID
        /// </summary>
        /// <param name="id">ID to search</param>
        /// <returns>The found GameClient or null if not found</returns>
        public static GamePlayer GetClientFromID(int id)
        {
            GamePlayer[] list = GetAllClients();
            foreach (GamePlayer client in list)
            {
                if (client.PlayerCharacter.ID == id)
                    return client;
            }
            return null;
        }


        /// <summary>
        /// Find a GameClient by the Player's Guid id
        /// </summary>
        /// <param name="playerID">ID to search</param>
        /// <returns>The found GameClient or null</returns>
        public static GamePlayer GetClientByPlayerID(int playerID)
        {
            GamePlayer[] list = GetAllClients();
            //foreach (GamePlayer client in list)
            //{
            //    if (client.PlayerCharacter.ID == playerID)
            //        return client;
            //}
            return null;
        }

        public static GamePlayer GetClientByPlayerNickName(string nickName)
        {
            GamePlayer player = null;
            m_clientLocker.AcquireReaderLock(Timeout.Infinite);
            try
            {
                if (m_clientsNick.Keys.Contains(nickName))
                {
                    player = m_clientsNick[nickName];
                }
            }
            finally
            {
                m_clientLocker.ReleaseReaderLock();
            }
            return player;
        }

        /// <summary>
        /// Gets a copy of ALL clients no matter at what state they are
        /// </summary>
        /// <returns>ArrayList of GameClients</returns>
        public static GamePlayer[] GetAllClients()
        {
            GamePlayer[] list = null;

            m_clientLocker.AcquireReaderLock(Timeout.Infinite);
            try
            {
                list = m_clients.Values.ToArray<GamePlayer>();
            }
            finally
            {
                m_clientLocker.ReleaseReaderLock();
            }

            return list == null ? new GamePlayer[0] : list;
        }
    }
}
