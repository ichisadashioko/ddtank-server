using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Reflection;
using Bussiness;
using SqlDataProvider.Data;
using System.Collections;
using Game.Server.Packets.Server;
using Game.Server.Worlds;
using System.Threading;

namespace Game.Server.GameObjects
{
    /// <summary>
    /// This class represents a player in the game
    /// </summary>
    public class GamePlayer
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private GameClient m_client;

        public GameClient Client
        {
            get
            {
                return m_client;
            }
            set
            {
                m_client = value;
            }
        }

        private PlayerInfo my_character;

        public PlayerInfo PlayerCharacter
        {
            get
            {
                return my_character;
            }
            set
            {
                my_character = value;
            }
        }

        public IPacketLib Out
        {
            get
            {
                return m_client.Out;
            }
        }


        public GamePlayer(GameClient client, PlayerInfo theChar)
            : base()
        {
            m_client = client;
            my_character = theChar;
        }

        public void SaveIntoDatabase()
        {
           
        }

        public virtual bool Quit()
        {
            WorldMgr.RemoveClient(this);
            return true;
        }

    }
}
