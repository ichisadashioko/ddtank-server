using System;
using Game.Base;
using System.IO;
using log4net.Config;
using log4net;
using System.Reflection;
using System.Collections;
using System.Threading;
using Game.Server.Packets.Server;
using log4net.Core;
using Game.Base.Config;
using Game.Server.Worlds;
using System.Net.Sockets;
using System.Net;
using Game.Server.Packets.Client;
using Game.Server.Managers;
using Game.Base.Events;

namespace Game.Server
{
    /// <summary>
    /// Class incapsulates all game server functions
    /// </summary>
    public class GameServer : BaseServer
    {
        /// <summary>
        /// Defines a logger for this class
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Check ping timer's inteval time in seconds.
        /// </summary>
        private static readonly int PING_TIMEOUT = 3 * 60;


        /// <summary>
        /// Creates the gameserver instance
        /// </summary>
        /// <param name="config"></param>
        public static void CreateInstance(GameServerConfiguration config)
        {
            //Only one intance
            if (Instance != null)
                return;

            //Try to find the log.config file, if it doesn't exist
            //we create it
            FileInfo logConfig = new FileInfo(config.LogConfigFile);
            if (!logConfig.Exists)
            {
                ResourceUtil.ExtractResource(logConfig.Name, logConfig.FullName,Assembly.GetAssembly(typeof(GameServer)));
            }
            //Configure and watch the config file
            XmlConfigurator.ConfigureAndWatch(logConfig);
            //Create the instance
            _instance = new GameServer(config);
        }

        #region Constructors

        /// <summary>
        /// Constructor with a given configuration
        /// </summary>
        /// <param name="config">A valid game server configuration</param>
        protected GameServer(GameServerConfiguration config)
        {
            _config = config;
            if (log.IsDebugEnabled)
            {
                log.Debug("Current directory is: " + Directory.GetCurrentDirectory());
                log.Debug("Gameserver root directory is: " + Configuration.RootDirectory);
                log.Debug("Changing directory to root directory");
            }

            Directory.SetCurrentDirectory(Configuration.RootDirectory);

            try
            {
                if (log.IsInfoEnabled)
                    log.Info("Game Server Initialization finished!");
            }
            catch (Exception e)
            {
                if (log.IsFatalEnabled)
                    log.Fatal("GameServer initialization failed!", e);
                throw new ApplicationException("Fatal Error: Could not initialize Game Server", e);
            }
        }
        #endregion

        #region Packet buffer pool

        /// <summary>
        /// The size of all packet buffers.
        /// </summary>
        private const int BUF_SIZE = 2048;

        /// <summary>
        /// Holds all packet buffers.
        /// </summary>
        private Queue m_packetBufPool;

        /// <summary>
        ///  all packet buffers.
        /// </summary>
        /// <returns>success</returns>
        private bool AllocatePacketBuffers()
        {
            int count = Configuration.MaxClientCount * 3;
            count += Math.Max(10 * 3, count * 3 / 8);
            m_packetBufPool = new Queue(count);
            for (int i = 0; i < count; i++)
            {
                m_packetBufPool.Enqueue(new byte[BUF_SIZE]);
            }
            if (log.IsDebugEnabled)
                log.DebugFormat("allocated packet buffers: {0}", count.ToString());
            return true;
        }

        /// <summary>
        /// Gets the count of packet buffers in the pool.
        /// </summary>
        public int PacketPoolSize
        {
            get { return m_packetBufPool.Count; }
        }

        /// <summary>
        /// Gets packet buffer from the pool.
        /// </summary>
        /// <returns>byte array that will be used as packet buffer.</returns>
        public byte[] AcquirePacketBuffer()
        {
            lock (m_packetBufPool.SyncRoot)
            {
                if (m_packetBufPool.Count > 0)
                    return (byte[])m_packetBufPool.Dequeue();
            }
            log.Warn("packet buffer pool is empty!");
            return new byte[BUF_SIZE];
        }

        /// <summary>
        /// Releases previously acquired packet buffer.
        /// </summary>
        /// <param name="buf">The released buf</param>
        public void ReleasePacketBuffer(byte[] buf)
        {
            if (buf == null || GC.GetGeneration(buf) < GC.MaxGeneration)
                return;
            lock (m_packetBufPool.SyncRoot)
            {
                m_packetBufPool.Enqueue(buf);
            }
        }

        #endregion

        #region Client

        /// <summary>
        /// Creates a new client
        /// </summary>
        /// <returns>An instance of a new client</returns>
        protected override BaseClient GetNewClient()
        {
            GameClient client = new GameClient(this);
            return client;
        }

        #endregion


        #region Fileds/Properties

        private GameServerConfiguration _config;
        /// <summary>
        /// Retrieves the server configuration
        /// </summary>
        public GameServerConfiguration Configuration
        {
            get { return (GameServerConfiguration)_config; }
        }

        #endregion

        #region Start

        private bool _debugMemory = false;

        /// <summary>
        /// Starts the server
        /// </summary>
        /// <returns>True if the server was successfully started</returns>
        public override bool Start()
        {
            try
            {
                if (_debugMemory)
                    log.Debug("Starting Server, Memory is " + GC.GetTotalMemory(false) / 1024 / 1024);

                Thread.CurrentThread.Priority = ThreadPriority.Normal;

                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                //---------------------------------------------------------------
                //Try to init the server port
                if (!InitComponent(InitSocket(_config.Ip,_config.Port), "InitSocket Port:" + Configuration.Port))
                    return false;

                //---------------------------------------------------------------
                //Packet buffers
                if (!InitComponent(AllocatePacketBuffers(), "AllocatePacketBuffers()"))
                    return false;

                //---------------------------------------------------------------
                //Try to initialize the script components
                if (!InitComponent(StartScriptComponents(), "Script components"))
                    return false;

                //---------------------------------------------------------------
                if (!InitComponent(WorldMgr.Init(), "WorldMgr Init"))
                    return false;

                //---------------------------------------------------------------
                //Notify our scripts that everything went fine!
                GameEventMgr.Notify(ScriptEvent.Loaded);

                //---------------------------------------------------------------
                //Try to start the base server (open server port for connections)
                if (!InitComponent(base.Start(), "base.Start()"))
                    return false;

                //---------------------------------------------------------------
                //Notify everyone that the server is now started!
                GameEventMgr.Notify(GameServerEvent.Started, this);

                GC.Collect(GC.MaxGeneration);


                if (log.IsInfoEnabled)
                    log.Info("GameServer is now open for connections!");

                return true;
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("Failed to start the server", e);

                return false;
            }
        }

        /// <summary>
        /// Logs unhandled exceptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            log.Fatal("Unhandled exception!\n" + e.ExceptionObject.ToString());
            if (e.IsTerminating)
                LogManager.Shutdown();
        }

        /// <summary>
        /// Initialize all script components
        /// </summary>
        /// <returns>true if successfull, false if not</returns>
        protected bool StartScriptComponents()
        {
            try
            {
                //---------------------------------------------------------------
                //Register all event handlers
                ArrayList scripts = new ArrayList(ScriptMgr.Scripts);
                scripts.Insert(0, typeof(GameServer).Assembly);
                foreach (Assembly asm in scripts)
                {
                    GameEventMgr.RegisterGlobalEvents(asm, typeof(GameServerStartedEventAttribute), GameServerEvent.Started);
                    GameEventMgr.RegisterGlobalEvents(asm, typeof(GameServerStoppedEventAttribute), GameServerEvent.Stopped);
                    GameEventMgr.RegisterGlobalEvents(asm, typeof(ScriptLoadedEventAttribute), ScriptEvent.Loaded);
                    GameEventMgr.RegisterGlobalEvents(asm, typeof(ScriptUnloadedEventAttribute), ScriptEvent.Unloaded);
                }
                if (log.IsInfoEnabled)
                    log.Info("Registering global event handlers: true");
            }
            catch (Exception e)
            {
                if (log.IsErrorEnabled)
                    log.Error("StartScriptComponents", e);
                return false;
            }
            //---------------------------------------------------------------
            return true;
        }

        /// <summary>
        /// Prints out some text info on component initialisation
        /// and stops the server again if the component failed
        /// </summary>
        /// <param name="componentInitState">The state</param>
        /// <param name="text">The text to print</param>
        /// <returns>false if startup should be interrupted</returns>
        protected bool InitComponent(bool componentInitState, string text)
        {
            if (_debugMemory)
                log.Debug("Start Memory " + text + ": " + GC.GetTotalMemory(false) / 1024 / 1024);
            if (log.IsInfoEnabled)
                log.Info(text + ": " + componentInitState);
            if (!componentInitState)
                Stop();
            if (_debugMemory)
                log.Debug("Finish Memory " + text + ": " + GC.GetTotalMemory(false) / 1024 / 1024);
            return componentInitState;
        }

        public GameClient[] GetAllClients()
        {
            GameClient[] list = null;

            lock (_clients.SyncRoot)
            {
                list = new GameClient[_clients.Count];
                _clients.Keys.CopyTo(list, 0);
            }
            return list;
        }

        #endregion

        #region Stop

        /// <summary>
        /// Stops the server, disconnects all clients, and writes the database to disk
        /// </summary>
        public override void Stop()
        {

            log.Info("GameServer.Stop() - enter method");

            //Stop the base server
            base.Stop();

            Thread.CurrentThread.Priority = ThreadPriority.BelowNormal;

            if (log.IsInfoEnabled)
                log.Info("Server Stopped");

            LogManager.Shutdown();
        }

        #endregion

        #region Singleton
        /// <summary>
        /// The singleton instance
        /// </summary>
        private static GameServer _instance;

        /// <summary>
        /// Returns the singleton instance
        /// </summary>
        public static GameServer Instance
        {
            get
            {
                return _instance;
            }
        }
        #endregion
    }
}
