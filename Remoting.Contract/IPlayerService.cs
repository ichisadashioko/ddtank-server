using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Remoting.Contract.Data;

namespace Remoting.Contract
{
    [ServiceContract]
    public interface IPlayerService
    {
        [OperationContract]
        PlayerInfo Login(string username, string password);

        [OperationContract]
        int UpdatePlayer(PlayerInfo player);

        [OperationContract]
        int UpdatePlayers(PlayerInfo[] list);

        [OperationContract]
        ItemInfo[] GetAllGrids(int UserID);

        [OperationContract]
        int BuyGoods(int UserID, int GoodsID, int count, int[] place);

        [OperationContract]
        int RepairGoods(int userID, int place);

        [OperationContract]
        int DeleteGoods(int UserID,int place);

        [OperationContract]
        int BreakGoods(int userID, int start, int end, int count);

        [OperationContract]
        int UnchainGoods(int userID, int start, int end);

        [OperationContract]
        int SendMail(MailInfo mail);

        [OperationContract]
        int DeleteMail(int UserID, int mailID);

        [OperationContract]
        int UpdateMail(int UserID, int mailID);

        [OperationContract]
        int EquipItem(int userId, int itemId);

        [OperationContract]
        int UnequipItem(int userId, int itemId);
    }
}
