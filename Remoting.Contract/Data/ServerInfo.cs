using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Remoting.Contract.Data
{
    public class ServerInfo
    {
        public int ID;

        public string Name;

        public string IP;

        public int Port;

        public int State;

        public int Online;

        public int Total;

        public int Room;

        public string Remark;
    }
}
