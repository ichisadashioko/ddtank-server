using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Remoting.Contract.Data
{
    [DataContract]
    [Serializable]
    public class PlayerInfo:DataObject
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string NickName{ get; set; }

        [DataMember]
        public bool Sex{ get; set; }

        [DataMember]
        public int Attack { get; set; }

        [DataMember]
        public int Defence { get; set; }

        [DataMember]
        public int Luck { get; set; }

        [DataMember]
        public int Agility { get; set; }

        [DataMember]
        public int Gold { get; set; }

        [DataMember]
        public int Money { get; set; }

        [DataMember]
        public string Style { get; set; }

        [DataMember]
        public string Colors { get; set; }

        [DataMember]
        public int Grade { get; set; }

        [DataMember]
        public int GP { get; set; }

        [DataMember]
        public int State { get; set; }

        [DataMember]
        public int ConsortiaID { get; set; }
    }
}
