using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Remoting.Contract.Data
{
    [DataContract]
    [Serializable]
    public class MailInfo:DataObject
    {
        [DataMember]
        public int ID;

        [DataMember]
        public int SenderID;

        [DataMember]
        public string Sender;

        [DataMember]
        public int ReceiverID;

        [DataMember]
        public string Receiver;

        [DataMember]
        public string Title;

        [DataMember]
        public string Content;

        [DataMember]
        public string Annex1;

        [DataMember]
        public string Annex2;

        [DataMember]
        public int Gold;

        [DataMember]
        public int Money;

        [DataMember]
        public bool IsExist;
    }
}
