using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Remoting.Contract.Data
{
    [DataContract]
    [Serializable]
    public class ItemInfo:ItemTemplateInfo
    {
        [DataMember]
        public int ItemId { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int Place { get; set; }

        [DataMember]
        public int Gold { get; set; }

        [DataMember]
        public int Money { get; set; }

        [DataMember]
        public int Durability { get; set; }

        [DataMember]
        public string Data { get; set; }

        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public string Color { get; set; }

        [DataMember]
        public bool IsJudge { get; set; }

    }
}
