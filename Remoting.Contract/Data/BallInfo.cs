using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Remoting.Contract.Data
{
    [Serializable]
    [DataContract]
    public class BallInfo
    {
        [DataMember]
        public int ID;

        [DataMember]
        public string Name;

        [DataMember]
        public int Power;

        [DataMember]
        public int Radii;

        [DataMember]
        public int Amount;
    }
}
