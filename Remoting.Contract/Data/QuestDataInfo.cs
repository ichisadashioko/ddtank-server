using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Remoting.Contract.Data
{
    public class QuestDataInfo:DataObject
    {

        public int UserId { set; get; }

        public int QuestId { set; get; }

        public int KillCount { set; get; }

        public int WinCount { set; get; }

        public int UserCount { set; get; }

        public bool IsComplete { set; get; }

        public string CompletedDate { set; get; }
    }
}
