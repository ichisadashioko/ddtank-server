using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Remoting.Contract.Data
{
    public class QuestInfo : DataObject
    {
        public int QuestId { set; get; }

        public int Level{ set; get; }

        public int NeedMinLevel{ set; get; }

        public int NeedMaxLevel{ set; get; }

        public int Type{ set; get; }

        public int PreQuestId{ set; get; }

        public int NextQuestId{ set; get; }

        public bool CanRepeat{ set; get; }

        public int RepeatInterval{ set; get; }

        public string Title{ set; get; }

        public string Detail{ set; get; }

        public string Objective{ set; get; }

        public int ReqItemId{ set; get; }

        public int ReqItemCount{ set; get; }

        public int ReqUseItemId{ set; get; }

        public int ReqUseCount{ set; get; }

        public int ReqKillLevel{ set; get; }

        public bool ReqKillMustBeCaptain{ set; get; }

        public int ReqKillCount{ set; get; }

        public int ReqWinMap{ set; get; }

        public bool ReqWinMustBeCaptain{ set; get; }

        public int ReqWinCount{ set; get; }

        public int RewardGold{ set; get; }

        public int RewardGP{ set; get; }

        public int RewardItemId{ set; get; }

        public int RewardItemCount{ set; get; }

        public int RewardItemValidateTime{ set; get; }
    }
}
