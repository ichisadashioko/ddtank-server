using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Remoting.Contract.Data
{
    [DataContract]
    [Serializable]
    public class MapGoodsInfo
    {
        [DataMember]
        public int ID;

        [DataMember]
        public int MapID;

        [DataMember]
        public int GoodsID;

        [DataMember]
        public int Random;
    }
}
