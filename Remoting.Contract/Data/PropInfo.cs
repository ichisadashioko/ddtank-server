using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Remoting.Contract.Data
{
    [DataContract]
    [Serializable]
    public class PropInfo
    {
        [DataMember]
        public int ID;

        [DataMember]
        public string Name;

        [DataMember]
        public int Category;

        [DataMember]
        public string Script;

        [DataMember]
        public int AffectTimes;

        [DataMember]
        public int AffectArea;

        [DataMember]
        public int Parameter;

        [DataMember]
        public int Random;

        [DataMember]
        public int BuyGold;

        [DataMember]
        public int BuyMoney;

        [DataMember]
        public int Delay;

        [DataMember]
        public string Icon;

        [DataMember]
        public string Pic;

        [DataMember]
        public string Description;

        [DataMember]
        public int AttackTimes;

        [DataMember]
        public int BoutTimes;

        [DataMember]
        public int Property1;

        [DataMember]
        public int Property2;

        [DataMember]
        public int Property3;
    }
}
