using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Remoting.Contract.Data
{
    [DataContract]
    [Serializable]
    public class ItemTemplateInfo:DataObject
    {
        [DataMember]
        public int TemplateId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int CategoryId { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int Attack { get; set; }

        [DataMember]
        public int Defence { get; set; }

        [DataMember]
        public int Luck { get; set; }

        [DataMember]
        public int Agility { get; set; }

        [DataMember]
        public int Level { get; set; }

        [DataMember]
        public string Pic { get; set; }

        [DataMember]
        public DateTime? AddTime { get; set; }

        [DataMember]
        public int BuyGold { get; set; }

        [DataMember]
        public int BuyMoney { get; set; }

        [DataMember]
        public int SellGold { get; set; }

        [DataMember]
        public int SellMoney { get; set; }

        [DataMember]
        public int Quality { get; set; }

        [DataMember]
        public int MaxCount { get; set; }

        [DataMember]
        public int MaxDurability { get; set; }

        [DataMember]
        public string Property1 { get; set; }

        [DataMember]
        public string Property2 { get; set; }

        [DataMember]
        public string Property3 { get; set; }

        [DataMember]
        public string Property4 { get; set; }

        [DataMember]
        public string Property5 { get; set; }

        [DataMember]
        public string Property6 { get; set; }

        [DataMember]
        public string Property7 { get; set; }

        [DataMember]
        public string Property8 { get; set; }

        [DataMember]
        public int NeedSex { get; set; }

        [DataMember]
        public int NeedLevel { get; set; }

        [DataMember]
        public bool CanDrop { get; set; }

        [DataMember]
        public bool CanDelete { get; set; }

        [DataMember]
        public bool CanEquip { get; set; }

        [DataMember]
        public bool CanUse { get; set; }

        [DataMember]
        public string Script { get; set; }

        [DataMember]
        public bool IsShow { get; set; }

        [DataMember]
        public string Colors { get; set; }

        [DataMember]
        public eUpdateType UpdateType { get; set; }
    }
}
