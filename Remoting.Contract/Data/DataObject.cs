using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Remoting.Contract.Data
{
    [DataContract]
    [Serializable]
    public class DataObject
    {
        public bool IsNew { get; set; }

        public bool IsDelete { get; set; }

        public bool IsUpdate { get; set; }
    }
}
