using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Remoting.Contract.Data
{
    [DataContract]
    [Serializable]
    public class MapInfo
    {
        [DataMember]
        public int ID;

        [DataMember]
        public string Code;

        [DataMember]
        public string Name;

        [DataMember]
        public string PosX;

    }
}
