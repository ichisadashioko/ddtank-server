using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Remoting.Contract.Data;

namespace Remoting.Contract
{
    [ServiceContract]
    public interface IProduceService
    {
        [OperationContract]
        ItemTemplateInfo[] GetAllGoods();

        [OperationContract]
        PropInfo[] GetAllProp();

        [OperationContract]
        BallInfo[] GetAllBall();
    }
}
