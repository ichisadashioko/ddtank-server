using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Phy.Maps;

namespace SaveMapToFile
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dig = new FolderBrowserDialog();
            dig.ShowDialog();
            tbxSourceMapPath.Text = dig.SelectedPath;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dig = new FolderBrowserDialog();
            dig.ShowDialog();
            tbxDestMapPath.Text = dig.SelectedPath;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dig = new FolderBrowserDialog();
            dig.ShowDialog();
            tbxSourceBombPath.Text = dig.SelectedPath;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dig = new FolderBrowserDialog();
            dig.ShowDialog();
            tbxDestBombPath.Text = dig.SelectedPath;
        }

        private void btnMakeMap_Click(object sender, EventArgs e)
        {
            string[] list = Directory.GetDirectories(tbxSourceMapPath.Text);
            if (list.Length > 0 &&　!string.IsNullOrEmpty(tbxDestMapPath.Text))
            {
                if(!Directory.Exists(tbxDestMapPath.Text))
                {
                    Directory.CreateDirectory(tbxDestMapPath.Text);
                }
                tbxMsg.AppendText(string.Format("共 {0} 文件...\r\n", list.Length));

                foreach (string map in list)
                {
                   
                    FileInfo file = new FileInfo(string.Format("{0}\\{1}", map, "fore.png"));
                    if(File.Exists(file.FullName))
                    {   
                        Bitmap bitmap = new Bitmap(file.FullName);
                        Tile tile = new Tile(bitmap,true);

                        FileInfo dest = new FileInfo(string.Format("{0}\\{1}\\{2}", tbxDestMapPath.Text, file.Directory.Name, "fore.map"));

                        tbxMsg.AppendText(string.Format("开始生成 地图 {0}------>{1}\r\n", file.FullName, dest.FullName));
                        if(!Directory.Exists(dest.DirectoryName))
                        {
                            Directory.CreateDirectory(dest.DirectoryName);
                        }

                        FileStream fs = File.Create(dest.FullName);
                        BinaryWriter writer = new BinaryWriter(fs);
                        writer.Write(tile.Width);
                        writer.Write(tile.Height);
                        writer.Flush();
                        fs.Write(tile.Data, 0, tile.Data.Length);
                        fs.Close();
                    }

                    file = new FileInfo(string.Format("{0}\\{1}", map, "dead.png"));
                    if (File.Exists(file.FullName))
                    {
                        Bitmap bitmap = new Bitmap(file.FullName);
                        Tile tile = new Tile(bitmap,true);

                        FileInfo dest = new FileInfo(string.Format("{0}\\{1}\\{2}", tbxDestMapPath.Text, file.Directory.Name, "dead.map"));

                        if (!Directory.Exists(dest.DirectoryName))
                        {
                            Directory.CreateDirectory(dest.DirectoryName);
                        }
                        tbxMsg.AppendText(string.Format("开始生成 地图 {0}------>{1}\r\n", file.FullName, dest.FullName));
                        FileStream fs = File.Create(dest.FullName);
                        BinaryWriter writer = new BinaryWriter(fs);
                        writer.Write(tile.Width);
                        writer.Write(tile.Height);
                        writer.Flush();
                        fs.Write(tile.Data, 0, tile.Data.Length);
                        fs.Close();
                        
                    }
                }
 
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Tile tile = new Tile("fore.map",true);
            pictureBox1.Image = tile.ToBitmap();
        }

        private void tbxMakBomb_Click(object sender, EventArgs e)
        {
            string[] list = Directory.GetFiles(tbxSourceBombPath.Text,"*.png");
            if (list.Length > 0 && !string.IsNullOrEmpty(tbxDestBombPath.Text))
            {
                if (!Directory.Exists(tbxDestMapPath.Text))
                {
                    Directory.CreateDirectory(tbxDestMapPath.Text);
                }
                tbxMsg.AppendText(string.Format("共 {0} 文件...\r\n", list.Length));

                foreach (string f in list)
                {

                    FileInfo file = new FileInfo(f);
                    if (File.Exists(file.FullName))
                    {
                        Bitmap bitmap = new Bitmap(file.FullName);
                        Tile tile = new Tile(bitmap,true);

                        FileInfo dest = new FileInfo(string.Format("{0}\\{1}.bomb", tbxDestBombPath.Text, file.Name.Substring(0,file.Name.Length - 4)));

                        tbxMsg.AppendText(string.Format("开始生成 炸弹文件 {0}------>{1}\r\n", file.FullName, dest.FullName));
                        if (!Directory.Exists(dest.DirectoryName))
                        {
                            Directory.CreateDirectory(dest.DirectoryName);
                        }

                        FileStream fs = File.Create(dest.FullName);
                        BinaryWriter writer = new BinaryWriter(fs);
                        writer.Write(tile.Width);
                        writer.Write(tile.Height);
                        writer.Flush();
                        fs.Write(tile.Data, 0, tile.Data.Length);
                        fs.Close();
                    }

                   
                }

            }
        }

        private Tile current = null;
        private void button6_Click(object sender, EventArgs e)
        {
            Tile tile = new Tile("10.bomb",true);

            if (current == null)
            {
                current = tile;
            }
            else
            {
                DateTime dt = DateTime.Now;

                for (int i = 0; i < 10000; i++)
                {
                    current.Dig(int.Parse(tbxX.Text), int.Parse(tbxY.Text), tile, null);
                }

                Console.WriteLine(DateTime.Now - dt);
            }
            pictureBox1.Image = current.ToBitmap();
        }
    }
}
