namespace SaveMapToFile
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMakeMap = new System.Windows.Forms.Button();
            this.tbxSourceMapPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxMsg = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxDestMapPath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxSourceBombPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxDestBombPath = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.tbxMakBomb = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tbxX = new System.Windows.Forms.TextBox();
            this.tbxY = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnMakeMap
            // 
            this.btnMakeMap.Location = new System.Drawing.Point(360, 10);
            this.btnMakeMap.Name = "btnMakeMap";
            this.btnMakeMap.Size = new System.Drawing.Size(102, 23);
            this.btnMakeMap.TabIndex = 2;
            this.btnMakeMap.Text = "生成地图";
            this.btnMakeMap.UseVisualStyleBackColor = true;
            this.btnMakeMap.Click += new System.EventHandler(this.btnMakeMap_Click);
            // 
            // tbxSourceMapPath
            // 
            this.tbxSourceMapPath.Location = new System.Drawing.Point(72, 12);
            this.tbxSourceMapPath.Name = "tbxSourceMapPath";
            this.tbxSourceMapPath.Size = new System.Drawing.Size(230, 21);
            this.tbxSourceMapPath.TabIndex = 3;
            this.tbxSourceMapPath.Text = "\\\\192.168.0.4\\Tank\\TestImg\\img\\map";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "地图目录";
            // 
            // tbxMsg
            // 
            this.tbxMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxMsg.Location = new System.Drawing.Point(13, 125);
            this.tbxMsg.Multiline = true;
            this.tbxMsg.Name = "tbxMsg";
            this.tbxMsg.Size = new System.Drawing.Size(567, 185);
            this.tbxMsg.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "目标目录";
            // 
            // tbxDestMapPath
            // 
            this.tbxDestMapPath.Location = new System.Drawing.Point(72, 39);
            this.tbxDestMapPath.Name = "tbxDestMapPath";
            this.tbxDestMapPath.Size = new System.Drawing.Size(230, 21);
            this.tbxDestMapPath.TabIndex = 7;
            this.tbxDestMapPath.Text = "c:\\map";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(308, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(36, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(308, 38);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(36, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "炸弹目录";
            // 
            // tbxSourceBombPath
            // 
            this.tbxSourceBombPath.Location = new System.Drawing.Point(72, 67);
            this.tbxSourceBombPath.Name = "tbxSourceBombPath";
            this.tbxSourceBombPath.Size = new System.Drawing.Size(230, 21);
            this.tbxSourceBombPath.TabIndex = 11;
            this.tbxSourceBombPath.Text = "\\\\192.168.0.4\\Tank\\Bomb";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "目标目录";
            // 
            // tbxDestBombPath
            // 
            this.tbxDestBombPath.Location = new System.Drawing.Point(72, 95);
            this.tbxDestBombPath.Name = "tbxDestBombPath";
            this.tbxDestBombPath.Size = new System.Drawing.Size(230, 21);
            this.tbxDestBombPath.TabIndex = 13;
            this.tbxDestBombPath.Text = "c:\\bomb";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(308, 93);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(36, 23);
            this.button4.TabIndex = 15;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(308, 65);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(36, 23);
            this.button5.TabIndex = 14;
            this.button5.Text = "...";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // tbxMakBomb
            // 
            this.tbxMakBomb.Location = new System.Drawing.Point(360, 67);
            this.tbxMakBomb.Name = "tbxMakBomb";
            this.tbxMakBomb.Size = new System.Drawing.Size(102, 23);
            this.tbxMakBomb.TabIndex = 16;
            this.tbxMakBomb.Text = "生成炸弹数据";
            this.tbxMakBomb.UseVisualStyleBackColor = true;
            this.tbxMakBomb.Click += new System.EventHandler(this.tbxMakBomb_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(468, 10);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "测试读取地图";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(468, 65);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 18;
            this.button6.Text = "测试读取炸弹";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(13, 316);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(567, 171);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // tbxX
            // 
            this.tbxX.Location = new System.Drawing.Point(360, 98);
            this.tbxX.Name = "tbxX";
            this.tbxX.Size = new System.Drawing.Size(76, 21);
            this.tbxX.TabIndex = 20;
            this.tbxX.Text = "10";
            // 
            // tbxY
            // 
            this.tbxY.Location = new System.Drawing.Point(468, 98);
            this.tbxY.Name = "tbxY";
            this.tbxY.Size = new System.Drawing.Size(75, 21);
            this.tbxY.TabIndex = 21;
            this.tbxY.Text = "10";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 499);
            this.Controls.Add(this.tbxY);
            this.Controls.Add(this.tbxX);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tbxMakBomb);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.tbxDestBombPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxSourceBombPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbxDestMapPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxMsg);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxSourceMapPath);
            this.Controls.Add(this.btnMakeMap);
            this.Name = "Form1";
            this.Text = "地图入库";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMakeMap;
        private System.Windows.Forms.TextBox tbxSourceMapPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxMsg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxDestMapPath;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbxSourceBombPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxDestBombPath;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button tbxMakBomb;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox tbxX;
        private System.Windows.Forms.TextBox tbxY;
    }
}

