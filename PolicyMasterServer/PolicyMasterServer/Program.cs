using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolicyMasterServer
{
    class Program
    {
        static void Main(string[] args)
        {
            PolicyServer svr = new PolicyServer();

            svr.Start();

            bool run = true;
            while (run)
            {
                Console.Write(">");
                string line = Console.ReadLine();
                switch (line)
                {
                    case "stop":
                        run = false;
                        break;
                    case "state":
                        Console.WriteLine(string.Format("client:{0}", svr.ClientCount));
                        break;
                }
            }
            svr.Stop();
        }
    }
}
