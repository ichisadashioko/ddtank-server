using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoadDatabase;
using Remoting.Contract.Data;
using Remoting.Contract;
using System.Xml.Linq;
using System.Data.Linq;
using System.Text.RegularExpressions;

namespace Remoting.Bussiness
{
    public class Player : IDisposable
    {
        private TankDBDataContext db;

        public Player()
        {
            db = new TankDBDataContext();
        }

        #region PlayerInfo

        public PlayerInfo Login(string username, string password)
        {

            WebLogin.PassPortSoapClient login = new WebLogin.PassPortSoapClient();
            int id = int.Parse(login.ChenckValidate(username, password));
            if (id != 0)
            {
                var query = from d in db.Sys_Users_Details
                            join f in db.Sys_Users_Fights on d.UserID equals f.UserID
                            where d.UserID == id && !string.IsNullOrEmpty(d.NickName)
                            select new PlayerInfo()
                            {
                                Agility = f.Celerity,
                                Attack = f.Attack,
                                Colors = d.Colors,
                                Defence = f.Recovery,
                                Gold = d.Gold,
                                GP = d.GP,
                                Grade = d.Grade,
                                Id = d.UserID,
                                Luck = f.Luck,
                                Money = d.Money,
                                NickName = d.NickName,
                                Sex = d.Sex,
                                Style = d.Style,
                            };
                return query.SingleOrDefault();
            }
            return null;
        }

        public int UpdatePlayer(PlayerInfo player)
        {
            int result = 1;
            Sys_Users_Detail detail = db.Sys_Users_Details.SingleOrDefault(c => c.UserID == player.Id);
            Sys_Users_Fight fight = db.Sys_Users_Fights.SingleOrDefault(c => c.UserID == player.Id);
            if (detail != null && fight != null)
            {
                fight.Celerity = player.Agility;
                fight.Attack = player.Attack;
                detail.Colors = player.Colors;
                fight.Recovery = player.Defence;
                detail.Gold = player.Gold;
                detail.GP = player.GP;
                detail.Grade = player.Grade;
                fight.Luck = player.Luck;
                detail.Money = player.Money;
                detail.Style = player.Style;
                db.SubmitChanges();
                result = 0;
            }

            return result;
        }


        public int UpdatePlayers(PlayerInfo[] list)
        {
            int result = 1;
            foreach (PlayerInfo player in list)
            {
                Sys_Users_Detail detail = db.Sys_Users_Details.SingleOrDefault(c => c.UserID == player.Id);
                Sys_Users_Fight fight = db.Sys_Users_Fights.SingleOrDefault(c => c.UserID == player.Id);
                if (detail != null && fight != null)
                {
                    fight.Celerity = player.Agility;
                    fight.Attack = player.Attack;
                    detail.Colors = player.Colors;
                    fight.Recovery = player.Defence;
                    detail.Gold = player.Gold;
                    detail.GP = player.GP;
                    detail.Grade = player.Grade;
                    fight.Luck = player.Luck;
                    detail.Money = player.Money;
                    detail.Style = player.Style;
                }
            }
            try
            {
                db.SubmitChanges();
                    result = 0;
            }
            catch
            {
                result = 2;
            }
            return result;
        }

        #endregion

        #region GoodsInfo

        public ItemInfo[] GetAllGrids(int UserID)
        {
            var query = from g in db.Sys_Users_Goods
                        where g.UserID == UserID && g.IsExist == true
                        select new ItemInfo
                        {
                            Agility = g.Celerity,
                            Attack = g.Attack,
                            CanDelete = g.IsDelete,
                            CanDrop = g.IsDrop,
                            CanEquip = g.IsEquip,
                            CanUse = g.IsUse,
                            Color = g.Color,
                            Count = g.Count,
                            Data = g.Data,
                            Defence = g.Recovery,
                            Durability = g.Wear,
                            Gold = (int)g.SellGold,
                            IsJudge = g.IsJudge,
                            ItemId = g.ID,
                            Level = g.Grade,
                            Luck = g.Luck,
                            Money = (int)g.SellMoney,
                            NeedLevel  = g.Grade,
                            NeedSex = g.Sex,
                            Place = g.Place,
                            Quality = g.Count,
                            Script = g.Script,
                            TemplateId = g.GoodsID,
                            UserId = g.UserID,
                        };
            return query.ToArray();
        }

        public int BuyGoods(int UserID, int GoodsID, int count, int[] place)
        {
            Shop_Good goods = db.Shop_Goods.SingleOrDefault(g => g.ID == GoodsID);
            if (goods != null)
                return 5;

            int len = 0;
            Sys_Users_Detail detail = db.Sys_Users_Details.SingleOrDefault(c => c.UserID == UserID);
            for (int i = 0; i < count; i += (int)goods.LimitCount)
            {
                int index = -1;
                for (int j = 0; j < place.Length; j++)
                {
                    if (place[j] != -1)
                    {
                        index = place[j];
                        place[j] = -1;
                        break;
                    }
                }

                len = i + goods.LimitCount > count ? count - i : (int)goods.LimitCount;
                if (index == -1)
                {
                    return 2;
                }
                else if (goods.BuyGold * len > detail.Gold)
                {
                    return 3;
                }
                else if (goods.BuyMoney * len > detail.Money)
                {
                    return 4;
                }
                else
                {
                    Sys_Users_Good userGoods = new Sys_Users_Good();
                    userGoods.UserID = UserID;
                    userGoods.GoodsID = goods.ID;
                    userGoods.AngleStart = goods.AngleStart;
                    userGoods.AngleBound = goods.AngleBound;
                    userGoods.Dander = goods.Dander;
                    userGoods.Attack = goods.Attack;
                    userGoods.Recovery = goods.Recovery;
                    userGoods.Luck = goods.Luck;
                    userGoods.Celerity = goods.Celerity;
                    userGoods.BuyMoney = goods.BuyMoney;
                    userGoods.BuyGold = goods.BuyGold;
                    userGoods.SellGold = goods.SellGold;
                    userGoods.SellMoney = goods.SellMoney;
                    userGoods.Tone = goods.Tone;
                    userGoods.IsDelete = goods.IsDelete;
                    userGoods.IsDrop = goods.IsDrop;
                    userGoods.IsEquip = goods.IsEquip;
                    userGoods.IsUse = goods.IsUse;
                    userGoods.IsJudge = true;
                    userGoods.Data = goods.Data;
                    userGoods.Grade = goods.Grade;
                    userGoods.Wear = goods.Wear;
                    userGoods.Count = count;
                    userGoods.Place = index;
                    userGoods.Sex = goods.Sex;
                    userGoods.IsExist = true;
                    db.Sys_Users_Goods.InsertOnSubmit(userGoods);

                    detail.Money -= (int)goods.BuyMoney * len;
                    detail.Gold -= (int)goods.BuyGold * len;
                }
            }

            try
            {
                db.SubmitChanges();
            }
            catch
            {
                return 1;
            }
            return 0;
        }

        public int UpdateGoods(int UserID, ItemInfo goods)
        {
            Sys_Users_Good userGoods = db.Sys_Users_Goods.SingleOrDefault(c => c.ID == goods.ItemId);
            if (userGoods != null)
            {
                userGoods.Attack = goods.Attack;
                userGoods.Recovery = goods.Defence;
                userGoods.Luck = goods.Luck;
                userGoods.Celerity = goods.Agility;
                userGoods.IsJudge = goods.IsJudge;
                userGoods.Data = goods.Data;
                userGoods.Grade = goods.Level;
                userGoods.Wear = goods.Durability;
                userGoods.Count = goods.Quality;
                userGoods.Place = goods.Place;
                userGoods.Sex = goods.NeedSex;
                db.Sys_Users_Goods.InsertOnSubmit(userGoods);
                db.SubmitChanges();
                return 0;
            }
            return 1;

        }

        public int SellGoods(int UserID, int place)
        {
            Sys_Users_Good goods = db.Sys_Users_Goods.SingleOrDefault(g => g.Place == place &&
                    g.UserID == UserID && g.Place > 10);
            if (goods != null)
            {
                Sys_Users_Detail detail = db.Sys_Users_Details.SingleOrDefault(c => c.UserID == UserID);
                detail.Money += (int)goods.SellMoney * goods.Count;
                detail.Gold  += (int)goods.SellGold * goods.Count;
                goods.IsExist = false;
                db.SubmitChanges();
                return 0;
            }
            return 1;
        }

        public int AddGoods(int UserID, int GoodsID, int count, int place)
        {
            Shop_Good goods = db.Shop_Goods.SingleOrDefault(g => g.ID == GoodsID);
            if (goods != null)
                return 5;

            Sys_Users_Good userGoods = new Sys_Users_Good();
            userGoods.UserID = UserID;
            userGoods.GoodsID = goods.ID;
            userGoods.AngleStart = goods.AngleStart;
            userGoods.AngleBound = goods.AngleBound;
            userGoods.Dander = goods.Dander;
            userGoods.Attack = goods.Attack;
            userGoods.Recovery = goods.Recovery;
            userGoods.Luck = goods.Luck;
            userGoods.Celerity = goods.Celerity;
            userGoods.BuyMoney = goods.BuyMoney;
            userGoods.BuyGold = goods.BuyGold;
            userGoods.SellGold = goods.SellGold;
            userGoods.SellMoney = goods.SellMoney;
            userGoods.Tone = goods.Tone;
            userGoods.IsDelete = goods.IsDelete;
            userGoods.IsDrop = goods.IsDrop;
            userGoods.IsEquip = goods.IsEquip;
            userGoods.IsUse = goods.IsUse;
            userGoods.IsJudge = true;
            userGoods.Data = goods.Data;
            userGoods.Grade = goods.Grade;
            userGoods.Wear = goods.Wear;
            userGoods.Count = count;
            userGoods.Place = place;
            userGoods.Sex = goods.Sex;
            userGoods.IsExist = true;

            try
            {
                db.Sys_Users_Goods.InsertOnSubmit(userGoods);
                db.SubmitChanges();
                return 0;
            }
            catch
            {
                return 1;
            }
        }

        public int DeleteGoods(int UserID, int place)
        {
            int result = 0;
            Sys_Users_Good goods = db.Sys_Users_Goods.SingleOrDefault(g => g.Place == place && g.UserID == UserID);
            if (goods != null && place > 10)
            {
                goods.IsExist = false;
                db.SubmitChanges();
                result = 1;
            }
            return result;
        }

        public int RepairGoods(int userID, int place)
        {
            Sys_Users_Detail detail = db.Sys_Users_Details.SingleOrDefault(c => c.UserID == userID);
            var goodses = from g in db.Sys_Users_Goods
                          where g.UserID == userID && g.IsExist ==true
                          select g;

            foreach (Sys_Users_Good goods in goodses)
            {
                if (place != -1 && place != goods.Place)
                    continue;

                Shop_Good temp = db.Shop_Goods.SingleOrDefault(g => g.ID == goods.GoodsID );
                if (goods != null && temp.CategoryID < 9)
                {
                    if (goods.Wear < temp.Wear)
                    {
                        int gold = (int)((temp.Wear - goods.Wear) / temp.Wear * goods.BuyGold);
                        int money = (int)((temp.Wear - goods.Wear) / temp.Wear * goods.BuyMoney);
                        if (detail.Money >= money && detail.Gold >= gold)
                        {
                            goods.Wear = temp.Wear;
                            detail.Gold -=gold;
                            detail.Money -=money;
                        }
                    }

                }
            }
            try
            {
                db.SubmitChanges();
                return 0;
            }
            catch
            {
                return 1;
            }
        }

        public int BreakGoods(int userID, int start, int end, int count)
        {
            Sys_Users_Good goods = db.Sys_Users_Goods.SingleOrDefault(c=>c.Place ==start && c.UserID == userID);
            if (goods != null)
            {
                goods.Count = goods.Count - count;
                Sys_Users_Good userGoods = new Sys_Users_Good();
                userGoods.UserID = goods.UserID;
                userGoods.GoodsID = goods.ID;
                userGoods.AngleStart = goods.AngleStart;
                userGoods.AngleBound = goods.AngleBound;
                userGoods.Dander = goods.Dander;
                userGoods.Attack = goods.Attack;
                userGoods.Recovery = goods.Recovery;
                userGoods.Luck = goods.Luck;
                userGoods.Celerity = goods.Celerity;
                userGoods.BuyMoney = goods.BuyMoney;
                userGoods.BuyGold = goods.BuyGold;
                userGoods.SellGold = goods.SellGold;
                userGoods.SellMoney = goods.SellMoney;
                userGoods.Tone = goods.Tone;
                userGoods.IsDelete = goods.IsDelete;
                userGoods.IsDrop = goods.IsDrop;
                userGoods.IsEquip = goods.IsEquip;
                userGoods.IsUse = goods.IsUse;
                userGoods.IsJudge = goods.IsJudge;
                userGoods.Data = goods.Data;
                userGoods.Grade = goods.Grade;
                userGoods.Wear = goods.Wear;
                userGoods.Count = count;
                userGoods.Place = end;
                userGoods.Sex = goods.Sex;
                userGoods.IsExist = goods.IsExist;
                db.Sys_Users_Goods.InsertOnSubmit(userGoods);
            }
            try
            {
                db.SubmitChanges();
                return 0;
            }
            catch
            {
                return 1;
            }
        }

        public int UnchainGoods(int userID, int start, int end)
        {
            Sys_Users_Good userGoods = db.Sys_Users_Goods.SingleOrDefault(c => c.UserID == userID && c.Place == start);
            if (userGoods == null)
                return 1;

            Sys_Users_Fight fight = db.Sys_Users_Fights.SingleOrDefault(c => c.UserID == userID);
            userGoods.Place = end;
            fight.Attack -= userGoods.Attack;
            fight.Celerity -= userGoods.Celerity;
            fight.Dander -= userGoods.Dander;
            fight.Luck -= userGoods.Luck;
            fight.Recovery -= userGoods.Recovery;

            try
            {
                db.SubmitChanges();
                return 0;
            }
            catch
            {
                return 1;
            }
        }

        public int ChainGoods(int userID, int start, int end)
        {
            Sys_Users_Good userGoods = db.Sys_Users_Goods.SingleOrDefault(c => c.UserID == userID && c.Place == start && c.IsExist == true);
            if (userGoods == null)
                return 1;

            Sys_Users_Good goods2 = db.Sys_Users_Goods.SingleOrDefault(g => g.UserID == userID
                 && g.Place == end && g.IsExist == true);
 

            return 0;
        }

        #region function


        #endregion

        #endregion

        #region Mail

        public int SendMail(MailInfo mail)
        {
            try
            {
                Sys_Users_Detail receive = db.Sys_Users_Details.SingleOrDefault(c => c.NickName == mail.Receiver);
                if (receive == null)
                    return 2;

                User_Message message = new User_Message();
                Sys_Users_Detail sender = db.Sys_Users_Details.SingleOrDefault(c => c.UserID == mail.SenderID);
                if (!string.IsNullOrEmpty(mail.Annex1))
                {
                    Sys_Users_Good goods = db.Sys_Users_Goods.SingleOrDefault(g => g.ID == int.Parse(mail.Annex1));
                    if (goods != null && goods.Place > 10)
                    {
                        goods.UserID = 0;
                        message.Annex1 = mail.Annex1;
                    }
                }
                if (!string.IsNullOrEmpty(mail.Annex2))
                {
                    Sys_Users_Good goods = db.Sys_Users_Goods.SingleOrDefault(g => g.ID == int.Parse(mail.Annex2));
                    if (goods != null && goods.Place > 10)
                    {
                        goods.UserID = 0;
                        message.Annex2 = mail.Annex2;
                    }
                }

                if (mail.Gold > 0)
                {
                    if (sender.Gold > mail.Gold)
                    {
                        sender.Gold -= mail.Gold;
                        message.Gold = mail.Gold;
                    }
                }
                if (mail.Money > 0)
                {
                    if (sender.Money > mail.Money)
                    {
                        sender.Money -= mail.Money;
                        message.Money = mail.Money;
                    }
                }

                message.Content = mail.Content;
                message.IfDelS = false;
                message.IsDelete = false;
                message.IsDelR = false;
                message.IsExist = true;
                message.IsRead = true;
                message.Receiver = receive.NickName;
                message.ReceiverID = receive.UserID;
                message.Sender = mail.Sender;
                message.SenderID = mail.SenderID;
                message.SendTime = DateTime.Now;
                message.Title = mail.Title;
                db.User_Messages.InsertOnSubmit(message);
                db.SubmitChanges();
                return 0;
            }
            catch
            {
                return 1;
            }


        }

        public int DeleteMail(int UserID, int mailID)
        {
            User_Message mes = db.User_Messages.SingleOrDefault(m => m.ID == mailID && m.ReceiverID == UserID);
            if (mes != null)
            {
                mes.IsExist = false;
                db.SubmitChanges();
                return 0;
            }
            return 1;
        }

        public int UpdateMail(int UserID, int mailID)
        {
            User_Message mes = db.User_Messages.SingleOrDefault(m => m.ID == mailID && m.ReceiverID == UserID);
            if (mes != null)
            {
                mes.IsRead = true;
                mes.SendTime = DateTime.Now;
                db.SubmitChanges();
                return 0;
            }
            return 1;
        }

        #endregion

        public void Dispose()
        {
            db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
