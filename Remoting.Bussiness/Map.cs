using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoadDatabase;
using Remoting.Contract.Data;
using Remoting.Contract;
using System.Xml.Linq;
using System.Data.Linq;
using System.Text.RegularExpressions;

namespace Remoting.Bussiness
{
    public class Map:IDisposable
    {     
        private TankDBDataContext db;

        public Map()
        {
            db = new TankDBDataContext();
        }


        public MapInfo[] GetAllMap()
        {
            var query = from c in db.Game_Maps
                        select new MapInfo
                        {
                            Code = c.Code,
                            ID = c.ID,
                            Name = c.Name,
                            PosX = c.PosX,
                        };
            return query.ToArray();
        }

        public MapGoodsInfo[] GetAllMapGoods()
        {
            var query = from c in db.Map_Goods
                        select new MapGoodsInfo
                        {
                            GoodsID = c.GoodsID,
                            ID = c.ID,
                            MapID = c.MapID,
                            Random = c.Random,
                        };
            return query.ToArray();
        }

        public void Dispose()
        {
            db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
