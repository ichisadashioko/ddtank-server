using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using Remoting.Contract.Data;
using RoadDatabase;

namespace Remoting.Bussiness
{
    public class Server:IDisposable
    {
        public TankDBDataContext db;
        public Server()
        {
            db = new TankDBDataContext();
        }

        public ServerInfo GetServerInfo(int ID)
        {
            ServerInfo service = (from c in db.Server_Lists
                                  where c.ID == ID
                                  select new ServerInfo
                                  {
                                      ID = c.ID,
                                      IP = c.IP,
                                      Name = c.Name,
                                      Online = c.Online,
                                      Port = (int)c.Port,
                                      Remark = c.Remark,
                                      Room = c.Room,
                                      State = c.State,
                                      Total = c.Total,
                                  }).SingleOrDefault();
            return service;
        }

        public ServerInfo[] GetAllServer()
        {
            var query = from c in db.Server_Lists
                        select new ServerInfo
                          {
                              ID = c.ID,
                              IP = c.IP,
                              Name = c.Name,
                              Online = c.Online,
                              Port = (int)c.Port,
                              Remark = c.Remark,
                              Room = c.Room,
                              State = c.State,
                              Total = c.Total,
                          };
            return query.ToArray();
        }

        public void Dispose()
        {
            db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
