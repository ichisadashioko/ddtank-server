using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoadDatabase;
using Remoting.Contract.Data;
using Remoting.Contract;
using System.Xml.Linq;
using System.Data.Linq;
using System.Text.RegularExpressions;

namespace Remoting.Bussiness
{
    public class Produce : IDisposable
    {
        private TankDBDataContext db;

        public Produce()
        {
            db = new TankDBDataContext();
        }

        public ItemTemplateInfo[] GetAllGoods()
        {
            var query = from c in db.Shop_Goods
                        select new ItemTemplateInfo
                        {
                            AddTime = c.AddTime,
                            Agility = c.Celerity,
                            Attack =c.Attack,
                            BuyGold = (int)c.BuyGold,
                            BuyMoney = (int)c.BuyMoney,
                            CanDelete = c.IsDelete,
                            CanDrop = c.IsDrop,
                            CanEquip = c.IsEquip,
                            CanUse = c.IsUse,
                            CategoryId = c.CategoryID,
                            Colors = c.Colors,
                            Defence = c.Recovery,
                            Description = c.Description,
                            IsShow = c.IsShow,
                            Level = c.Grade,
                            Luck = c.Luck,
                            MaxCount = c.LimitCount,
                            MaxDurability = c.Wear,
                            Name = c.Name,
                            NeedSex = c.Sex,
                            Pic = c.Pic,
                            Property1 = c.Property1,
                            Property2 = c.Property2,
                            Property3 = c.Property3,
                            Property4 = c.Property4,
                            Property5 = c.Property5,
                            Property6 = c.Property6,
                            Property7 = c.Property7,
                            Property8 = c.Property8,
                            Quality = c.Count,
                            Script = c.Script,
                            SellGold = (int)c.SellGold,
                            SellMoney = (int)c.SellMoney,
                            TemplateId = c.ID,                            
                        };

            return query.ToArray();
        }

        public PropInfo[] GetAllProp()
        {
            var query = from c in db.Props
                        select new PropInfo
                        {
                            AffectArea =c.AffectArea,
                            AffectTimes = c.AffectTimes,
                            AttackTimes = c.AttackTimes,
                            BoutTimes = c.BoutTimes,
                            BuyGold = c.BuyGold,
                            BuyMoney = c.BuyMoney,
                            Category = c.Category,
                            Delay = c.Delay,
                            Description =c.Description,
                            Icon = c.Icon,
                            ID = c.ID,
                            Name = c.Name,
                            Parameter = c.Parameter,
                            Pic = c.Pic,
                            Property1 = c.Property1,
                            Property2 = c.Property2,
                            Property3 = c.Property3,
                            Random = c.Random,
                            Script = c.Script,
                        };
            return query.ToArray();
        }

        public BallInfo[] GetAllBall()
        {
            var query = from c in db.Balls
                        select new BallInfo
                        {
                            Amount = c.Amount,
                            ID = c.ID,
                            Name = c.Name,
                            Power = c.Power,
                            Radii = c.Radii,
                        };
            return query.ToArray();
        }

        public void Dispose()
        {
            db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
