using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PressureTest
{
    public class ConfigMgr
    {
        public static string AccountFormat = "test{0}";
        public static int AccountStart = 10000;
        public static int Count = 100;
        public static int ActionInterval = 1000;
        public static string ServerIp;
        public static int ServerPort;
        public static string CreateLoginUrl;
        public static string LoginUrl;
        public static string Md5Key;
        public static string RSAKey;
        public static int Version = 11000;
        public static int RoomType = 2;
        public static int TimeType = 3;
        public static int CountPerSecond = 50;
    }
}
