using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Base;
using System.Timers;
using System.Web;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using Game.Base.Packets;
using log4net;
using System.Reflection;

namespace PressureTest
{
    public delegate void PlayerExecutable();

    public class TestPlayer : BaseConnector
    {
        public static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static byte GAME_CMD = 0x5b;

        private ePlayerState m_state;
        public string Account;
        public string Password;
        public string LastError;
        public string LastMsg;
        public int UserId;
        private Timer m_timer;
        private Queue<PlayerExecutable> m_queue;
        private bool m_isHost;
        private int m_team;
        private int m_playerId;
        private int m_blood;
        private int m_shootCount;
        private int m_gameCount;
        private int m_roomCount;
        private short m_lastRecv;
        private int m_recvCount;
        private short m_lastSent;
        private int m_sentCount;
        private DateTime m_lastSentTime;
        private long m_lastSentTick;
        private long m_actionInterval;
        private StreamWriter m_log;

        public TestPlayer(string account, int interval, string ip, int port)
            : base(ip, port, false, new byte[2048], new byte[2048])
        {
            Account = account;
            Password = account;
            m_isHost = false;
            m_timer = new Timer(interval);
            m_timer.Elapsed += new ElapsedEventHandler(m_timer_Elapsed);
            m_queue = new Queue<PlayerExecutable>();
            m_state = ePlayerState.Init;
            m_lastSentTick = TickHelper.GetTickCount();
            m_actionInterval = 0;
            m_recvCount = 0;
            m_sentCount = 0;
            Strict = true;
            Encryted = true;
            m_log = new StreamWriter(File.Create(string.Format("./logs/{0}.log", account)));
        }

        public ePlayerState State
        {
            get { return m_state; }
        }

        public int RoomCount
        {
            get { return m_roomCount; }
        }

        public int GameCount
        {
            get { return m_gameCount; }
        }

        public short LastRecv
        {
            get { return m_lastRecv; }
        }

        public short LastSent
        {
            get { return m_lastSent; }
        }

        public DateTime LastSentTime
        {
            get { return m_lastSentTime; }
        }

        public long ActionInterval
        {
            get { return m_actionInterval; }
        }

        public int RecvCount
        {
            get { return m_recvCount; }
        }

        public int SentCount
        {
            get { return m_sentCount; }
        }

        #region Action
        public void Act(PlayerExecutable action)
        {
            lock (m_queue)
            {
                m_queue.Enqueue(action);
            }
        }

        void m_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (m_state == ePlayerState.Stopped)
            {
                Stop();
                m_queue = new Queue<PlayerExecutable>();
            }
            else
            {
                PlayerExecutable func = null;
                lock (m_queue)
                {
                    if (m_queue.Count > 0)
                    {
                        func = m_queue.Dequeue();
                    }
                }

                if (func != null)
                {
                    try
                    {
                        func();
                    }
                    catch (Exception ex)
                    {
                        m_state = ePlayerState.Stopped;
                        LastError = ex.Message;
                        Console.WriteLine("player[{0}] execute error:{1}", Account,ex.Message);
                        Console.WriteLine(ex.StackTrace);
                    }
                }
            }
        }

        #endregion

        public void Start()
        {
            Console.WriteLine("{0} starting....", Account);
            m_timer.Start();
            Act(new PlayerExecutable(CreateLogin));
        }

        public void Stop()
        {
            m_state = ePlayerState.Stopped;
            Disconnect();
            m_timer.Stop();
            if(m_log != null)
                m_log.Dispose();
        }
        public void CreateLogin()
        {
            string strtime = DateTime.Now.ToFileTime().ToString();

            string strdata = string.Format("{0}?content={1}", ConfigMgr.CreateLoginUrl, HttpUtility.UrlEncode(Account + "|" + Password + "|" + strtime + "|" + Md5(Account + Password + strtime + ConfigMgr.Md5Key)));

            string result = "";

            WebRequest reqeust = WebRequest.Create(strdata);
            using (WebResponse respose = reqeust.GetResponse())
            using (StreamReader reader = new StreamReader(respose.GetResponseStream()))
            {
                result = reader.ReadLine();
            }

            if (result == "0")
            {
                m_state = ePlayerState.CreateLogined;
                Console.WriteLine("{0} create login success!....", Account);
                Act(new PlayerExecutable(LoginWeb));
            }
            else
            {
                m_state = ePlayerState.Stopped;
                LastError = "CreateLogin失败!";
            }
        }

        public void LoginWeb()
        {
            string strdata = Account + "," + Password + "," + Password + "," + Account;//DZ_M

            MemoryStream msData = new MemoryStream();

            string param = "";
            using (BinaryWriter writer = new BinaryWriter(msData))
            {

                writer.Write((short)DateTime.UtcNow.Year);
                writer.Write((byte)DateTime.UtcNow.Month);
                writer.Write((byte)DateTime.UtcNow.Date.Day);
                writer.Write((byte)DateTime.UtcNow.Hour);
                writer.Write((byte)DateTime.UtcNow.Minute);
                writer.Write((byte)DateTime.UtcNow.Second);
                writer.Write(Encoding.UTF8.GetBytes(strdata));

                byte[] data = Rsa(msData.ToArray());

                param = string.Format("{0}?p={1}&v=0", ConfigMgr.LoginUrl, HttpUtility.UrlEncode(Convert.ToBase64String(data)));
            }

            string result = "";

            WebRequest reqeust = WebRequest.Create(param);
            using (WebResponse respose = reqeust.GetResponse())
            using (StreamReader reader = new StreamReader(respose.GetResponseStream()))
            {
                result = reader.ReadLine();
            }

            if (result.IndexOf("Login Success") > 0 || result.IndexOf("登陆成功") > 0)
            {
                Console.WriteLine("{0} web login success....", Account);
                m_state = ePlayerState.WebLogined;
                if (Connect() == false)
                {
                    Console.WriteLine("{0} connect to server failed...", Account);
                    m_state = ePlayerState.Stopped;
                    LastError = "无法连接服务器.";
                }
                else
                {
                    Console.WriteLine("{0} socket connected....", Account);
                    if (m_state == ePlayerState.WebLogined)
                    {
                        Act(new PlayerExecutable(LoginSocket));
                    }
                }
            }
            else
            {
                m_state = ePlayerState.Stopped;
                LastError = "web登陆失败";
            }
        }

        public void LoginSocket()
        {
            string strdata = Account + "," + Password;
            short fsm_key = 101;
            byte[] bkey = BitConverter.GetBytes(fsm_key);
            Array.Reverse(bkey);

            MemoryStream ms = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(ms))
            {
                writer.Write((short)DateTime.UtcNow.Year);
                writer.Write((byte)DateTime.UtcNow.Month);
                writer.Write((byte)DateTime.UtcNow.Date.Day);
                writer.Write((byte)DateTime.UtcNow.Hour);
                writer.Write((byte)DateTime.UtcNow.Minute);
                writer.Write((byte)DateTime.UtcNow.Second);
                writer.Write(bkey[0]);
                writer.Write(bkey[1]);
                writer.Write(Encoding.UTF8.GetBytes(strdata));
            }

            byte[] data = Rsa(ms.ToArray());

            GSPacketIn pkg = new GSPacketIn((byte)ePackageType.LOGIN);
            pkg.WriteInt(ConfigMgr.Version);
            pkg.Write(data);
            SendTCP(pkg);
            SetFsm(fsm_key, ConfigMgr.Version);
            Console.WriteLine("{0} sending login socket....", Account);
        }

        public override void SendTCP(GSPacketIn pkg)
        {
            base.SendTCP(pkg);
            m_sentCount++;
            m_lastSent = pkg.Code;
            m_lastSentTime = DateTime.Now;
            m_actionInterval = TickHelper.GetTickCount() - m_lastSentTick;
            m_lastSentTick = TickHelper.GetTickCount();
            if(m_log != null)
                m_log.WriteLine(Marshal.ToHexDump(string.Format("player [{0}]:{1} sent:", Account, DateTime.Now), pkg.Buffer, 0, pkg.Length));
        }

        public override void Disconnect()
        {
            base.Disconnect();
            m_state = ePlayerState.Stopped;
            LastError = "主动从服务器断开!";
        }

        protected override void OnDisconnect()
        {
            base.OnDisconnect();
            if (m_state != ePlayerState.Stopped)
            {
                m_state = ePlayerState.Stopped;
                LastError = "被服务器断开!";
            }
        }

        public override void OnRecvPacket(GSPacketIn pkg)
        {
            m_lastRecv = pkg.Code;
            m_recvCount++;
            Console.WriteLine("player [{0}]:{1} receive: 0x{2:X2}", Account, DateTime.Now, pkg.Code);
            if(m_log != null)
                m_log.WriteLine(Marshal.ToHexDump(string.Format("player [{0}]:{1} recive:", Account, DateTime.Now), pkg.Buffer, 0, pkg.Length));
            //Console.WriteLine(Marshal.ToHexDump("pkg detail", pkg.Buffer, 0, pkg.Length));
            switch (pkg.Code)
            {
                case (byte)ePackageType.KIT_USER:
                    m_state = ePlayerState.Stopped;
                    LastError = "踢出游戏:" + pkg.ReadString();
                    break;
                case (byte)ePackageType.SYS_MESS:
                        pkg.ReadInt();
                        LastMsg = pkg.ReadString();
                    break;
                case (byte)ePackageType.LOGIN:
                    if (pkg.ReadByte() == 0)
                    {
                        Console.WriteLine("{0} login socket success!", Account);
                        UserId = pkg.ClientID;
                        Act(new PlayerExecutable(CreateRoom));
                    }
                    else
                    {
                        string msg = pkg.ReadString();
                        Console.WriteLine("{0} login socket failed:{1}", Account, msg);
                        m_state = ePlayerState.Stopped;
                        LastError = "Socket登陆失败:" + msg;
                    }
                    break;
                case (byte)ePackageType.GAME_ROOM_CREATE:
                    m_state = ePlayerState.Room;
                    m_isHost = true;
                    m_roomCount++;
                    Act(new PlayerExecutable(StartGame));
                    break;
                //游戏内协议
                case 0x5b:
                    eTankCmdType cmd = (eTankCmdType)pkg.ReadByte();
                    switch (cmd)
                    {
                        case eTankCmdType.GAME_CREATE:
                            m_gameCount++;
                            pkg.ReadInt();
                            pkg.ReadInt();
                            pkg.ReadInt();

                            int count = pkg.ReadInt();
                            for (int i = 0; i < count; i++)
                            {
                                int cid = pkg.ReadInt();
                                pkg.ReadString();
                                pkg.ReadBoolean();
                                pkg.ReadInt();
                                pkg.ReadString();
                                pkg.ReadString();
                                pkg.ReadString();
                                pkg.ReadInt();
                                pkg.ReadInt();
                                pkg.ReadInt();
                                pkg.ReadInt();
                                pkg.ReadString();
                                pkg.ReadInt();
                                pkg.ReadInt();

                                int team = pkg.ReadInt();
                                int pid = pkg.ReadInt();
                                int blood = pkg.ReadInt();
                                if (cid == UserId)
                                {
                                    m_team = team;
                                    m_playerId = pid;
                                    m_blood = blood;
                                }
                            }
                            m_state = ePlayerState.CreateGame;
                            break;
                        case eTankCmdType.GAME_LOAD:
                            m_state = ePlayerState.Loading;
                            Act(new PlayerExecutable(SendLoadingComplete));
                            break;
                        case eTankCmdType.START_GAME:
                            m_state = ePlayerState.StartGame;
                            break;
                        case eTankCmdType.TURN:
                            if (pkg.Parameter1 == m_playerId)
                            {
                                if (m_shootCount > 0)
                                {
                                    m_state = ePlayerState.Shoot;
                                    m_shootCount--;
                                    Act(new PlayerExecutable(Shoot));
                                }
                                else
                                {
                                    Act(new PlayerExecutable(EnterWaitingRoom));
                                    Act(new PlayerExecutable(CreateRoom));
                                }
                            }
                            break;
                        case eTankCmdType.GAME_OVER:
                            Act(new PlayerExecutable(EnterWaitingRoom));
                            break;
                    }

                    break;


            }
        }

        public void CreateRoom()
        {
            m_lastRecv = 0;

            GSPacketIn pkg = new GSPacketIn((byte)ePackageType.GAME_ROOM_CREATE);
            pkg.WriteByte((byte)ConfigMgr.RoomType);
            pkg.WriteByte((byte)ConfigMgr.TimeType);
            pkg.WriteString("开心弹弹堂，快乐无极限!");
            pkg.WriteString("");

            SendTCP(pkg);
        }

        public void StartGame()
        {
            if (m_isHost)
            {
                GSPacketIn pkg = new GSPacketIn((byte)ePackageType.GAME_START);
                SendTCP(pkg);
            }
        }

        public void SendLoadingComplete()
        {
            GSPacketIn pkg = new GSPacketIn((byte)GAME_CMD);
            pkg.WriteByte((byte)eTankCmdType.LOAD);
            pkg.WriteInt(100);
            SendTCP(pkg);
        }

        public void Shoot()
        {
            SendShootTag(true, 0);
            SendGameCMDShoot(100, 100, 100, 60);
        }

        public void EnterWaitingRoom()
        {
            m_state = ePlayerState.WaitingRoom;
            GSPacketIn pkg = new GSPacketIn((byte)ePackageType.SCENE_LOGIN);
            SendTCP(pkg);
        }

        private void SendGameCMDShoot(int x, int y, int force, int angle)
        {
            GSPacketIn pkg = new GSPacketIn((byte)GAME_CMD);
            pkg.WriteByte((byte)eTankCmdType.FIRE);
            pkg.WriteInt(x);
            pkg.WriteInt(y);
            pkg.WriteInt(force);
            pkg.WriteInt(angle);
            SendTCP(pkg);
        }

        private void SendShootTag(bool b, int time)
        {
            GSPacketIn pkg = new GSPacketIn((byte)GAME_CMD);
            pkg.WriteByte((byte)eTankCmdType.FIRE_TAG);
            pkg.WriteBoolean(b);
            pkg.WriteByte((byte)time);

            SendTCP(pkg);
        }



        #region Helps  RSA/Md5

        public static string Md5(string str)
        {
            return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(str, "md5").ToLower();
        }

        public static byte[] Rsa(byte[] input)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(ConfigMgr.RSAKey);
            return rsa.Encrypt(input, false);
        }
        #endregion

    }
}
