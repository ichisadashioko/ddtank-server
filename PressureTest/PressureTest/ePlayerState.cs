using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PressureTest
{
    public enum ePlayerState
    {
        Init,
        CreateLogined,
        WebLogined,
        Logined,
        WaitingRoom,
        Room,
        CreateGame,
        Loading,
        StartGame,
        Shoot,
        Stopped,
        Dipose
    }
}
