namespace PressureTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbnBoss = new System.Windows.Forms.RadioButton();
            this.rbnFuben = new System.Windows.Forms.RadioButton();
            this.rbnJinJi = new System.Windows.Forms.RadioButton();
            this.rbnTanXian = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbxServerIp = new System.Windows.Forms.TextBox();
            this.tbxServerPort = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbxCountPerSec = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxActionInterval = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ckbSellectConsole = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbxAccountFormat = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxCount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxAccountStart = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lvPlayers = new System.Windows.Forms.ListView();
            this.colAccount = new System.Windows.Forms.ColumnHeader();
            this.clnState = new System.Windows.Forms.ColumnHeader();
            this.clnRoomCount = new System.Windows.Forms.ColumnHeader();
            this.clnGameCount = new System.Windows.Forms.ColumnHeader();
            this.clnLastRecv = new System.Windows.Forms.ColumnHeader();
            this.clnLastSend = new System.Windows.Forms.ColumnHeader();
            this.clnLastSentTime = new System.Windows.Forms.ColumnHeader();
            this.clhLastMsg = new System.Windows.Forms.ColumnHeader();
            this.clnLastError = new System.Windows.Forms.ColumnHeader();
            this.clnIsConnected = new System.Windows.Forms.ColumnHeader();
            this.clnActionInterval = new System.Windows.Forms.ColumnHeader();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblCount = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.clnRecvCount = new System.Windows.Forms.ColumnHeader();
            this.clnSentCount = new System.Windows.Forms.ColumnHeader();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbnBoss);
            this.groupBox5.Controls.Add(this.rbnFuben);
            this.groupBox5.Controls.Add(this.rbnJinJi);
            this.groupBox5.Controls.Add(this.rbnTanXian);
            this.groupBox5.Location = new System.Drawing.Point(197, 17);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(113, 117);
            this.groupBox5.TabIndex = 29;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "房间类型:";
            // 
            // rbnBoss
            // 
            this.rbnBoss.AutoSize = true;
            this.rbnBoss.Location = new System.Drawing.Point(22, 87);
            this.rbnBoss.Name = "rbnBoss";
            this.rbnBoss.Size = new System.Drawing.Size(59, 16);
            this.rbnBoss.TabIndex = 3;
            this.rbnBoss.Text = "Boss战";
            this.rbnBoss.UseVisualStyleBackColor = true;
            // 
            // rbnFuben
            // 
            this.rbnFuben.AutoSize = true;
            this.rbnFuben.Checked = true;
            this.rbnFuben.Location = new System.Drawing.Point(22, 65);
            this.rbnFuben.Name = "rbnFuben";
            this.rbnFuben.Size = new System.Drawing.Size(47, 16);
            this.rbnFuben.TabIndex = 2;
            this.rbnFuben.TabStop = true;
            this.rbnFuben.Text = "副本";
            this.rbnFuben.UseVisualStyleBackColor = true;
            // 
            // rbnJinJi
            // 
            this.rbnJinJi.AutoSize = true;
            this.rbnJinJi.Location = new System.Drawing.Point(22, 44);
            this.rbnJinJi.Name = "rbnJinJi";
            this.rbnJinJi.Size = new System.Drawing.Size(47, 16);
            this.rbnJinJi.TabIndex = 1;
            this.rbnJinJi.Text = "竞技";
            this.rbnJinJi.UseVisualStyleBackColor = true;
            // 
            // rbnTanXian
            // 
            this.rbnTanXian.AutoSize = true;
            this.rbnTanXian.Location = new System.Drawing.Point(22, 22);
            this.rbnTanXian.Name = "rbnTanXian";
            this.rbnTanXian.Size = new System.Drawing.Size(47, 16);
            this.rbnTanXian.TabIndex = 0;
            this.rbnTanXian.Text = "探险";
            this.rbnTanXian.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.tbxServerIp);
            this.groupBox4.Controls.Add(this.tbxServerPort);
            this.groupBox4.Location = new System.Drawing.Point(12, 16);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(165, 118);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "游戏服务器选择:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 12);
            this.label12.TabIndex = 14;
            this.label12.Text = "服务器IP:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 12);
            this.label13.TabIndex = 16;
            this.label13.Text = "服务器端口:";
            // 
            // tbxServerIp
            // 
            this.tbxServerIp.Location = new System.Drawing.Point(8, 44);
            this.tbxServerIp.Name = "tbxServerIp";
            this.tbxServerIp.Size = new System.Drawing.Size(137, 21);
            this.tbxServerIp.TabIndex = 15;
            // 
            // tbxServerPort
            // 
            this.tbxServerPort.Location = new System.Drawing.Point(8, 87);
            this.tbxServerPort.Name = "tbxServerPort";
            this.tbxServerPort.Size = new System.Drawing.Size(137, 21);
            this.tbxServerPort.TabIndex = 17;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbxCountPerSec);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.tbxActionInterval);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(507, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(156, 117);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "动作";
            // 
            // tbxCountPerSec
            // 
            this.tbxCountPerSec.Location = new System.Drawing.Point(6, 78);
            this.tbxCountPerSec.Name = "tbxCountPerSec";
            this.tbxCountPerSec.Size = new System.Drawing.Size(141, 21);
            this.tbxCountPerSec.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "每秒人数";
            // 
            // tbxActionInterval
            // 
            this.tbxActionInterval.Location = new System.Drawing.Point(6, 32);
            this.tbxActionInterval.Name = "tbxActionInterval";
            this.tbxActionInterval.Size = new System.Drawing.Size(141, 21);
            this.tbxActionInterval.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "动作间隔";
            // 
            // ckbSellectConsole
            // 
            this.ckbSellectConsole.AutoSize = true;
            this.ckbSellectConsole.Checked = true;
            this.ckbSellectConsole.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbSellectConsole.Location = new System.Drawing.Point(381, 162);
            this.ckbSellectConsole.Name = "ckbSellectConsole";
            this.ckbSellectConsole.Size = new System.Drawing.Size(108, 16);
            this.ckbSellectConsole.TabIndex = 0;
            this.ckbSellectConsole.Text = "显示控制台消息";
            this.ckbSellectConsole.UseVisualStyleBackColor = true;
            this.ckbSellectConsole.CheckedChanged += new System.EventHandler(this.ckbSellectConsole_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbxAccountFormat);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.tbxCount);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.tbxAccountStart);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(333, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(156, 118);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "账户信息";
            // 
            // tbxAccountFormat
            // 
            this.tbxAccountFormat.Location = new System.Drawing.Point(9, 33);
            this.tbxAccountFormat.Name = "tbxAccountFormat";
            this.tbxAccountFormat.Size = new System.Drawing.Size(141, 21);
            this.tbxAccountFormat.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "开始:";
            // 
            // tbxCount
            // 
            this.tbxCount.Location = new System.Drawing.Point(85, 79);
            this.tbxCount.Name = "tbxCount";
            this.tbxCount.Size = new System.Drawing.Size(65, 21);
            this.tbxCount.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(83, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "数量:";
            // 
            // tbxAccountStart
            // 
            this.tbxAccountStart.Location = new System.Drawing.Point(6, 79);
            this.tbxAccountStart.Name = "tbxAccountStart";
            this.tbxAccountStart.Size = new System.Drawing.Size(60, 21);
            this.tbxAccountStart.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "账户格式:";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(507, 155);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 31;
            this.btnStart.Text = "开始";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(588, 155);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 32;
            this.btnStop.Text = "停止";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 33;
            this.label3.Text = "总在线人数:";
            // 
            // lvPlayers
            // 
            this.lvPlayers.AllowColumnReorder = true;
            this.lvPlayers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvPlayers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colAccount,
            this.clnState,
            this.clnRoomCount,
            this.clnGameCount,
            this.clnRecvCount,
            this.clnSentCount,
            this.clnLastRecv,
            this.clnLastSend,
            this.clnLastSentTime,
            this.clnIsConnected,
            this.clnActionInterval,
            this.clhLastMsg,
            this.clnLastError});
            this.lvPlayers.GridLines = true;
            this.lvPlayers.Location = new System.Drawing.Point(12, 184);
            this.lvPlayers.Name = "lvPlayers";
            this.lvPlayers.Size = new System.Drawing.Size(861, 382);
            this.lvPlayers.TabIndex = 34;
            this.lvPlayers.UseCompatibleStateImageBehavior = false;
            this.lvPlayers.View = System.Windows.Forms.View.Details;
            // 
            // colAccount
            // 
            this.colAccount.Text = "Account";
            this.colAccount.Width = 70;
            // 
            // clnState
            // 
            this.clnState.Text = "State";
            this.clnState.Width = 62;
            // 
            // clnRoomCount
            // 
            this.clnRoomCount.Text = "R-C";
            this.clnRoomCount.Width = 42;
            // 
            // clnGameCount
            // 
            this.clnGameCount.Text = "G-C";
            this.clnGameCount.Width = 40;
            // 
            // clnLastRecv
            // 
            this.clnLastRecv.Text = "L-Recv";
            this.clnLastRecv.Width = 54;
            // 
            // clnLastSend
            // 
            this.clnLastSend.Text = "L-Send";
            this.clnLastSend.Width = 56;
            // 
            // clnLastSentTime
            // 
            this.clnLastSentTime.Text = "L-Time";
            // 
            // clhLastMsg
            // 
            this.clhLastMsg.Text = "Last Msg";
            this.clhLastMsg.Width = 146;
            // 
            // clnLastError
            // 
            this.clnLastError.Text = "LastError";
            this.clnLastError.Width = 100;
            // 
            // clnIsConnected
            // 
            this.clnIsConnected.Text = "Conn.";
            // 
            // clnActionInterval
            // 
            this.clnActionInterval.Text = "A.IV";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(100, 160);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(23, 12);
            this.lblCount.TabIndex = 35;
            this.lblCount.Text = "0/0";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(172, 155);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 36;
            this.btnRefresh.Text = "refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // clnRecvCount
            // 
            this.clnRecvCount.Text = "Recv.C";
            // 
            // clnSentCount
            // 
            this.clnSentCount.Text = "Sent.C";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(670, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 161);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "说明";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(197, 84);
            this.label7.TabIndex = 0;
            this.label7.Text = "R-C房间数量,G-C游戏数量,Recv.C\r\n总接收包数,Sent.C总发送包数,\r\nL-Recv最后收到的协议,L-Send最后\r\n发送的协议,L-Time最" +
                "后发送时间,\r\nConn.是否连接,A.IV平均动作间隔,\r\nLast Msg最后收到的文本消息,Last \r\nError最后错误";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 578);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.lvPlayers);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ckbSellectConsole);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "压力测试";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbnBoss;
        private System.Windows.Forms.RadioButton rbnFuben;
        private System.Windows.Forms.RadioButton rbnJinJi;
        private System.Windows.Forms.RadioButton rbnTanXian;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbxServerIp;
        private System.Windows.Forms.TextBox tbxServerPort;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox ckbSellectConsole;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxAccountStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxCount;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxActionInterval;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxAccountFormat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxCountPerSec;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.ColumnHeader colAccount;
        private System.Windows.Forms.ColumnHeader clnState;
        private System.Windows.Forms.ColumnHeader clnLastError;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ColumnHeader clnRoomCount;
        private System.Windows.Forms.ColumnHeader clnGameCount;
        private System.Windows.Forms.ColumnHeader clnLastRecv;
        private System.Windows.Forms.ColumnHeader clnLastSend;
        private System.Windows.Forms.ListView lvPlayers;
        private System.Windows.Forms.ColumnHeader clhLastMsg;
        private System.Windows.Forms.ColumnHeader clnLastSentTime;
        private System.Windows.Forms.ColumnHeader clnIsConnected;
        private System.Windows.Forms.ColumnHeader clnActionInterval;
        private System.Windows.Forms.ColumnHeader clnRecvCount;
        private System.Windows.Forms.ColumnHeader clnSentCount;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
    }
}

