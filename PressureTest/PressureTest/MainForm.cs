using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace PressureTest
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            tbxAccountFormat.Text = ConfigurationSettings.AppSettings["accountFormat"];
            tbxAccountStart.Text = ConfigurationSettings.AppSettings["accountStart"];
            tbxCount.Text = ConfigurationSettings.AppSettings["accountCount"];
            tbxActionInterval.Text = ConfigurationSettings.AppSettings["actionInterval"];
            string type = ConfigurationSettings.AppSettings["roomType"];
            if (type == "4")
            {
                rbnFuben.Checked = true;
            }
            else
            {
                rbnJinJi.Checked = true;
            }
            rbnBoss.Enabled = false;
            rbnTanXian.Enabled = false;

            tbxServerIp.Text = ConfigurationSettings.AppSettings["loginServer"];
            tbxServerPort.Text = ConfigurationSettings.AppSettings["loginPort"];
            tbxCountPerSec.Text = ConfigurationSettings.AppSettings["countPerSec"];

            ConfigMgr.CreateLoginUrl = ConfigurationSettings.AppSettings["createLoginUrl"];
            ConfigMgr.LoginUrl = ConfigurationSettings.AppSettings["loginUrl"];
            ConfigMgr.Md5Key = ConfigurationSettings.AppSettings["md5"];
            ConfigMgr.RSAKey = ConfigurationSettings.AppSettings["rsa"];
            ConfigMgr.Version = int.Parse(ConfigurationSettings.AppSettings["Version"]);

            ckbSellectConsole.Checked = true;
            
            btnStart.Enabled = true;
            btnStop.Enabled = false;
            TestMgr.Setup();
            timer1.Start();

            log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo("logconfig.xml"));
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            ConfigMgr.ServerIp = tbxServerIp.Text;
            ConfigMgr.ServerPort = int.Parse(tbxServerPort.Text);
            ConfigMgr.AccountFormat = tbxAccountFormat.Text;
            ConfigMgr.AccountStart = int.Parse(tbxAccountStart.Text);
            ConfigMgr.Count = int.Parse(tbxCount.Text);
            ConfigMgr.ActionInterval = int.Parse(tbxActionInterval.Text);
            ConfigMgr.CountPerSecond = int.Parse(tbxCountPerSec.Text);
            if (rbnFuben.Checked)
            {
                ConfigMgr.RoomType = 4;
            }
            else
            {
                ConfigMgr.RoomType = 0; //竞技
            }

            TestMgr.Start();
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            if (ckbSellectConsole.Checked)
            {
                ConsoleManager.Create();
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            TestMgr.Stop();
            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }

        private void ckbSellectConsole_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbSellectConsole.Checked)
            {
                ConsoleManager.Create();
            }
            else
            {
                ConsoleManager.Destroy();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblCount.Text = string.Format("{0}/{1}", TestMgr.GetLivingCount(), TestMgr.GetPlayerCount());
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            lvPlayers.Items.Clear();
            List<TestPlayer> list = TestMgr.GetPlayers();
            foreach (TestPlayer player in list)
            {
                ListViewItem item = new ListViewItem();
                item.Text = player.Account;
                item.SubItems.Add(player.State.ToString());
                item.SubItems.Add(player.RoomCount.ToString());
                item.SubItems.Add(player.GameCount.ToString());
                item.SubItems.Add(player.RecvCount.ToString());
                item.SubItems.Add(player.SentCount.ToString());
                item.SubItems.Add(player.LastRecv.ToString("X2"));
                item.SubItems.Add(player.LastSent.ToString("X2"));
                item.SubItems.Add(player.LastSentTime.ToString("mm:ss"));
                item.SubItems.Add(player.IsConnected.ToString());
                item.SubItems.Add(player.ActionInterval.ToString());
                item.SubItems.Add(player.LastMsg);
                item.SubItems.Add(player.LastError);
                lvPlayers.Items.Add(item);
            }
        }
    }
}
