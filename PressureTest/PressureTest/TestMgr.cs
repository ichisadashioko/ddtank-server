using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace PressureTest
{
    public class TestMgr
    {
        private static List<TestPlayer> m_list = new List<TestPlayer>();
        private static Timer m_timer = new Timer(1000);
        private static int m_index;
        private static bool m_running;

        public static void Setup()
        {
            m_timer.Elapsed += new ElapsedEventHandler(m_timer_Elapsed);
        }



        public static void Start()
        {
            if (!m_running)
            {
                m_running = true;
                m_list.Clear();
                m_index = 0;
                m_timer.Start();
            }
        }

        public static void Stop()
        {
            if (m_running)
            {
                m_running = false;
                m_timer.Stop();
                foreach (TestPlayer player in m_list)
                {
                    player.Stop();
                }
            }
        }

        public static List<TestPlayer> GetPlayers()
        {
            List<TestPlayer> list = new List<TestPlayer>();
            lock (m_list)
            {
                list.AddRange(m_list);
            }
            return list;
        }

        public static int GetPlayerCount()
        {
            return m_list.Count;
        }

        public static int GetLivingCount()
        {
            int i = 0;
            lock (m_list)
            {
                foreach (TestPlayer player in m_list)
                {
                    if (player.State != ePlayerState.Stopped)
                    {
                        i++;
                    }
                }
            }
            return i;
        }

        
        static void m_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            m_timer.Stop(); 
            int i = ConfigMgr.CountPerSecond;
            while (i > 0 && m_index < ConfigMgr.Count)
            {
                TestPlayer player = new TestPlayer(string.Format(ConfigMgr.AccountFormat, ConfigMgr.AccountStart + m_index), ConfigMgr.ActionInterval, ConfigMgr.ServerIp, ConfigMgr.ServerPort);
                player.Start();
                lock (m_list)
                {
                    m_list.Add(player);
                    i--;
                    m_index++;
                }
            }
            if (m_index < ConfigMgr.Count)
            {
                m_timer.Start();
            }
        }
    }
}
