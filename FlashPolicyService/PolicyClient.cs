using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Base;
using Game.Base.Packets;

namespace PolicyMasterServer
{
    public class PolicyClient:BaseClient
    {
        private static readonly byte[] POLICY = Encoding.UTF8.GetBytes("<?xml version=\"1.0\"?><cross-domain-policy><allow-access-from domain=\"*\" to-ports=\"*\" /></cross-domain-policy>\0");

        private static readonly byte[] sendBuffer = new byte[512];

        private static readonly byte[] readBuffer = new byte[512];

        public PolicyClient()
            : base(readBuffer, sendBuffer)
        {
        }

        protected override void OnConnect()
        {
            base.OnConnect();
            Socket.Send(POLICY);
            this.Disconnect();
        }
    }
}
