using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Base;
using System.Net;

namespace PolicyMasterServer
{
    public class PolicyServer:BaseServer
    {
        protected override BaseClient GetNewClient()
        {
            return new PolicyClient();
        }

        public override bool Start()
        {
            InitSocket(IPAddress.Any, 843);
            return base.Start();
        }
    }
}
