using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using PolicyMasterServer;

namespace FlashPolicyService
{
    public partial class FlashPolicyService : ServiceBase
    {
        private PolicyServer svr = new PolicyServer();
        public FlashPolicyService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            svr.Start();
        }

        protected override void OnStop()
        {
            svr.Stop();
        }
    }
}
